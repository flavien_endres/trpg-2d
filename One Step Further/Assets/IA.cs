﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Pathfinding
{
    public class IA : MonoBehaviour
    {
        public TurnManagement turnManager;
        CharacterStats stats;

        public Transform targetPosition;
        public GameObject PlayerUnit;

        public Path unitPath;
        public float speed = 2;
        public float nextWaypointDistance = 3;
        private int currentWaypoint = 0;
        public bool reachedEndOfPath;

        [HideInInspector]
        public List<GameObject> UnitsInAttackRange = new List<GameObject>();
        [HideInInspector]
        public bool hasAttackedThisTurn = false;

        private levelManager managerLevel;
        
        Seeker seeker;

        private AttackAi iaAttack;

        private ClearCanvasCombo clearCombo;

        private Pathfinding.BarreDadrenaline adrenalineBar;

        private AudioSource sourceTransition;

        private ContainerAudioClip scriptContainer;

        void Start()
        {
            stats = GetComponent<CharacterStats>();
            seeker = GetComponent<Seeker>();
            managerLevel = GameObject.Find("GameManager").GetComponent<levelManager>();
            iaAttack = GameObject.Find("Units").GetComponent<AttackAi>();
            clearCombo = GameObject.Find("CanvasCombo").GetComponent<ClearCanvasCombo>();
            adrenalineBar = GameObject.Find("CanvasCombat").GetComponent<Pathfinding.BarreDadrenaline>();

            sourceTransition = GameObject.Find("SFX_Transition").GetComponent<AudioSource>();
            scriptContainer = GameObject.Find("SoundManager").GetComponent<ContainerAudioClip>();
        }

        void Update()
        {
            //Debug.Log(hasAttackedThisTurn);
            //L'IA check si c'est bien à son tour d'agir en regardant le turnManager
            if (turnManager.turnUnit.name == name)
            {
                //Avant toutes choses, on vérifie que l'IA ne se déplace pas pour éviter qu'elle fasse X actions en même temps
                if (reachedEndOfPath)
                {
                    //Debug.Log(UnitsInAttackRange.Count);
                    if (UnitsInAttackRange.Count != 0 && !hasAttackedThisTurn)
                    {

                        GameObject.Find("GameManager").GetComponent<ManagerUIScreens>().ClearCanvasComboForEnemies();
                        managerLevel.currentCharacterAttacked = "Evelyn";

                        //With this, we launch combat
                        //For more informations, go to "AttackAi" script and "ManagerPunchScreen" script
                        if (turnManager.turnUnit.name == "Asha")
                        {
                            managerLevel.currentCharacterAttacking = "Asha";
                        }
                        else if(turnManager.turnUnit.name == "Sbire1")
                        {
                            managerLevel.currentCharacterAttacking = "Sbire1";
                        }
                        else if (turnManager.turnUnit.name == "Sbire2")
                        {
                            managerLevel.currentCharacterAttacking = "Sbire2";
                        }
                        Debug.Log("Bisous");
                        if(managerLevel.currentCharacterAttacking != null)
                        {
                            adrenalineBar.WhatStatsNeeded();
                            GameObject.Find("CanvasMoving").GetComponent<MovingManager>().DisableUnitsDuringBattle();
                            hasAttackedThisTurn = true;
                            GameObject.Find("CanvasMoving").GetComponent<Animator>().SetTrigger("PlayerAttacks");
                            sourceTransition.clip = scriptContainer.sfxTransiCombatIn;
                            sourceTransition.Play();
                        }
                        
                        //iaAttack.BeginAttack();
                    }

                    //Debug.Log(transform.name);

                    if (stats.MovementPoints > 0 && UnitsInAttackRange.Count == 0)
                    {                        
                        IAMove();
                    }

                    if (hasAttackedThisTurn)
                    {
                        //EndTurn();
                    }
                }

                //Si un path a été établi : on vérifie si on a atteint la destination ou non.
                //Normalement on devrait obtenir ça avec le code du package, mais pour une raison inconnue ça ne fonctionne pas. My bad peut-être.
                
                if (unitPath != null && transform.position == (Vector3)unitPath.path.Last().position)
                {
                    HasReachedDestination();
                }
            }
        }

        public void SearchPath()
        {
            // Start to calculate a new path to the targetPosition object, return the result to the OnPathComplete method.
            // Path requests are asynchronous, so when the OnPathComplete method is called depends on how long it
            // takes to calculate the path. Usually it is called the next frame.

            //La position de la cible est définie sur la position du joueur. À améliorer quand ce sera possible pour prendre plusieurs joueurs en compte
            targetPosition.position = PlayerUnit.transform.position;
            seeker.StartPath(transform.position, targetPosition.position, OnPathComplete);
        }

        public void OnPathComplete(Path p)
        {
            //On stocke le path dans une variable locale pour informer le reste du script qu'on a bien un path.
            unitPath = p;

            //Debug.Log("Yay, we got a path back. Did it have an error? " + unitPath.error);

            //Si le chemin est plus long que notre total de PM, on prend la case la plus loin possible (que l'on peut atteindre) et l'on TP la cible dessus
            //On recalcule le chemin, et ça donne un chemin plus court donc valide
            if (stats.MovementPoints < p.GetTotalLength())
            {
                targetPosition.transform.position = (Vector3)p.path[stats.MovementPoints - 1].position;
                seeker.StartPath(transform.position, targetPosition.position, OnPathComplete);
            }
            else
            {
                turnManager.UnitMovementManager((int)p.GetTotalLength());
            }
        }

        void IAMove()
        {
            if (unitPath == null || name == "Asha" || name == "Sbire1" || name == "Sbire2")
            {
                // We have no path to follow yet, so don't do anything
                return;
            }

            // Check in a loop if we are close enough to the current waypoint to switch to the next one.
            // We do this in a loop because many waypoints might be close to each other and we may reach
            // several of them in the same frame.
            reachedEndOfPath = false;
            // The distance to the next waypoint in the path
            float distanceToWaypoint;
            while (true)
            {
                // If you want maximum performance you can check the squared distance instead to get rid of a
                // square root calculation. But that is outside the scope of this tutorial.
                distanceToWaypoint = Vector3.Distance(transform.position, unitPath.vectorPath[currentWaypoint]);
                if (distanceToWaypoint < nextWaypointDistance)
                {
                    // Check if there is another waypoint or if we have reached the end of the path
                    if (currentWaypoint + 1 < unitPath.vectorPath.Count)
                    {
                        currentWaypoint++;
                    }
                    else
                    {
                        // Set a status variable to indicate that the agent has reached the end of the path.
                        // You can use this to trigger some special code if your game requires that.
                        reachedEndOfPath = true; 
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            // Slow down smoothly upon approaching the end of the path
            // This value will smoothly go from 1 to 0 as the agent approaches the last waypoint in the path.
            var speedFactor = reachedEndOfPath ? Mathf.Sqrt(distanceToWaypoint / nextWaypointDistance) : 1f;
            // Direction to the next waypoint
            // Normalize it so that it has a length of 1 world unit
            Vector3 dir = (unitPath.vectorPath[currentWaypoint] - transform.position).normalized;
            // Multiply the direction by our desired speed to get a velocity
            Vector3 velocity = dir * speed * speedFactor;
            transform.Translate(velocity, Space.World);
            
        }

        //Vide le path actuel de l'unité + fin du tour (à changer quand l'IA aura d'autres actions à faire)
        void HasReachedDestination()
        {
            reachedEndOfPath = true;
            unitPath = null;

            if (UnitsInAttackRange.Count == 0)
                turnManager.EndOfTurn();
        }

        void EndTurn()
        {
            Debug.Log("IA TurnManagement");
            turnManager.EndOfTurn();
        }
    }
}
