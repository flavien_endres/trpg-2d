﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pathfinding
{

    public class IA_objet : MonoBehaviour
    {
        private Vector2 velocity;
        private Vector2 direction = Vector2.zero;
        private Vector2 initialPosition;

        private Vector2 positionToGoBackFrom;
        private Vector2 directionGoBack;

        public Transform targetPosition;

        public int DmgWhenThrown;

        float lerpDuration = 0.25f;

        [HideInInspector]
        public bool isMarked = false;
        private bool isMoving = false;
        private bool goBack = false;

        private levelManager managerLevel;
        private GameObject canvasMoving;

        // Start is called before the first frame update
        void Start()
        {
            managerLevel = GameObject.Find("GameManager").GetComponent<levelManager>();
            canvasMoving = GameObject.Find("CanvasMoving");
        }

        // C'est ici qu'on call l'usage du don sur un objet marqué
        void Update()
        {
            if (Input.GetKeyDown("r") && isMarked && !isMoving)
            {
                MoveLeft();
            }
        }

        private void OnTriggerEnter(Collider coll)
        {
            //Pour éviter les collisions avec le layer IgnoreRaycasts. Je peux pas les désactiver dans la matrice physique, sinon l'objet n'est plus repéré pour interaction
            //par les collider d'Evelyn.
            if (isMoving && coll.gameObject.tag != "Non-physical")
            {
                Debug.Log(coll.gameObject);


                //Si la chaise frappe un mec, elle fait mal et le gars peut en mourir
                if (coll.gameObject.tag == "IA")
                {
                    coll.gameObject.GetComponent<AnimationsCharacters>().takeDamage = true;
                    coll.gameObject.GetComponentInChildren<Animator>().Play("TakingDamage");
                    coll.gameObject.GetComponent<CharacterStats>().HealthPoints -= DmgWhenThrown;
                    if (coll.gameObject.GetComponent<CharacterStats>().HealthPoints <= 0) GameObject.Find("GameManager").
                            GetComponent<TurnManagement>().UnitDeath(coll.gameObject);

                    if(coll.gameObject.name == "Asha" && coll.gameObject.GetComponent<CharacterStats>().HealthPoints <= 0)
                    {
                        managerLevel.victory = true;
                        canvasMoving.GetComponent<Animator>().SetTrigger("DisappearUILeft");
                        canvasMoving.GetComponent<Animator>().SetTrigger("Victory");

                        if (canvasMoving.GetComponent<MovingManager>().isRightUIAppeared)
                            canvasMoving.GetComponent<Animator>().SetTrigger("DisappearUIRight");
                    }
                }
                //Si on touche un objet solide, on retourne au point de départ. Ces valeurs vont permettre au Lerp de corriger sa trajectoire
                positionToGoBackFrom = transform.position;
                directionGoBack = positionToGoBackFrom - initialPosition;
                goBack = true;

            }
        }

        public void Move(Vector2 positionEvelyn)
        {
            isMoving = true;

            //La direction du déplacement est déterminée par le calcul du vecteur entre Evelyn et l'objet
            initialPosition = transform.position;
            direction = new Vector2(transform.position.x - positionEvelyn.x, transform.position.y - positionEvelyn.y);

            StartCoroutine(Lerp());
        }

        //Assure le déplacement smooth à l'écran
        IEnumerator Lerp()
        {
            float timeElapsed = 0;

            while (timeElapsed < lerpDuration)
            {
                if (!goBack)
                {
                    transform.position = Vector2.Lerp(initialPosition, initialPosition + direction, timeElapsed / lerpDuration);
                }

                if (goBack)
                {
                    transform.position = Vector2.Lerp(positionToGoBackFrom, positionToGoBackFrom - directionGoBack, timeElapsed / lerpDuration);
                }

                timeElapsed += Time.deltaTime;

                yield return null;
            }

            //Selon qu'on aille à la destination ou qu'on retourne au point de départ, on corrige le point d'arrivée pour le rendre exact (éviter une fioriture de Lerp)
            if (!goBack)
            {
                transform.position = initialPosition + direction;
            }
            else { transform.position = initialPosition; }

            AstarPath.active.Scan();

            isMoving = false;
            goBack = false;
        }

        public void MoveAgain(string clickedArrow)
        {
            isMoving = true;
            isMarked = false;
            GetComponentInChildren<SetAlpha>().ChangeAlphaValue(0);

            //La direction du déplacement est déterminée par le calcul du vecteur entre Evelyn et l'objet
            initialPosition = transform.position;
            switch (clickedArrow) {
                case "Flèche Haut":
                    direction = new Vector2(-0.5f, 0.5f);
                    break;
                case "Flèche Bas":
                    direction = new Vector2(0.5f, -0.5f);
                    break;
                case "Flèche Gauche":
                    direction = new Vector2(0.5f, 0.5f);
                    break;
                case "Flèche Droite":
                    direction = new Vector2(-0.5f, -0.5f);
                    break;
            }

            StartCoroutine(Lerp());
            GameObject.Find("Evelyn").GetComponent<Interact>().ShowGiftArrows();
        }

        public void MoveUp()
        {
            isMoving = true;
            isMarked = false;
            GetComponentInChildren<SetAlpha>().ChangeAlphaValue(0);

            //La direction du déplacement est déterminée par le calcul du vecteur entre Evelyn et l'objet
            initialPosition = transform.position;
            direction = new Vector2(-0.5f, 0.5f);

            StartCoroutine(Lerp());
            GameObject.Find("Evelyn").GetComponent<Interact>().ShowGiftArrows();
        }

        public void MoveDown()
        {
            isMoving = true;
            isMarked = false;
            GetComponentInChildren<SetAlpha>().ChangeAlphaValue(0);

            //La direction du déplacement est déterminée par le calcul du vecteur entre Evelyn et l'objet
            initialPosition = transform.position;
            direction = new Vector2(0.5f, -0.5f);

            StartCoroutine(Lerp());
            GameObject.Find("Evelyn").GetComponent<Interact>().ShowGiftArrows();
        }

        public void MoveLeft()
        {
            isMoving = true;
            isMarked = false;
            GetComponentInChildren<SetAlpha>().ChangeAlphaValue(0);

            //La direction du déplacement est déterminée par le calcul du vecteur entre Evelyn et l'objet
            initialPosition = transform.position;
            direction = new Vector2(0.5f, 0.5f);

            StartCoroutine(Lerp());
            GameObject.Find("Evelyn").GetComponent<Interact>().ShowGiftArrows();
        }

        public void MoveRight()
        {
            isMoving = true;
            isMarked = false;
            GetComponentInChildren<SetAlpha>().ChangeAlphaValue(0);

            //La direction du déplacement est déterminée par le calcul du vecteur entre Evelyn et l'objet
            initialPosition = transform.position;
            direction = new Vector2(-0.5f, -0.5f);

            StartCoroutine(Lerp());
            GameObject.Find("Evelyn").GetComponent<Interact>().ShowGiftArrows();
        }
    }

}