﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetAlpha : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    Material materialWithAlphaValue;

    private void Start()
    {
        materialWithAlphaValue = spriteRenderer.material;
        ChangeAlphaValue(0f);
    }

    public void ChangeAlphaValue(float alpha)
    {
        var color = materialWithAlphaValue.GetColor("_Color");
        materialWithAlphaValue.SetColor("_Color", new Color(color.r, color.g, color.b, alpha));
    }
}
