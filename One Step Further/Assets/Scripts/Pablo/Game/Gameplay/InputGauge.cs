﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Pathfinding
{
    public class InputGauge : MonoBehaviour
    {
        private GameObject inputPress0, inputPress1, inputPress2, inputPress3, inputPress4, inputPress5, inputPress6, inputPress7;

        private levelManager managerLevel;

        private comboManager managerCombo;

        public List<string> inputs = new List<string>();

        public List<Image> inputsImage = new List<Image>();

        public Sprite inputPressedLeft, inputPressedRight, inputPressedDown, inputPressedTop;

        public int counter;

        public Image selectionnedImage;

        // Start is called before the first frame update
        void Start()
        {
            inputPress0 = GameObject.Find("Input_Press_0");
            inputPress1 = GameObject.Find("Input_Press_1");
            inputPress2 = GameObject.Find("Input_Press_2");
            inputPress3 = GameObject.Find("Input_Press_3");
            inputPress4 = GameObject.Find("Input_Press_4");
            inputPress5 = GameObject.Find("Input_Press_5");
            inputPress6 = GameObject.Find("Input_Press_6");
            inputPress7 = GameObject.Find("Input_Press_7");

            managerLevel = GameObject.Find("GameManager").GetComponent<levelManager>();

            managerCombo = gameObject.GetComponent<comboManager>();

        }

        // Update is called once per frame
        void Update()
        {
            inputs = managerCombo.inputs;
            counter = 0;

            if (managerLevel.isInComboScreen)
            {
                if (inputs.Count == 0)
                {
                    DisableAllImages();
                }
                else if (inputs.Count == 1)
                {
                    CounterGauge1();
                    DisplayCorrectImage();
                }
                else if (inputs.Count == 2)
                {
                    CounterGauge2();
                    DisplayCorrectImage();
                }
                else if (inputs.Count == 3)
                {
                    CounterGauge3();
                    DisplayCorrectImage();
                }
                else if (inputs.Count == 4)
                {
                    CounterGauge4();
                    DisplayCorrectImage();
                }
                else if (inputs.Count == 5)
                {
                    CounterGauge5();
                    DisplayCorrectImage();
                }
                else if (inputs.Count == 6)
                {
                    CounterGauge6();
                    DisplayCorrectImage();
                }
                else if (inputs.Count == 7)
                {
                    CounterGauge7();
                    DisplayCorrectImage();
                }
                else if (inputs.Count == 8)
                {
                    CounterGauge8();
                    DisplayCorrectImage();
                }
            }
        }

        public void DisableAllImages()
        {
            inputPress0.GetComponent<Image>().enabled = false;
            inputPress1.GetComponent<Image>().enabled = false;
            inputPress2.GetComponent<Image>().enabled = false;
            inputPress3.GetComponent<Image>().enabled = false;
            inputPress4.GetComponent<Image>().enabled = false;
            inputPress5.GetComponent<Image>().enabled = false;
            inputPress6.GetComponent<Image>().enabled = false;
            inputPress7.GetComponent<Image>().enabled = false;
        }

        public void DisplayCorrectImage()
        {
            inputs.ForEach(delegate (string input)
            {
                counter++;
                if (counter == 1)
                {
                    selectionnedImage = GameObject.Find("Input_Press_0").GetComponent<Image>();
                }
                else if (counter == 2)
                {
                    selectionnedImage = GameObject.Find("Input_Press_1").GetComponent<Image>();
                }
                else if (counter == 3)
                {
                    selectionnedImage = GameObject.Find("Input_Press_2").GetComponent<Image>();
                }
                else if (counter == 4)
                {
                    selectionnedImage = GameObject.Find("Input_Press_3").GetComponent<Image>();
                }
                else if (counter == 5)
                {
                    selectionnedImage = GameObject.Find("Input_Press_4").GetComponent<Image>();
                }
                else if (counter == 6)
                {
                    selectionnedImage = GameObject.Find("Input_Press_5").GetComponent<Image>();
                }
                else if (counter == 7)
                {
                    selectionnedImage = GameObject.Find("Input_Press_6").GetComponent<Image>();
                }
                else if (counter == 8)
                {
                    selectionnedImage = GameObject.Find("Input_Press_7").GetComponent<Image>();
                }

                if (input == "Gauche")
                {
                    selectionnedImage.sprite = inputPressedLeft;
                }

                if (input == "Droite")
                {
                    selectionnedImage.sprite = inputPressedRight;
                }

                if (input == "Haut")
                {
                    selectionnedImage.sprite = inputPressedTop;
                }

                if (input == "Bas")
                {
                    selectionnedImage.sprite = inputPressedDown;
                }

            });
        }


        public void CounterGauge1()
        {
            inputPress0.transform.position = new Vector3(916, 245, 0);
            inputPress0.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            inputPress0.GetComponent<Image>().enabled = true;

            inputPress1.GetComponent<Image>().enabled = false;
            inputPress2.GetComponent<Image>().enabled = false;
            inputPress3.GetComponent<Image>().enabled = false;
            inputPress4.GetComponent<Image>().enabled = false;
            inputPress5.GetComponent<Image>().enabled = false;
            inputPress6.GetComponent<Image>().enabled = false;
            inputPress7.GetComponent<Image>().enabled = false;
        }

        public void CounterGauge2()
        {
            inputPress0.transform.position = new Vector3(916, 245, 0);
            inputPress0.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            inputPress0.GetComponent<Image>().enabled = true;

            inputPress1.transform.position = new Vector3(1057, 332, 0);
            inputPress1.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            inputPress1.GetComponent<Image>().enabled = true;

            inputPress2.GetComponent<Image>().enabled = false;
            inputPress3.GetComponent<Image>().enabled = false;
            inputPress4.GetComponent<Image>().enabled = false;
            inputPress5.GetComponent<Image>().enabled = false;
            inputPress6.GetComponent<Image>().enabled = false;
            inputPress7.GetComponent<Image>().enabled = false;
        }

        public void CounterGauge3()
        {
            inputPress0.transform.position = new Vector3(916, 245, 0);
            inputPress0.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            inputPress0.GetComponent<Image>().enabled = true;

            inputPress1.transform.position = new Vector3(1057, 332, 0);
            inputPress1.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            inputPress1.GetComponent<Image>().enabled = true;

            inputPress2.transform.position = new Vector3(1213, 412, 0);
            inputPress2.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            inputPress2.GetComponent<Image>().enabled = true;

            inputPress3.GetComponent<Image>().enabled = false;
            inputPress4.GetComponent<Image>().enabled = false;
            inputPress5.GetComponent<Image>().enabled = false;
            inputPress6.GetComponent<Image>().enabled = false;
            inputPress7.GetComponent<Image>().enabled = false;
        }

        public void CounterGauge4()
        {
            inputPress0.transform.position = new Vector3(916, 245, 0);
            inputPress0.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            inputPress0.GetComponent<Image>().enabled = true;

            inputPress1.transform.position = new Vector3(1057, 332, 0);
            inputPress1.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            inputPress1.GetComponent<Image>().enabled = true;

            inputPress2.transform.position = new Vector3(1213, 412, 0);
            inputPress2.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            inputPress2.GetComponent<Image>().enabled = true;

            inputPress3.transform.position = new Vector3(1368, 493, 0);
            inputPress3.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            inputPress3.GetComponent<Image>().enabled = true;

            inputPress4.GetComponent<Image>().enabled = false;
            inputPress5.GetComponent<Image>().enabled = false;
            inputPress6.GetComponent<Image>().enabled = false;
            inputPress7.GetComponent<Image>().enabled = false;
        }

        public void CounterGauge5()
        {
            inputPress0.transform.position = new Vector3(906, 235, 0);
            inputPress0.transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
            inputPress0.GetComponent<Image>().enabled = true;

            inputPress1.transform.position = new Vector3(1024, 299, 0);
            inputPress1.transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
            inputPress1.GetComponent<Image>().enabled = true;

            inputPress2.transform.position = new Vector3(1148, 362, 0);
            inputPress2.transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
            inputPress2.GetComponent<Image>().enabled = true;

            inputPress3.transform.position = new Vector3(1258, 426, 0);
            inputPress3.transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
            inputPress3.GetComponent<Image>().enabled = true;

            inputPress4.transform.position = new Vector3(1368, 486, 0);
            inputPress4.transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
            inputPress4.GetComponent<Image>().enabled = true;

            inputPress5.GetComponent<Image>().enabled = false;
            inputPress6.GetComponent<Image>().enabled = false;
            inputPress7.GetComponent<Image>().enabled = false;
        }

        public void CounterGauge6()
        {
            inputPress0.transform.position = new Vector3(896, 236, 0);
            inputPress0.transform.localScale = new Vector3(0.32f, 0.32f, 0.32f);
            inputPress0.GetComponent<Image>().enabled = true;

            inputPress1.transform.position = new Vector3(993, 283, 0);
            inputPress1.transform.localScale = new Vector3(0.32f, 0.32f, 0.32f);
            inputPress1.GetComponent<Image>().enabled = true;

            inputPress2.transform.position = new Vector3(1088, 334, 0);
            inputPress2.transform.localScale = new Vector3(0.32f, 0.32f, 0.32f);
            inputPress2.GetComponent<Image>().enabled = true;

            inputPress3.transform.position = new Vector3(1179, 382, 0);
            inputPress3.transform.localScale = new Vector3(0.32f, 0.32f, 0.32f);
            inputPress3.GetComponent<Image>().enabled = true;

            inputPress4.transform.position = new Vector3(1274, 434, 0);
            inputPress4.transform.localScale = new Vector3(0.32f, 0.32f, 0.32f);
            inputPress4.GetComponent<Image>().enabled = true;

            inputPress5.transform.position = new Vector3(1362, 487, 0);
            inputPress5.transform.localScale = new Vector3(0.32f, 0.32f, 0.32f);
            inputPress5.GetComponent<Image>().enabled = true;

            inputPress6.GetComponent<Image>().enabled = false;
            inputPress7.GetComponent<Image>().enabled = false;
        }

        public void CounterGauge7()
        {
            inputPress0.transform.position = new Vector3(893, 243, 0);
            inputPress0.transform.localScale = new Vector3(0.28f, 0.28f, 0.28f);
            inputPress0.GetComponent<Image>().enabled = true;

            inputPress1.transform.position = new Vector3(964, 282, 0);
            inputPress1.transform.localScale = new Vector3(0.28f, 0.28f, 0.28f);
            inputPress1.GetComponent<Image>().enabled = true;

            inputPress2.transform.position = new Vector3(1043, 320, 0);
            inputPress2.transform.localScale = new Vector3(0.28f, 0.28f, 0.28f);
            inputPress2.GetComponent<Image>().enabled = true;

            inputPress3.transform.position = new Vector3(1122, 358, 0);
            inputPress3.transform.localScale = new Vector3(0.28f, 0.28f, 0.28f);
            inputPress3.GetComponent<Image>().enabled = true;

            inputPress4.transform.position = new Vector3(1203, 397, 0);
            inputPress4.transform.localScale = new Vector3(0.28f, 0.28f, 0.28f);
            inputPress4.GetComponent<Image>().enabled = true;

            inputPress5.transform.position = new Vector3(1281, 437, 0);
            inputPress5.transform.localScale = new Vector3(0.28f, 0.28f, 0.28f);
            inputPress5.GetComponent<Image>().enabled = true;

            inputPress6.transform.position = new Vector3(1362, 487, 0);
            inputPress6.transform.localScale = new Vector3(0.28f, 0.28f, 0.28f);
            inputPress6.GetComponent<Image>().enabled = true;

            inputPress7.GetComponent<Image>().enabled = false;
        }

        public void CounterGauge8()
        {
            inputPress0.transform.position = new Vector3(883, 245, 0);
            inputPress0.transform.localScale = new Vector3(0.23f, 0.23f, 0.23f);
            inputPress0.GetComponent<Image>().enabled = true;

            inputPress1.transform.position = new Vector3(954, 281, 0);
            inputPress1.transform.localScale = new Vector3(0.23f, 0.23f, 0.23f);
            inputPress1.GetComponent<Image>().enabled = true;

            inputPress2.transform.position = new Vector3(1025, 315, 0);
            inputPress2.transform.localScale = new Vector3(0.23f, 0.23f, 0.23f);
            inputPress2.GetComponent<Image>().enabled = true;

            inputPress3.transform.position = new Vector3(1092, 349, 0);
            inputPress3.transform.localScale = new Vector3(0.23f, 0.23f, 0.23f);
            inputPress3.GetComponent<Image>().enabled = true;

            inputPress4.transform.position = new Vector3(1164, 384, 0);
            inputPress4.transform.localScale = new Vector3(0.23f, 0.23f, 0.23f);
            inputPress4.GetComponent<Image>().enabled = true;

            inputPress5.transform.position = new Vector3(1236, 420, 0);
            inputPress5.transform.localScale = new Vector3(0.23f, 0.23f, 0.23f);
            inputPress5.GetComponent<Image>().enabled = true;

            inputPress6.transform.position = new Vector3(1306, 454, 0);
            inputPress6.transform.localScale = new Vector3(0.23f, 0.23f, 0.23f);
            inputPress6.GetComponent<Image>().enabled = true;

            inputPress7.transform.position = new Vector3(1376, 490, 0);
            inputPress7.transform.localScale = new Vector3(0.23f, 0.23f, 0.23f);
            inputPress7.GetComponent<Image>().enabled = true;
        }
    }
}