﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CombatEndTurn : MonoBehaviour
{
    private levelManager managerLevel;

    private GameObject canvasCombo, canvasPunch, cameraMain, cameraPunch, canvasMoving, canvasCombat;

    private Pathfinding.CharacterStats statsEvelyn, statsAsha, statsHenchman1, statsHenchman2;

    private Pathfinding.AttackAi iaAttack;

    private Pathfinding.TurnManagement managerTurn;

    private ManagerPunchscreen punchscreenManager;

    public Pathfinding.CharacterStats statsCharacterAttacking, statsCharacterAttacked;
    public Pathfinding.TurnManagement turnManager;

    private GameObject characterAttacked;

    private ManagerUIScreens manageUIscreens;

    private SpriteRenderer backgroundPunchSprite, outlineBackgroundPunchSprite, aboveBackgroundPunchSprite;

    public bool needNewTurn;

    private AudioSource sourceTransition;

    private ContainerAudioClip scriptContainer;

    private Pathfinding.TurnManagement scriptTurnManagement;

    // Start is called before the first frame update
    void Start()
    {
        managerLevel = GameObject.Find("GameManager").GetComponent<levelManager>();
        canvasCombo = GameObject.Find("CanvasCombo");
        canvasPunch = GameObject.Find("CanvasPunch");
        canvasMoving = GameObject.Find("CanvasMoving");
        canvasCombat = GameObject.Find("CanvasCombat");
        cameraMain = GameObject.Find("Main Camera");
        cameraPunch = GameObject.Find("CameraUIOnly");

        statsEvelyn = GameObject.Find("Evelyn").GetComponent<Pathfinding.CharacterStats>();
        statsAsha = GameObject.Find("Asha").GetComponent<Pathfinding.CharacterStats>();
        statsHenchman1 = GameObject.Find("Sbire1").GetComponent<Pathfinding.CharacterStats>();
        statsHenchman2 = GameObject.Find("Sbire2").GetComponent<Pathfinding.CharacterStats>();

        iaAttack = GameObject.Find("Units").GetComponent<Pathfinding.AttackAi>();
        managerTurn = GameObject.Find("GameManager").GetComponent<Pathfinding.TurnManagement>();

        punchscreenManager = GameObject.Find("CanvasPunch").GetComponent<ManagerPunchscreen>();
        manageUIscreens = GameObject.Find("GameManager").GetComponent<ManagerUIScreens>();

        backgroundPunchSprite = GameObject.Find("Background_Canvas_Punch").GetComponent<SpriteRenderer>();
        outlineBackgroundPunchSprite = GameObject.Find("Background_Contours_Canvas_Punch").GetComponent<SpriteRenderer>();
        aboveBackgroundPunchSprite = GameObject.Find("Above_Background_Canvas_Punch").GetComponent<SpriteRenderer>();

        needNewTurn = false;

        sourceTransition = GameObject.Find("SFX_Transition").GetComponent<AudioSource>();
        scriptContainer = GameObject.Find("SoundManager").GetComponent<ContainerAudioClip>();

        scriptTurnManagement = GameObject.Find("GameManager").GetComponent<Pathfinding.TurnManagement>();

    }

    //Updating variables at the right moment
    public void WhoIsConcerns()
    {
        if(managerLevel.currentCharacterAttacked == "Evelyn")
        {
            statsCharacterAttacked = statsEvelyn;
        }
        else if(managerLevel.currentCharacterAttacked == "Asha")
        {
            statsCharacterAttacked = statsAsha;
        }
        else if(managerLevel.currentCharacterAttacked == "Sbire1")
        {
            statsCharacterAttacked = statsHenchman1;
        }
        else if(managerLevel.currentCharacterAttacked == "Sbire2")
        {
            statsCharacterAttacked = statsHenchman2;
        }

        if (managerLevel.currentCharacterAttacking == "Evelyn")
        {
            statsCharacterAttacking = statsEvelyn;
        }
        else if (managerLevel.currentCharacterAttacking == "Asha")
        {
            statsCharacterAttacking = statsAsha;
        }
        else if (managerLevel.currentCharacterAttacking == "Sbire1")
        {
            statsCharacterAttacking = statsHenchman1;
        }
        else if (managerLevel.currentCharacterAttacking == "Sbire2")
        {
            statsCharacterAttacking = statsHenchman2;
        }
    }

    //Function calls after all vfx have appeared
    //Manage what happens after. If the current character attacked is an enemy and he is dead, go to moving phase
    //If the current character attacked is Evelyn and she is dead, game over
    // If no one is dead, go to another Function
    public void IsCharacterAttackedDead()
    {
        WhoIsConcerns();

        canvasPunch.GetComponent<Animator>().Rebind();
        canvasCombo.GetComponent<Pathfinding.InputGauge>().inputs.Clear();
        iaAttack.isComboFinished = false;
        canvasCombo.GetComponent<Animator>().SetTrigger("Return");
        canvasCombo.GetComponent<Pathfinding.comboManager>().inputs.Clear();
        punchscreenManager.isVFXLaunched = false;
        managerLevel.isInPunchScreen = false;

        if (managerLevel.currentCharacterAttacked == "Asha" && statsCharacterAttacked.HealthPoints <= 0)
        {
            sourceTransition.clip = scriptContainer.sfxWin;
            sourceTransition.Play();
            managerLevel.victory = true;
            //Debug.Log("La partie est gagnée par le joueur");
            EndGame();
            return;
        }
        else if (managerLevel.currentCharacterAttacked == "Evelyn" && statsCharacterAttacked.HealthPoints <= 0)
        {
            sourceTransition.clip = scriptContainer.sfxLose;
            sourceTransition.Play();
            managerLevel.defeat = true;
            //Debug.Log("La partie est perdue, j'en suis bien navré");
            EndGame();
            return;
        }
        else if (managerLevel.currentCharacterAttacked.Contains("Sbire") && statsCharacterAttacked.HealthPoints <= 0)
        {
            characterAttacked = GameObject.Find(managerLevel.currentCharacterAttacked);
            ReturnInMovingPhase();
        }
        else if (statsCharacterAttacked.HealthPoints > 0)
        {
            NewTurn();
        }
        
    }

    //Function called when character attacked is not dead and has action points
    public void NewTurn()
    {
        statsCharacterAttacking.nombreActions -= 1;

        string tmpAttacked = managerLevel.currentCharacterAttacked;
        string tmpAttacking = managerLevel.currentCharacterAttacking;

        if (managerLevel.currentCharacterAttacked == "Evelyn")
        {
            Debug.Log("C'est Evelyn qui a été attaquée");
            managerLevel.actionsEnemy -= 1;

            if (statsCharacterAttacked.nombreActions > 0 && statsCharacterAttacked.Adrenaline > 0)
            {
                managerLevel.currentCharacterAttacked = tmpAttacking;
                managerLevel.currentCharacterAttacking = tmpAttacked;

                EvelynAttack();

            }
            else if (statsCharacterAttacked.nombreActions == 0 || statsCharacterAttacked.Adrenaline == 0)
            {

                Debug.Log("Evelyn n'a plus de points d'actions. Que pouvons-nous faire?");
                if (statsCharacterAttacking.nombreActions > 0)
                {
                    //L'ennemi continue à donner des coups
                    Enemyattack();
                    Debug.Log("L'ennemi quant à lui a encore des points d'actions. Il va pouvoir attaquer de nouveau");
                }
                else if(statsCharacterAttacking.nombreActions == 0)
                {
                    Debug.Log("L'ennemi ne peut plus aussi attaquer. Retour à la phase de déplacement");
                    tmpAttacked = "";
                    tmpAttacking = "";
                    needNewTurn = true;
                    ReturnInMovingPhase();
                }

            }
        }
        else if (managerLevel.currentCharacterAttacked == "Asha" || managerLevel.currentCharacterAttacked == "Sbire1" || managerLevel.currentCharacterAttacked == "Sbire2")
        {
            //Debug.Log("C'est un des ennemis qui est attaqué");
            managerLevel.actionsEvelyn -= 1;  

            if (statsCharacterAttacked.nombreActions > 0)
            {
                //Debug.Log("L'ennemi attaqué a encore des points d'actions. Il va pouvoir répliquer");

                //Debug.Log("1 statscharacterAttacked: " + statsCharacterAttacked);
                //Debug.Log("1 statsCharacterAttacking: " + statsCharacterAttacking);
                //Debug.Log("1 managerLevel.currentCharacterAttacked: " + managerLevel.currentCharacterAttacked);
                //Debug.Log("1 managerLevel.currentCharacterAttacking: " + managerLevel.currentCharacterAttacking);
                //Debug.Log("1 tmpAttacked: "+ tmpAttacked);
                //Debug.Log("1 tmpAttacking: " + tmpAttacking);
                managerLevel.currentCharacterAttacked = tmpAttacking;
                managerLevel.currentCharacterAttacking = tmpAttacked;

                WhoIsConcerns();

                //Debug.Log("2 statscharacterAttacked:" + statsCharacterAttacked);
                //Debug.Log("2 statsCharacterAttacking:" + statsCharacterAttacking);
                //Debug.Log("2 managerLevel.currentCharacterAttacked:" + managerLevel.currentCharacterAttacked);
                //Debug.Log("2 managerLevel.currentCharacterAttacking:" + managerLevel.currentCharacterAttacking);
                //Debug.Log("2 tmpAttacked: " + tmpAttacked);
                //Debug.Log("2 tmpAttacking: " + tmpAttacking);

                Enemyattack();
            }

            if (statsCharacterAttacked.nombreActions <= 0)
            {
                //Debug.Log("L'ennemi attaqué n'a plus de points. Comment faire?");

                if(statsCharacterAttacking.nombreActions > 0 && statsCharacterAttacking.Adrenaline > 0)
                {
                    //Evelyn peut attaquer encore car l'ennemi n'a plus de points d'actions
                    EvelynAttack();
                    //Debug.Log("Evelyn a encore des points d'actions. Elle peut donc encore attaquer");
                }
                else if(statsCharacterAttacking.nombreActions == 0 || statsCharacterAttacking.Adrenaline == 0)
                {
                    //Debug.Log("Evelyn n'a elle aussi plus de points d'action. Retour à la phase de déplacement");
                    tmpAttacked = "";
                    tmpAttacking = "";
                    needNewTurn = true;
                    ReturnInMovingPhase();
                }

            }

        }
    }

    public void ReturnInMovingPhase()
    {
        sourceTransition.clip = scriptContainer.sfxTransiCombatOut;
        sourceTransition.Play();

        characterAttacked = GameObject.Find(managerLevel.currentCharacterAttacked);
        managerLevel.currentCharacterAttacked = "";
        managerLevel.currentCharacterAttacking = "";
        managerLevel.actionsEnemy = 0;
        managerLevel.actionsEvelyn = 0;
        DisableBackgroundsCanvasPunch();
        managerLevel.isInMovingPhase = true;
        managerLevel.isInPunchScreen = false;

        if(managerLevel.giftEvelynActivated)
        {
            managerLevel.giftEvelynActivated = false;
            GameObject.Find("Icone_Don").GetComponent<Image>().enabled = false;
        }

        Debug.Log(characterAttacked);
        if (characterAttacked.GetComponent<Pathfinding.CharacterStats>().HealthPoints <= 0)
        {
            turnManager.UnitDeath(characterAttacked);
            characterAttacked = null;
        }
        //scriptTurnManagement.EndOfTurn();
        manageUIscreens.FromCombatToMovingPhase();
        //canvasCombo.GetComponent<Animator>().Rebind();
        //canvasCombo.GetComponent<Animator>().enabled = false;
    }

    public void Enemyattack()
    {
        canvasCombo.GetComponent<Pathfinding.comboManager>().inputs.Clear();
        manageUIscreens.FromPunchToCombo();
        DisableBackgroundsCanvasPunch();
        manageUIscreens.ClearCanvasComboForEnemies();
        //canvasCombo.GetComponent<Animator>().enabled = true;
        //canvasCombo.GetComponent<Animator>().Rebind();
        Debug.Log("YO");
        iaAttack.BeginAttack();
        /*
        if (managerLevel.currentCharacterAttacking == "Sbire1")
        {
            iaAttack.AttackSbire();
            return;
        }
        else if (managerLevel.currentCharacterAttacking == "Sbire2")
        {
            iaAttack.AttackSbire();
            return;
        }
        else if (managerLevel.currentCharacterAttacking == "Asha")
        {
            iaAttack.Attackasha();
            return;
        }
        */
    }

    public void EvelynAttack()
    {
        canvasCombo.GetComponent<Pathfinding.comboManager>().inputs.Clear();
        //canvasCombo.GetComponent<Animator>().Rebind();
        //canvasCombo.GetComponent<Animator>().enabled = false;
        manageUIscreens.FromPunchToCombat();
        DisableBackgroundsCanvasPunch();
        //iaAttack.enabled = false;
        GameObject.Find("GameManager").GetComponent<ManagerUIScreens>().ClearCanvasComboForEvelyn();
    }

    public void DisableBackgroundsCanvasPunch()
    {
        backgroundPunchSprite.enabled = false;
        aboveBackgroundPunchSprite.enabled = false;
        outlineBackgroundPunchSprite.enabled = false;
    }

    public void EndGame()
    {
        canvasPunch.GetComponent<CanvasGroup>().alpha = 0;
        GameObject.Find("Background_Canvas_Punch").GetComponent<SpriteRenderer>().enabled = false;
        GameObject.Find("Above_Background_Canvas_Punch").GetComponent<SpriteRenderer>().enabled = false;
        GameObject.Find("Background_Contours_Canvas_Punch").GetComponent<SpriteRenderer>().enabled = false;
        canvasMoving.GetComponent<CanvasGroup>().alpha = 1;
        GameObject.Find("CameraUIOnly").GetComponent<Camera>().enabled = false;
        GameObject.Find("Main Camera").GetComponent<Camera>().enabled = true;
        canvasMoving.GetComponent<Animator>().SetTrigger("DisappearUILeft");

        if(canvasMoving.GetComponent<MovingManager>().isRightUIAppeared)
            canvasMoving.GetComponent<Animator>().SetTrigger("DisappearUIRight");
    }

}
