﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManagerCounterAttack : MonoBehaviour
{
    public List<string> inputs = new List<string>();

    public List<GameObject> vfxLeftGO = new List<GameObject>();
    public List<GameObject> vfxRightGO = new List<GameObject>();
    public List<GameObject> vfxTopGO = new List<GameObject>();
    public List<GameObject> vfxBottomGO = new List<GameObject>();

    public string currentInputPlayer;

    private Pathfinding.CharacterStats statsAsha, statsEvelyn, statsSbire1, statsSbire2;

    private levelManager managerLevel;

    private int valueCombo, damageAttack;

    public Sprite brokenShield, normalShield;

    private Image currentInputTop, currentInputBottom, currentInputRight, currentInputLeft, imageDeflected, tokenCounterAttack;

    private SpriteRenderer shield;

    private GameObject edgeCurrentInput, canvasPunch, canvasCombat, canvasMoving, characterAttacking;

    private Vector3 edgeOriginalScale, edgeDestinationScale;

    private float currentTime, timeForQTE;

    public bool qteFailed;

    private bool firstTry, justOneTime;

    private CombatEndTurn scriptEndTurn;

    private AudioSource sfxPunch0, sfxPunch1, sfxPunch2, sfxPunch3, sfxPunch4, sfxPunch5, sfxPunch6, sfxPunch7, sfxRiposte, sourceTransition;

    private ContainerAudioClip scriptContainer;

    private Pathfinding.TurnManagement turnManager;

    private ManagerUIScreens manageUIscreens;

    public float shakeMagnitude = 0.05f, shakeTime = 0.5f;

    public Camera cameraUI;

    Vector3 cameraInitialPosition;

    // Start is called before the first frame update
    void Start()
    {
        statsAsha = GameObject.Find("Asha").GetComponent<Pathfinding.CharacterStats>();
        statsEvelyn = GameObject.Find("Evelyn").GetComponent<Pathfinding.CharacterStats>();
        statsSbire1 = GameObject.Find("Sbire1").GetComponent<Pathfinding.CharacterStats>();
        statsSbire2 = GameObject.Find("Sbire2").GetComponent<Pathfinding.CharacterStats>();

        managerLevel = GameObject.Find("GameManager").GetComponent<levelManager>();

        currentInputTop = GameObject.Find("Current_Input_Top").GetComponent<Image>();
        currentInputBottom = GameObject.Find("Current_Input_Bottom").GetComponent<Image>();
        currentInputLeft = GameObject.Find("Current_Input_Left").GetComponent<Image>();
        currentInputRight = GameObject.Find("Current_Input_Right").GetComponent<Image>();

        imageDeflected = GameObject.Find("Image_Deflected").GetComponent<Image>();
        tokenCounterAttack = GameObject.Find("Icone_Riposte").GetComponent<Image>();
        shield = GameObject.Find("Shield_Center").GetComponent<SpriteRenderer>();

        edgeCurrentInput = GameObject.Find("Edge_Input");
        edgeOriginalScale = GameObject.Find("Edge_Input").GetComponent<Transform>().transform.localScale;
        edgeDestinationScale = new Vector3(1.0f, 1.0f, 1.0f);

        canvasCombat = GameObject.Find("CanvasCombat");
        canvasPunch = GameObject.Find("CanvasPunch");
        canvasMoving = GameObject.Find("CanvasMoving");

        scriptEndTurn = GameObject.Find("GameManager").GetComponent<CombatEndTurn>();

        valueCombo = 0;
        currentTime = 0.0f;
        timeForQTE = 0.0f;
        qteFailed = false;

        sfxPunch0 = GameObject.Find("SFX_VFX_00").GetComponent<AudioSource>();
        sfxPunch1 = GameObject.Find("SFX_VFX_01").GetComponent<AudioSource>();
        sfxPunch2 = GameObject.Find("SFX_VFX_02").GetComponent<AudioSource>();
        sfxPunch3 = GameObject.Find("SFX_VFX_03").GetComponent<AudioSource>();
        sfxPunch4 = GameObject.Find("SFX_VFX_04").GetComponent<AudioSource>();
        sfxPunch5 = GameObject.Find("SFX_VFX_05").GetComponent<AudioSource>();
        sfxPunch6 = GameObject.Find("SFX_VFX_06").GetComponent<AudioSource>();
        sfxPunch7 = GameObject.Find("SFX_VFX_07").GetComponent<AudioSource>();
        sourceTransition = GameObject.Find("SFX_Transition").GetComponent<AudioSource>();

        sfxRiposte = GameObject.Find("SFX_Riposte").GetComponent<AudioSource>();
        scriptContainer = GameObject.Find("SoundManager").GetComponent<ContainerAudioClip>();

        turnManager = GameObject.Find("GameManager").GetComponent<Pathfinding.TurnManagement>();

        manageUIscreens = GameObject.Find("GameManager").GetComponent<ManagerUIScreens>();

        cameraInitialPosition = cameraUI.transform.position;
    }

    private void Update()
    {
        /*
        if(qteFailed)
        {
            StopCoroutine(QTE());
            StartCoroutine(LaunchVFX());
            //Appeler une fonction pour lorsque le joueur ne réussit pas la riposte
        }
        */
    }

    public void StartCameraShaking()
    {
        float cameraShakingOffsetX = Random.value * shakeMagnitude * 2 - shakeMagnitude;
        float cameraShakingOffsetY = Random.value * shakeMagnitude * 2 - shakeMagnitude;
        Vector3 cameraIntermediatePosition = cameraUI.transform.position;
        cameraIntermediatePosition.x += cameraShakingOffsetX;
        cameraIntermediatePosition.y += cameraShakingOffsetY;
        cameraUI.transform.position = cameraIntermediatePosition;
    }

    public void StopCameraShaking()
    {
        CancelInvoke("StartCameraShaking");
        cameraUI.transform.position = cameraInitialPosition;
    }

    public void ChooseAttack()
    {
        valueCombo = 0;
        currentTime = 0.0f;
        timeForQTE = 0.0f;
        inputs.Clear();
        qteFailed = false;
        firstTry = false;
        edgeCurrentInput.GetComponent<Image>().enabled = true;
        shield.enabled = false;
        imageDeflected.enabled = false;
        justOneTime = false;

        if(managerLevel.currentCharacterAttacking.Contains("Sbire"))
        {
            if (statsEvelyn.HealthPoints >= 50)
            {
                inputs.Add("Haut");
                inputs.Add("Bas");
                inputs.Add("Bas");
                inputs.Add("Gauche");

                damageAttack = 18;

            }
            else if (statsEvelyn.HealthPoints < 50)
            {
                valueCombo = Random.Range(1, 3);

                if (valueCombo == 1)
                {
                    inputs.Add("Gauche");
                    inputs.Add("Haut");

                    damageAttack = 17;

                }
                else if (valueCombo == 2)
                {
                    inputs.Add("Bas");
                    inputs.Add("Bas");
                    inputs.Add("Bas");
                    inputs.Add("Bas");
                    inputs.Add("Bas");
                    damageAttack = 10;
                }
                else if (valueCombo == 3)
                {
                    inputs.Add("Haut");
                    inputs.Add("Bas");
                    inputs.Add("Bas");
                    damageAttack = 18;
                }

            }
        }
        else if(managerLevel.currentCharacterAttacking == "Asha")
        {
            if (statsEvelyn.HealthPoints >= 50)
            {
                inputs.Add("Gauche");
                inputs.Add("Droite");
                inputs.Add("Bas");
                inputs.Add("Bas");
                inputs.Add("Droite");
                damageAttack = 42;
            }
            else if (statsEvelyn.HealthPoints < 50)
            {
                valueCombo = Random.Range(1, 4);

                if (valueCombo == 1)
                {
                    inputs.Add("Bas");
                    inputs.Add("Bas");
                    inputs.Add("Haut");
                    inputs.Add("Haut");
                    damageAttack = 30;
                }
                else if (valueCombo == 2)
                {
                    inputs.Add("Droite");
                    inputs.Add("Gauche");
                    inputs.Add("Droite");
                    inputs.Add("Gauche");
                    damageAttack = 33;

                }
                else if (valueCombo == 3)
                {
                    inputs.Add("Haut");
                    inputs.Add("Bas");
                    inputs.Add("Haut");
                    inputs.Add("Bas");
                    inputs.Add("Droite");
                    inputs.Add("Bas");
                    damageAttack = 38;
                }
                else if (valueCombo == 4)
                {
                    inputs.Add("Gauche");
                    inputs.Add("Droite");
                    inputs.Add("Haut");
                    inputs.Add("Bas");
                    inputs.Add("Bas");
                    inputs.Add("Droite");
                    damageAttack = 42;
                }

            }
        }

        StartCoroutine(QTE());
    }

    public void ApparitionEnemies()
    {
        if(managerLevel.currentCharacterAttacking == "Asha")
        {
            gameObject.GetComponent<Animator>().SetTrigger("ashaApparition");
        }
        else if(managerLevel.currentCharacterAttacking.Contains("Sbire"))
        {
            gameObject.GetComponent<Animator>().SetTrigger("henchmanApparition");
        }
    }

    public void DisparitionEnemies()
    {
        if (managerLevel.currentCharacterAttacking == "Asha")
        {
            gameObject.GetComponent<Animator>().SetTrigger("ashaDisparition");
        }
        else if (managerLevel.currentCharacterAttacking.Contains("Sbire"))
        {
            gameObject.GetComponent<Animator>().SetTrigger("henchmanDisparition");
        }
    }

    public IEnumerator QTE()
    {
        for(int i = 0; i < inputs.Count; i++)
        {
            
            currentInputPlayer = "";

            if (qteFailed)
            {
                shield.sprite = brokenShield;
                shield.enabled = true;
                tokenCounterAttack.enabled = false;
                //StopCoroutine(QTE());
            }

            if(!qteFailed)
            {
                if (inputs[i] == "Bas")
                {
                    currentInputTop.enabled = false;
                    currentInputBottom.enabled = true;
                    currentInputLeft.enabled = false;
                    currentInputRight.enabled = false;
                }
                else if (inputs[i] == "Haut")
                {

                    currentInputTop.enabled = true;
                    currentInputBottom.enabled = false;
                    currentInputLeft.enabled = false;
                    currentInputRight.enabled = false;
                }
                else if (inputs[i] == "Gauche")
                {
                    currentInputTop.enabled = false;
                    currentInputBottom.enabled = false;
                    currentInputLeft.enabled = true;
                    currentInputRight.enabled = false;
                }
                else if (inputs[i] == "Droite")
                {
                    currentInputTop.enabled = false;
                    currentInputBottom.enabled = false;
                    currentInputLeft.enabled = false;
                    currentInputRight.enabled = true;
                }

                timeForQTE = UnityEngine.Random.Range(1f, 2f);

                do
                {
                    edgeCurrentInput.transform.localScale = Vector3.Lerp(edgeOriginalScale, edgeDestinationScale, currentTime / timeForQTE);
                    currentTime += Time.deltaTime;

                    if (Input.GetKeyDown(KeyCode.Z) || Input.GetKeyDown(KeyCode.UpArrow))
                    {
                        currentInputPlayer = "Haut";
                    }
                    else if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
                    {
                        currentInputPlayer = "Bas";
                    }
                    else if (Input.GetKeyDown(KeyCode.Q) || Input.GetKeyDown(KeyCode.LeftArrow))
                    {
                        currentInputPlayer = "Gauche";
                    }
                    else if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
                    {
                        currentInputPlayer = "Droite";
                    }

                    yield return null;
                }
                while (currentTime < timeForQTE);

                currentTime = 0.0f;

                if (currentInputPlayer != inputs[i])
                {
                    qteFailed = true;
                    sfxRiposte.clip = scriptContainer.sfxQTEFail;
                    sfxRiposte.Play();
                    Debug.Log("Tu viens de rater le QTE gros nul");
                    StopAllCoroutines();
                    StartCoroutine(LaunchVFX());


                }

                if (qteFailed == false)
                {
                    sfxRiposte.clip = scriptContainer.sfxQTEWin;
                    sfxRiposte.Play(); 
                }
            }
        }

        currentInputTop.enabled = false;
        currentInputBottom.enabled = false;
        currentInputLeft.enabled = false;
        currentInputRight.enabled = false;
        edgeCurrentInput.GetComponent<Image>().enabled = false;

        if(!qteFailed)
        {
            sfxRiposte.clip = scriptContainer.sfxQTEFullWin;
            sfxRiposte.Play();
            shield.sprite = normalShield;
            ApparitionEnemies();

        }
    }

    public IEnumerator LaunchVFX()
    {
        shield.enabled = true;

        if(!justOneTime)
        {
            justOneTime = true;

            if (qteFailed)
            {

                foreach (GameObject gameobjectGO in vfxLeftGO)
                {
                    gameobjectGO.transform.localScale = new Vector3(120, 120, 120);
                }
                foreach (GameObject gameobjectGO in vfxRightGO)
                {
                    gameobjectGO.transform.localScale = new Vector3(120, 120, 120);
                }
                foreach (GameObject gameobjectGO in vfxTopGO)
                {
                    gameobjectGO.transform.localScale = new Vector3(120, 120, 120);
                }
                foreach (GameObject gameobjectGO in vfxBottomGO)
                {
                    gameobjectGO.transform.localScale = new Vector3(120, 120, 120);
                }

            }
            else
            {
                imageDeflected.enabled = true;

                foreach (GameObject gameobjectGO in vfxLeftGO)
                {
                    gameobjectGO.transform.localScale = new Vector3(60, 60, 60);
                }
                foreach (GameObject gameobjectGO in vfxRightGO)
                {
                    gameobjectGO.transform.localScale = new Vector3(60, 60, 60);
                }
                foreach (GameObject gameobjectGO in vfxTopGO)
                {
                    gameobjectGO.transform.localScale = new Vector3(60, 60, 60);
                }
                foreach (GameObject gameobjectGO in vfxBottomGO)
                {
                    gameobjectGO.transform.localScale = new Vector3(60, 60, 60);
                }

            }

            for (int i = 0; i < inputs.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        sfxPunch0.Play();
                        break;
                    case 1:
                        sfxPunch1.Play();
                        break;
                    case 2:
                        sfxPunch2.Play();
                        break;
                    case 3:
                        sfxPunch3.Play();
                        break;
                    case 4:
                        sfxPunch4.Play();
                        break;
                    case 5:
                        sfxPunch5.Play();
                        break;
                    case 6:
                        sfxPunch6.Play();
                        break;
                    case 7:
                        sfxPunch7.Play();
                        break;
                }

                if (inputs[i] == "Gauche")
                {
                    switch (i)
                    {
                        case 0:
                            vfxLeftGO[0].GetComponent<ParticleSystem>().Play();
                            break;

                        case 1:
                            vfxLeftGO[1].GetComponent<ParticleSystem>().Play();
                            break;

                        case 2:
                            vfxLeftGO[2].GetComponent<ParticleSystem>().Play();
                            break;

                        case 3:
                            vfxLeftGO[3].GetComponent<ParticleSystem>().Play();
                            break;

                        case 4:
                            vfxLeftGO[4].GetComponent<ParticleSystem>().Play();
                            break;

                        case 5:
                            vfxLeftGO[5].GetComponent<ParticleSystem>().Play();
                            break;

                        case 6:
                            vfxLeftGO[6].GetComponent<ParticleSystem>().Play();
                            break;

                        case 7:
                            vfxLeftGO[7].GetComponent<ParticleSystem>().Play();
                            break;
                    }
                }
                else if (inputs[i] == "Bas")
                {
                    switch (i)
                    {
                        case 0:
                            vfxBottomGO[0].GetComponent<ParticleSystem>().Play();
                            break;

                        case 1:
                            vfxBottomGO[1].GetComponent<ParticleSystem>().Play();
                            break;

                        case 2:
                            vfxBottomGO[2].GetComponent<ParticleSystem>().Play();
                            break;

                        case 3:
                            vfxBottomGO[3].GetComponent<ParticleSystem>().Play();
                            break;

                        case 4:
                            vfxBottomGO[4].GetComponent<ParticleSystem>().Play();
                            break;

                        case 5:
                            vfxBottomGO[5].GetComponent<ParticleSystem>().Play();
                            break;

                        case 6:
                            vfxBottomGO[6].GetComponent<ParticleSystem>().Play();
                            break;

                        case 7:
                            vfxBottomGO[7].GetComponent<ParticleSystem>().Play();
                            break;
                    }
                }
                else if (inputs[i] == "Droite")
                {
                    switch (i)
                    {
                        case 0:
                            vfxRightGO[0].GetComponent<ParticleSystem>().Play();
                            break;

                        case 1:
                            vfxRightGO[1].GetComponent<ParticleSystem>().Play();
                            break;

                        case 2:
                            vfxRightGO[2].GetComponent<ParticleSystem>().Play();
                            break;

                        case 3:
                            vfxRightGO[3].GetComponent<ParticleSystem>().Play();
                            break;

                        case 4:
                            vfxRightGO[4].GetComponent<ParticleSystem>().Play();
                            break;

                        case 5:
                            vfxRightGO[5].GetComponent<ParticleSystem>().Play();
                            break;

                        case 6:
                            vfxRightGO[6].GetComponent<ParticleSystem>().Play();
                            break;

                        case 7:
                            vfxRightGO[7].GetComponent<ParticleSystem>().Play();
                            break;
                    }
                }
                else if (inputs[i] == "Haut")
                {
                    switch (i)
                    {
                        case 0:
                            vfxTopGO[0].GetComponent<ParticleSystem>().Play();
                            break;

                        case 1:
                            vfxTopGO[1].GetComponent<ParticleSystem>().Play();
                            break;

                        case 2:
                            vfxTopGO[2].GetComponent<ParticleSystem>().Play();
                            break;

                        case 3:
                            vfxTopGO[3].GetComponent<ParticleSystem>().Play();
                            break;

                        case 4:
                            vfxTopGO[4].GetComponent<ParticleSystem>().Play();
                            break;

                        case 5:
                            vfxTopGO[5].GetComponent<ParticleSystem>().Play();
                            break;

                        case 6:
                            vfxTopGO[6].GetComponent<ParticleSystem>().Play();
                            break;

                        case 7:
                            vfxTopGO[7].GetComponent<ParticleSystem>().Play();
                            break;
                    }

                }

                InvokeRepeating("StartCameraShaking", 0f, 0.005f);
                Invoke("StopCameraShaking", shakeTime);

                WaitForSeconds wait = new WaitForSeconds(0.20f);
                yield return wait;
            }

            if (!qteFailed)
                DisparitionEnemies();
            else
                EndCounterAttack();
        }

        
    }

    public void EndCounterAttack()
    {
        if (managerLevel.currentCharacterAttacking == "Asha")
        {
            statsAsha.nombreActions -= 1;
            managerLevel.actionsEnemy -= 1;

            if (!qteFailed)
            {
                statsAsha.HealthPoints -= (int)(damageAttack * 0.25f);

                if(statsAsha.HealthPoints <= 0)
                {
                    //Que faire si Asha n'a plus de points de vie?
                    Debug.Log("Salut");
                    sourceTransition.clip = scriptContainer.sfxWin;
                    sourceTransition.Play();
                    managerLevel.isInPunchScreen = false;
                    managerLevel.isInMovingPhase = true;
                    managerLevel.victory = true;
                    canvasPunch.GetComponent<CanvasGroup>().alpha = 0;
                    GameObject.Find("Background_Canvas_Punch").GetComponent<SpriteRenderer>().enabled = false;
                    GameObject.Find("Above_Background_Canvas_Punch").GetComponent<SpriteRenderer>().enabled = false;
                    GameObject.Find("Background_Contours_Canvas_Punch").GetComponent<SpriteRenderer>().enabled = false;
                    canvasMoving.GetComponent<CanvasGroup>().alpha = 1;
                    canvasMoving.GetComponent<Animator>().SetTrigger("DisappearUILeft");

                    if (canvasMoving.GetComponent<MovingManager>().isRightUIAppeared)
                    {
                        canvasMoving.GetComponent<Animator>().SetTrigger("DisappearUIRight");
                    }
                    return;
                }

                if(statsAsha.nombreActions > 0)
                {
                    StartCoroutine(OneMoreTime());
                }
                else if(statsAsha.nombreActions == 0 && statsEvelyn.nombreActions > 0)
                {
                    ReturnInCanvasCombat();
                }
                else
                {
                    scriptEndTurn.ReturnInMovingPhase();
                }
            }
        }
        else if(managerLevel.currentCharacterAttacking == "Sbire1")
        {
            statsSbire1.nombreActions -= 1;
            managerLevel.actionsEnemy -= 1;

            if (!qteFailed)
            {
                statsSbire1.HealthPoints -= (int)(damageAttack * 0.25f);

                if (statsSbire1.HealthPoints <= 0)
                {
                    //Que faire si Sbire1 n'a plus de points de vie?
                    HenchmanDeath();
                    return;
                }

                if(statsSbire1.nombreActions > 0)
                {
                    StartCoroutine(OneMoreTime());
                }
                else if (statsSbire1.nombreActions == 0 && statsEvelyn.nombreActions > 0)
                {
                    ReturnInCanvasCombat();
                }
                else
                {
                    scriptEndTurn.ReturnInMovingPhase();
                }
            }
        }
        else if(managerLevel.currentCharacterAttacking == "Sbire2")
        {
            statsSbire2.nombreActions -= 1;
            managerLevel.actionsEnemy -= 1;

            if (!qteFailed)
            {
                statsSbire2.HealthPoints -= (int)(damageAttack * 0.25f);

                if (statsSbire2.HealthPoints <= 0)
                {
                    //Que faire si le sbire 2 décède?
                    HenchmanDeath();
                    return;
                }

                if(statsSbire2.nombreActions > 0)
                {
                    StartCoroutine(OneMoreTime());
                }
                else if (statsSbire2.nombreActions == 0 && statsEvelyn.nombreActions > 0)
                {
                    ReturnInCanvasCombat();
                }
                else
                {
                    scriptEndTurn.ReturnInMovingPhase();
                }
            }
        }

        if(qteFailed && !firstTry)
        {
            statsEvelyn.HealthPoints -= damageAttack;
            managerLevel.isInPunchScreen = false;
            managerLevel.isInCombatScreen = true;

            if(statsEvelyn.HealthPoints <= 0)
            {
                //Que faire quand Evelyn a raté sa contre-attaque et qu'elle meurt?
                sourceTransition.clip = scriptContainer.sfxLose;
                sourceTransition.Play();
                managerLevel.defeat = true;
                scriptEndTurn.EndGame();
                return;
            }

            if(statsEvelyn.nombreActions > 0)
            {
                ReturnInCanvasCombat();
            }
            else if(statsEvelyn.nombreActions <= 0)
            {
                Debug.Log("Tu as perdu gros nul");
                return;
            }
            firstTry = true;
        }
    }

    public IEnumerator OneMoreTime()
    {
        WaitForSeconds wait = new WaitForSeconds(0.5f);
        yield return wait;
        ChooseAttack();
    }

    public void ReturnInCanvasCombat()
    {
        if (managerLevel.currentCharacterAttacking == "Asha")
        {
            managerLevel.currentCharacterAttacked = "Asha";
        }
        else if(managerLevel.currentCharacterAttacking == "Sbire1")
        {
            managerLevel.currentCharacterAttacked = "Sbire1";
        }
        else if(managerLevel.currentCharacterAttacking == "Sbire2")
        {
            managerLevel.currentCharacterAttacked = "Sbire2";
        }

        managerLevel.currentCharacterAttacking = "Evelyn";

        DisableImages();
        imageDeflected.enabled = false;
        canvasCombat.GetComponent<Pathfinding.BarreDadrenaline>().AdrenalineGaugeVisually();

        managerLevel.isInPunchScreen = false;
        managerLevel.isInCombatScreen = true;
        GameObject.Find("GameManager").GetComponent<ManagerUIScreens>().FromPunchToCombat();

    }

    public void DisableImages()
    {
        edgeCurrentInput.GetComponent<Image>().enabled = false;
        currentInputLeft.enabled = false;
        currentInputRight.enabled = false;
        currentInputTop.enabled = false;
        currentInputBottom.enabled = false;
        shield.enabled = false;
    }

    public void HenchmanDeath()
    {
        characterAttacking = GameObject.Find(managerLevel.currentCharacterAttacking);
        managerLevel.currentCharacterAttacked = "";
        managerLevel.currentCharacterAttacking = "";
        managerLevel.actionsEnemy = 0;
        managerLevel.actionsEvelyn = 0;
        managerLevel.isInMovingPhase = true;
        turnManager.UnitDeath(characterAttacking);
        characterAttacking = null;
        turnManager.EndOfTurn();
        manageUIscreens.FromCombatToMovingPhase();
    }

}
