﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{

    public VideoPlayer introVideo;

    private GameObject beforeIntro, afterIntro;

    private bool videoLaunched, logoInCenter, isMusicLaunched ; 

    public bool isInIntro, isInMainMenu, canPressnyButtons, isInOptions;

    private Animator animatorIntroLogo, animatorIntroText, animatorButtons, animatorMainMenu, animatorBackgroundTwoMain,
                        animatorBackgroundOptions, animatorOptionsSquare;

    private Image buttonReturnOptionsImage;

    private Text buttonReturnOptionsText;

    public Sprite buttonHover, buttonNormale;

    public Color white, black;

    private AudioSource musicsource, sfxbuttons;

    public AudioClip sfxHoverButton, sfxClickButton, sfxEnterGame;

    [HideInInspector]
    public bool isApparitionOptionsEnded, isButtonOptionsClicked;

    // Start is called before the first frame update
    void Start()
    {
        beforeIntro = GameObject.Find("Before_Intro");
        afterIntro = GameObject.Find("After_Intro");

        animatorIntroLogo = GameObject.Find("Logo_Game").GetComponent<Animator>();
        animatorIntroText = GameObject.Find("Txt_Appuyer_Touche").GetComponent<Animator>();
        animatorButtons = GameObject.Find("Buttons_Main_Menu").GetComponent<Animator>();
        animatorMainMenu = GameObject.Find("Main_Menu").GetComponent<Animator>();
        animatorBackgroundTwoMain = GameObject.Find("Background_2_Red").GetComponent<Animator>();
        animatorBackgroundOptions = GameObject.Find("Background_2_Options").GetComponent<Animator>();
        animatorOptionsSquare = GameObject.Find("Background_Options_Square").GetComponent<Animator>();

        buttonReturnOptionsImage = GameObject.Find("Button_Return_Options").GetComponent<Image>();
        buttonReturnOptionsText = GameObject.Find("Text_Button_Return_Options").GetComponent<Text>();

        isInIntro = true;
        isInMainMenu = false;
        isMusicLaunched = false;

        musicsource = GameObject.Find("Music").GetComponent<AudioSource>();
        sfxbuttons = GameObject.Find("Sfx_Buttons").GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {
        //Lancement de la vidéo au début de la scène
        if(videoLaunched == false)
        {
            introVideo.Play();
            videoLaunched = true;
        }

        //Permet de garder une image blanche derrière la vidéo le temps que celle-ci se lance
        if(introVideo.time > 0.0001)
        {
            beforeIntro.GetComponent<Image>().enabled = false;
        }

        //Si la vidéo est en pause, c'est qu'elle est donc terminée
        //Activation du fond rouge et des animations
        if (introVideo.isPaused)
        {
            //Activation du fond rouge
            afterIntro.GetComponent<Image>().enabled = true;
                        
            if(isMusicLaunched == false)
            {
                musicsource.enabled = true;
                isMusicLaunched = true;
            } 

            if(!logoInCenter)
            {
                animatorIntroLogo.SetTrigger("center");
                //animatorIntroLogo.Play("Go_To_Center_Logo");
                logoInCenter = true;
            }

            if(canPressnyButtons)
            {
                if(Input.anyKey)
                {
                    animatorIntroText.SetTrigger("click");
                    sfxbuttons.clip = sfxEnterGame;
                    sfxbuttons.Play();
                    canPressnyButtons = false;
                }
            }

            if(isInMainMenu)
            {
                //Si le joueur appuie sur une touche pour sélectionner un bouton du dessus
                if(Input.GetKeyDown(KeyCode.Z) || Input.GetKeyDown(KeyCode.UpArrow))
                {
                    if(animatorButtons.GetBool("buttonPlay") == true)
                    {
                        animatorButtons.SetBool("buttonPlay", false);
                        animatorButtons.SetBool("buttonQuit", true);

                        animatorMainMenu.SetBool("showPlay", false);
                        animatorMainMenu.SetBool("showQuit", true);
                    }
                    else if (animatorButtons.GetBool("buttonCredits") == true)
                    {
                        animatorButtons.SetBool("buttonCredits", false);
                        animatorButtons.SetBool("buttonPlay", true);

                        animatorMainMenu.SetBool("showCredits", false);
                        animatorMainMenu.SetBool("showPlay", true);
                    }
                    else if (animatorButtons.GetBool("buttonOptions") == true)
                    {
                        animatorButtons.SetBool("buttonOptions", false);
                        animatorButtons.SetBool("buttonCredits", true);

                        animatorMainMenu.SetBool("showOptions", false);
                        animatorMainMenu.SetBool("showCredits", true);
                    }
                    else if (animatorButtons.GetBool("buttonQuit") == true)
                    {
                        animatorButtons.SetBool("buttonQuit", false);
                        animatorButtons.SetBool("buttonOptions", true);

                        animatorMainMenu.SetBool("showQuit", false);
                        animatorMainMenu.SetBool("showOptions", true);
                    }
                }

                //Si le joueur appuie sur une touche pour sélectionner un bouton en-dessous
                if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
                {
                    if (animatorButtons.GetBool("buttonPlay") == true)
                    {
                        animatorButtons.SetBool("buttonPlay", false);
                        animatorButtons.SetBool("buttonCredits", true);

                        animatorMainMenu.SetBool("showPlay", false);
                        animatorMainMenu.SetBool("showCredits", true);
                    }
                    else if (animatorButtons.GetBool("buttonCredits") == true)
                    {
                        animatorButtons.SetBool("buttonCredits", false);
                        animatorButtons.SetBool("buttonOptions", true);

                        animatorMainMenu.SetBool("showCredits", false);
                        animatorMainMenu.SetBool("showOptions", true);
                    }
                    else if (animatorButtons.GetBool("buttonOptions") == true)
                    {
                        animatorButtons.SetBool("buttonOptions", false);
                        animatorButtons.SetBool("buttonQuit", true);

                        animatorMainMenu.SetBool("showOptions", false);
                        animatorMainMenu.SetBool("showQuit", true);
                    }
                    else if (animatorButtons.GetBool("buttonQuit") == true)
                    {
                        animatorButtons.SetBool("buttonQuit", false);
                        animatorButtons.SetBool("buttonPlay", true);

                        animatorMainMenu.SetBool("showQuit", false);
                        animatorMainMenu.SetBool("showPlay", true);
                    }
                }

                if (Input.GetKeyDown(KeyCode.Return))
                {
                    Debug.Log("On valide un bouton");

                    if (animatorButtons.GetBool("buttonPlay") == true)
                    {
                        //A PROGRAMMER
                    }
                    else if (animatorButtons.GetBool("buttonCredits") == true)
                    {
                        //A PROGRAMMER
                    }
                    else if (animatorButtons.GetBool("buttonOptions") == true)
                    {
                        ApparitionOptions();
                    }
                    else if (animatorButtons.GetBool("buttonQuit") == true)
                    {
                        Application.Quit();
                    }
                }
            }

            if(isInOptions && Input.GetKeyDown(KeyCode.Escape) && isApparitionOptionsEnded)
            {
                DisparitionOptions();
            }
            
        }
    }

    public void ApparitionOptions()
    {
        if(!isButtonOptionsClicked)
        {
            isButtonOptionsClicked = true;
            isInMainMenu = false;
            isInOptions = true;
            animatorButtons.SetTrigger("disparitionButtonsOptions");
            animatorBackgroundTwoMain.SetTrigger("apparitionOptions");
            animatorMainMenu.SetTrigger("disparitionEve");
            animatorBackgroundOptions.SetTrigger("apparitionBackgroundOptions");
        }

    }

    public void DisparitionOptions()
    {
        isButtonOptionsClicked = false;
        isInOptions = false;
        isApparitionOptionsEnded = false;
        isInMainMenu = true;
        animatorOptionsSquare.SetTrigger("disparitionSquare");
    }

    public void GoToCinematicScene()
    {
        SceneManager.LoadScene(1);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void GoToCreditsScene()
    {
        SceneManager.LoadScene(3);
    }

    public void OnEnterPlayButton()
    {
        animatorButtons.SetBool("buttonPlay", true);
        animatorButtons.SetBool("buttonQuit", false);
        animatorButtons.SetBool("buttonCredits", false);
        animatorButtons.SetBool("buttonOptions", false);

        animatorMainMenu.SetBool("showPlay", true);
        animatorMainMenu.SetBool("showQuit", false);
        animatorMainMenu.SetBool("showCredits", false);
        animatorMainMenu.SetBool("showOptions", false);
    }

    public void OnEnterCreditsButton()
    {
        animatorButtons.SetBool("buttonPlay", false);
        animatorButtons.SetBool("buttonQuit", false);
        animatorButtons.SetBool("buttonCredits", true);
        animatorButtons.SetBool("buttonOptions", false);

        animatorMainMenu.SetBool("showPlay", false);
        animatorMainMenu.SetBool("showQuit", false);
        animatorMainMenu.SetBool("showCredits", true);
        animatorMainMenu.SetBool("showOptions", false);
    }

    public void OnEnterOptionsButton()
    {
        animatorButtons.SetBool("buttonPlay", false);
        animatorButtons.SetBool("buttonQuit", false);
        animatorButtons.SetBool("buttonCredits", false);
        animatorButtons.SetBool("buttonOptions", true);

        animatorMainMenu.SetBool("showPlay", false);
        animatorMainMenu.SetBool("showQuit", false);
        animatorMainMenu.SetBool("showCredits", false);
        animatorMainMenu.SetBool("showOptions", true);
    }
    public void OnEnterQuitButton()
    {
        animatorButtons.SetBool("buttonPlay", false);
        animatorButtons.SetBool("buttonQuit", true);
        animatorButtons.SetBool("buttonCredits", false);
        animatorButtons.SetBool("buttonOptions", false);

        animatorMainMenu.SetBool("showPlay", false);
        animatorMainMenu.SetBool("showQuit", true);
        animatorMainMenu.SetBool("showCredits", false);
        animatorMainMenu.SetBool("showOptions", false);
    }

    public void OnEnterReturnOptions()
    {
        buttonReturnOptionsImage.sprite = buttonHover;
        buttonReturnOptionsText.color = black;
    }

    public void OnExitReturnOptions()
    {
        buttonReturnOptionsImage.sprite = buttonNormale;
        buttonReturnOptionsText.color = white;
    }

    public void HoverSoundButtons()
    {
        sfxbuttons.clip = sfxHoverButton;
        sfxbuttons.Play();  
    }

    public void ClickSoundButtons()
    {
        sfxbuttons.clip = sfxClickButton;
        sfxbuttons.Play(); 
    }
    
}
