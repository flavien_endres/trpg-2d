﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatsForCharacters : MonoBehaviour
{
    public int PV;
    public int Attaque;
    public int Defense;
    public int Adrenaline;
    public int Agilite;
    public int Vitesse;
    public int Chance;
    public int nombreActions;
}
