﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class levelManager : MonoBehaviour
{
    [Header("In what screen player is? Only for player inputs")]
    public bool isInCombatScreen;
    public bool isInComboScreen;
    public bool isInMovingPhase;
    public bool isInPauseMenu;
    public bool isInOptionsMenu;
    public bool isInPunchScreen;
    public bool isInStatsScreen;

    [Header("Is Evelyn's gift activated?")]
    public bool giftEvelynActivated;

    [Header("Who Attacks and who is attacking?")]
    public string currentCharacterAttacking;
    public string currentCharacterAttacked;

    [Header("Players win? or defeated")]
    public bool victory;
    public bool defeat;

    [Header("Number Of Actions")]
    public int actionsEvelyn;
    public int actionsEnemy;

    [Header("Stats")]
    public string unitsNameStats;

    private GameObject mainCameraGO, cameraUIGO;


    private Pathfinding.TargetMover targetForPlayers, targetForAis;

    private Pathfinding.BarreDadrenaline adrenalineBar;

    private ContainerAudioClip scriptContainer;

    private AudioSource sourceButtons;

    // Start is called before the first frame update
    void Start()
    {
        mainCameraGO = GameObject.Find("Main Camera");
        cameraUIGO = GameObject.Find("CameraUIOnly");

        targetForPlayers = GameObject.Find("Target").GetComponent<Pathfinding.TargetMover>();
        targetForAis = GameObject.Find("AITarget").GetComponent<Pathfinding.TargetMover>();

        adrenalineBar = GameObject.Find("CanvasCombat").GetComponent<Pathfinding.BarreDadrenaline>();

        scriptContainer = GameObject.Find("SoundManager").GetComponent<ContainerAudioClip>();
        sourceButtons = GameObject.Find("Source_Buttons").GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        /*
        if(isInComboScreen)
        {
            animatorCanvasCombo.enabled = false;
        }
        else
        {
            animatorCanvasCombo.enabled = true;
        }
        */
        /*
        //==========Zone de debug pour Jeffrey==========
        if(giftEvelynActivated)
        {
            GameObject.Find("Debug_Don").GetComponent<Text>().text = "OUI";
        }
        else
        {
            GameObject.Find("Debug_Don").GetComponent<Text>().text = "NON";
        }

        GameObject.Find("Debug_Points_Actions_Evelyn").GetComponent<Text>().text = GameObject.Find("Evelyn_Character").GetComponent<StatsForCharacters>().nombreActions.ToString();
        GameObject.Find("Debug_Points_Actions_Asha").GetComponent<Text>().text = GameObject.Find("Asha_Character").GetComponent<StatsForCharacters>().nombreActions.ToString();
        GameObject.Find("Debug_Degats_Entree").GetComponent<Text>().text = GameObject.Find("CanvasCombo").GetComponent<comboManager>().degatsEntreeInputs.ToString();
        GameObject.Find("Debug_Degats_Calcul").GetComponent<Text>().text = GameObject.Find("CanvasCombo").GetComponent<comboManager>().degatsDebug.ToString();
        GameObject.Find("Debug_Vie_Evelyn").GetComponent<Text>().text = GameObject.Find("Evelyn_Character").GetComponent<StatsForCharacters>().PV.ToString();
        GameObject.Find("Debug_Vie_Asha").GetComponent<Text>().text = GameObject.Find("Asha_Character").GetComponent<StatsForCharacters>().PV.ToString();
        GameObject.Find("Debug_Adrenaline_Evelyn").GetComponent<Text>().text = GameObject.Find("Evelyn_Character").GetComponent<StatsForCharacters>().Adrenaline.ToString();
        GameObject.Find("Debug_Adrenaline_Asha").GetComponent<Text>().text = GameObject.Find("Asha_Character").GetComponent<StatsForCharacters>().Adrenaline.ToString();
        //==========Zone de debug pour Jeffrey==========
        */

        if(isInCombatScreen || isInComboScreen || isInMovingPhase || isInPauseMenu || isInOptionsMenu)
        {
            mainCameraGO.GetComponent<Camera>().enabled = true;
            cameraUIGO.GetComponent<Camera>().enabled = false;
        }

        if(isInPunchScreen)
        {
            mainCameraGO.GetComponent<Camera>().enabled = false;
            cameraUIGO.GetComponent<Camera>().enabled = true;
        }

        if(isInMovingPhase)
        {
            targetForPlayers.enabled = true;
            targetForAis.enabled = true;
            mainCameraGO.GetComponent<Camera>().enabled = true;
            cameraUIGO.GetComponent<Camera>().enabled = false;
        }
        else
        {
            targetForPlayers.enabled = false;
            targetForAis.enabled = false;
        }

        if (isInCombatScreen)
            adrenalineBar.AdrenalineGaugeVisually();

    }

    public void HoverSoundButtons()
    {
        sourceButtons.clip = scriptContainer.sfxHoverButtons;
        sourceButtons.Play();
    }

    public void ClickSoundButtons()
    {
        sourceButtons.clip = scriptContainer.sfxClickButtons;
        sourceButtons.Play();
    }
}
