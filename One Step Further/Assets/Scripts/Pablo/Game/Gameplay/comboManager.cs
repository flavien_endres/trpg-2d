﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Pathfinding
{
    public class comboManager : MonoBehaviour
    {
        public levelManager levelManager;
        public TurnManagement turnManager;

        private Animator arrowUpAnimator, arrowLeftAnimator, arrowRightAnimator, arrowDownAnimator;

        public List<string> inputsDebug = new List<string>();
        public List<string> inputs = new List<string>();

        public bool comboPerfect, leftSpecial, rightSpecial, comboPerfectThreeHits, comboPerfectFourHits, comboPerfectFiveHits;

        public int degats, counterLeftRight, counterAdrenaline, currentAdrenaline;

        private CharacterStats statsEvelyn;

        private GameObject iconeGift, canvasCombat, canvasCombo, perfectUI, canvasPunch;

        private Animator animatorCanvasPunch;

        private ManagerUIScreens manageUIscreens;

        public Sprite spriteHenchman, spriteAsha, spriteEvelynAttacking, spriteEvelynAttacked;

        private Image componentImageEnemies, componentImageEvelyn, componentImageEvelynAttacked;

        private AudioSource sourceInput;

        private ContainerAudioClip scriptContainer;  

        //For debug
        public int degatsEntreeInputs, degatsDebug;
        public string result;

        // Start is called before the first frame update
        void Start()
        {
            arrowUpAnimator = GameObject.Find("Arrow_Up").GetComponent<Animator>();
            arrowLeftAnimator = GameObject.Find("Arrow_Left").GetComponent<Animator>();
            arrowRightAnimator = GameObject.Find("Arrow_Right").GetComponent<Animator>();
            arrowDownAnimator = GameObject.Find("Arrow_Down").GetComponent<Animator>();
            animatorCanvasPunch = GameObject.Find("CanvasPunch").GetComponent<Animator>();

            statsEvelyn = GameObject.Find("Evelyn").GetComponent<CharacterStats>();

            iconeGift = GameObject.Find("Icone_Don");
            canvasCombat = GameObject.Find("CanvasCombat");
            canvasCombo = GameObject.Find("CanvasCombo");
            perfectUI = GameObject.Find("Announcement_Perfect");
            canvasPunch = GameObject.Find("CanvasPunch");

            manageUIscreens = GameObject.Find("GameManager").GetComponent<ManagerUIScreens>();

            componentImageEnemies = GameObject.Find("Sprite_Enemy_Combo").GetComponent<Image>();
            componentImageEvelyn = GameObject.Find("Sprite_Evelyn_Combo").GetComponent<Image>();
            componentImageEvelynAttacked = GameObject.Find("Sprite_Enemy_Evelyn").GetComponent<Image>();

            degats = 0;
            counterLeftRight = 0;
            counterAdrenaline = 0;

            comboPerfect = false;
            comboPerfectThreeHits = false;
            comboPerfectFourHits = false;
            comboPerfectFiveHits = false;
            leftSpecial = false;
            rightSpecial = false;

            sourceInput = GameObject.Find("SFX_Combo_Inputs").GetComponent<AudioSource>();

            scriptContainer = GameObject.Find("SoundManager").GetComponent<ContainerAudioClip>();
        }

        // Update is called once per frame
        void Update()
        {
            /*
            if(canvasCombo.GetComponent<CanvasGroup>().alpha == 1)
            {
                if(levelManager.currentCharacterAttacking != null)
                {
                    if (levelManager.currentCharacterAttacking == "Asha" || levelManager.currentCharacterAttacking.Contains("Sbire"))
                    {
                        componentImageEnemies.enabled = false;
                        componentImageEvelyn.enabled = false;
                    }
                }

            }
            */

            if (levelManager.currentCharacterAttacked == "Evelyn")
            {
                componentImageEnemies.enabled = false;
                componentImageEvelynAttacked.enabled = true;

                if (levelManager.currentCharacterAttacking == "Asha")
                {
                    componentImageEvelyn.sprite = spriteAsha;
                }
                if (levelManager.currentCharacterAttacking.Contains("Sbire"))
                {
                    componentImageEvelyn.sprite = spriteHenchman;
                }
            }

            if (levelManager.isInComboScreen)
            {
                if(levelManager.currentCharacterAttacked != null)
                {
                    componentImageEvelynAttacked.enabled = false;
                    componentImageEnemies.enabled = true;
                    componentImageEvelyn.enabled = true;

                    if (levelManager.currentCharacterAttacked == "Asha" || levelManager.currentCharacterAttacked.Contains("Sbire"))
                    {

                        if (levelManager.currentCharacterAttacked == "Asha")
                        {
                            componentImageEnemies.sprite = spriteAsha;
                            componentImageEvelyn.sprite = spriteEvelynAttacking;
                        }
                        else if (levelManager.currentCharacterAttacked.Contains("Sbire"))
                        {
                            componentImageEnemies.sprite = spriteHenchman;
                            componentImageEvelyn.sprite = spriteEvelynAttacking;
                        }
                    }

                    
                }

                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    inputs.Clear();
                    statsEvelyn.Adrenaline = currentAdrenaline;
                    counterAdrenaline = 0;
                    //DEBUGS FOR JEFFREY
                    //GameObject.Find("Debug_inputs").GetComponent<Text>().text = "";
                    levelManager.isInComboScreen = false;
                    levelManager.isInCombatScreen = true;
                    result = null;
                    manageUIscreens.FromComboToCombat();
                }

                //Si le joueur appuie sur A, il peut supprimer le dernier coup préparé
                if (Input.GetKeyDown(KeyCode.A) && inputs.Count != 0)
                {
                    inputs.RemoveAt(inputs.Count - 1);
                    counterAdrenaline = inputs.Count;
                    //DebugInputsJeffrey();
                    UpdateAdrenaline();
                }

                //Si le joueur appuie sur la touche E OU entrée et qu'il a au moins un coup en préparation, on passe à la validation de combo
                if(Input.GetKeyDown(KeyCode.E) || Input.GetKeyDown(KeyCode.Return))
                {
                    if (inputs.Count != 0 && !comboPerfect)
                    {
                        sourceInput.clip = scriptContainer.sfxComboValidate;
                        sourceInput.Play();
                        ValidateNeutralCombo();
                        inputsDebug = inputs;
                    }
                    else if (inputs.Count != 0 && comboPerfect)
                    {
                        sourceInput.clip = scriptContainer.sfxComboValidate;
                        sourceInput.Play();
                        ValidatePerfectCombo();
                        inputsDebug = inputs;
                    }
                }

                if (inputs.Count <=7)
                {
                    if(Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.Z))
                    {
                        if (statsEvelyn.Adrenaline >= 3)
                        {
                            AnimationArrowUp();
                            inputs.Add("Haut");
                            //DebugInputsJeffrey();
                            sourceInput.clip = scriptContainer.sfxInputTop;
                            sourceInput.Play();

                            UpdateAdrenaline();
                        }
                    }

                    if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
                    {
                        if (statsEvelyn.Adrenaline >= 2 || leftSpecial)
                        {
                            AnimationArrowRight();
                            inputs.Add("Droite");
                            //DebugInputsJeffrey();
                            sourceInput.clip = scriptContainer.sfxInputLeftRight;
                            sourceInput.Play();
                            UpdateAdrenaline();
                        }
                    }

                    if (Input.GetKeyDown(KeyCode.Q) || Input.GetKeyDown(KeyCode.LeftArrow))
                    {
                        if (statsEvelyn.Adrenaline >= 2 || rightSpecial)
                        {
                            AnimationArrowLeft();
                            inputs.Add("Gauche");
                            //DebugInputsJeffrey();
                            sourceInput.clip = scriptContainer.sfxInputLeftRight;
                            sourceInput.Play();
                            UpdateAdrenaline();
                        }

                    }

                    if(Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
                    {
                        if (statsEvelyn.Adrenaline >= 1)
                        {
                            AnimationArrowDown();
                            inputs.Add("Bas");
                            //DebugInputsJeffrey();
                            sourceInput.clip = scriptContainer.sfxInputBottom;
                            sourceInput.Play();
                            UpdateAdrenaline();

                        }
                    }
                } 

            }
        }

        /*
        public void DebugInputsJeffrey()
        {
            //Pour le debug de Jeffrey
            result = null;
            if(inputs.Count != 0)
            {
                inputs.ForEach(delegate (string input)
                {
                    result += input.ToString() + "+ ";

                });
                GameObject.Find("Debug_inputs").GetComponent<Text>().text = result.ToString();
            }
            else
            {
                GameObject.Find("Debug_inputs").GetComponent<Text>().text = "";

            }

        }
        */

        public void AnimationArrowUp()
        {
            arrowUpAnimator.SetTrigger("animationArrowUp");
        }

        public void AnimationArrowDown()
        {
            arrowDownAnimator.SetTrigger("AnimationArrowDown");
        }

        public void AnimationArrowLeft()
        {
            arrowLeftAnimator.SetTrigger("AnimationArrowLeft");
        }

        public void AnimationArrowRight()
        {
            arrowRightAnimator.SetTrigger("AnimationArrowRight");
        }

        public void UpdateAdrenaline()
        {

            counterLeftRight = 0;
            statsEvelyn.Adrenaline = currentAdrenaline;
            rightSpecial = false;
            leftSpecial = false;


            //===========VÉRIFICATION SI LE COMBO EST PARAFAIT OU NON==========
            if (inputs.Count == 3 && inputs[0] == "Gauche" && inputs[1] == "Droite" && inputs[2] == "Gauche")
            {
                perfectUI.GetComponent<Image>().enabled = true;
                comboPerfect = true;
                comboPerfectThreeHits = true;
                counterAdrenaline = 2;
                statsEvelyn.Adrenaline -= 2;

                sourceInput.clip = scriptContainer.sfxComboPerfect;
                sourceInput.Play();

                return;
            }
            else if (inputs.Count == 4 && inputs[0] == "Droite" && inputs[1] == "Gauche" && inputs[2] == "Bas" && inputs[3] == "Droite")
            {
                perfectUI.GetComponent<Image>().enabled = true;
                comboPerfect = true;
                comboPerfectFourHits = true;
                counterAdrenaline = 3;
                statsEvelyn.Adrenaline -= 3;
                sourceInput.clip = scriptContainer.sfxComboPerfect;
                sourceInput.Play();
                return;
            }
            else if (inputs.Count == 5 && inputs[0] == "Bas" && inputs[1] == "Gauche" && inputs[2] == "Bas" && inputs[3] == "Gauche" && inputs[4] == "Droite")
            {
                perfectUI.GetComponent<Image>().enabled = true;
                comboPerfect = true;
                comboPerfectFiveHits = true;
                counterAdrenaline = 4;
                statsEvelyn.Adrenaline -= 4;
                sourceInput.clip = scriptContainer.sfxComboPerfect;
                sourceInput.Play();
                return;
            }
            else
            {
                perfectUI.GetComponent<Image>().enabled = false;
                comboPerfectThreeHits = false;
                comboPerfectFourHits = false;
                comboPerfectFiveHits = false;
                comboPerfect = false;
            }
            //===========VÉRIFICATION SI LE COMBO EST PARAFAIT OU NON==========

            //===========GESTION ADRÉNALINE ET DES COMBOS DROITE-GAUCHE==========
            inputs.ForEach(delegate (string input)
            {
                //Debug.Log(input);
                if (input == "Haut")
                {
                    statsEvelyn.Adrenaline -= 3;
                }

                if (input == "Bas")
                {
                    statsEvelyn.Adrenaline -= 1;
                }

                if (leftSpecial && input != "Droite")
                {
                    leftSpecial = false;
                }

                if (rightSpecial && input != "Gauche")
                {
                    rightSpecial = false;
                }

                if (input == "Droite")
                {
                    if (rightSpecial == false)
                    {
                        statsEvelyn.Adrenaline -= 2;
                        rightSpecial = true;
                    }
                }

                if (input == "Gauche")
                {
                    if (leftSpecial == false)
                    {
                        //Debug.Log("Boom  ");
                        statsEvelyn.Adrenaline -= 2;
                        leftSpecial = true;
                    }
                }

                if (rightSpecial && leftSpecial)
                {
                    //Debug.Log("Bingo");
                    counterLeftRight++;
                    statsEvelyn.Adrenaline += 2;
                    rightSpecial = false;
                    leftSpecial = false;
                }
            });
            //===========GESTION ADRÉNALINE ET DES COMBOS DROITE-GAUCHE==========
        }

        public void ValidatePerfectCombo()
        {
                       //Debug.Log("Perfect Combo");

            //==========AJOUT DES DÉGÂTS==========
            if (comboPerfectThreeHits)
            {
                if (!levelManager.giftEvelynActivated)
                {
                    degats += 7;
                }
                else
                {
                    degats += 8;
                }
            }
            else if (comboPerfectFourHits)
            {
                if (!levelManager.giftEvelynActivated)
                {
                    degats += 8;
                }
                else
                {
                    degats += 12;
                }
            }
            else if (comboPerfectFiveHits)
            {
                if (!levelManager.giftEvelynActivated)
                {
                    degats += 9;
                }
                else
                {
                    degats += 18;
                }
            }
            degatsEntreeInputs = degats;
            GoToPunchScreen();
            //==========AJOUT DES DÉGÂTS==========
        }


        public void ValidateNeutralCombo()
        {
             
            //==========GESTION DES DEGATS==========
            inputs.ForEach(delegate (string input)
            {

                if (input == "Haut")
                {
                    if (!levelManager.giftEvelynActivated)
                    {
                        degats += 3;
                    }
                    else
                    {
                        degats += 5;
                    }
                }
                else if (input == "Gauche")
                {
                    if (!levelManager.giftEvelynActivated)
                    {
                        degats += 2;
                    }
                    else
                    {
                        degats += 3;
                    }
                }
                else if (input == "Droite")
                {
                    if (!levelManager.giftEvelynActivated)
                    {
                        degats += 2;
                    }
                    else
                    {
                        degats += 3;
                    }
                }
                else if (input == "Bas")
                {
                    if (!levelManager.giftEvelynActivated)
                    {
                        degats += 1;
                    }
                    else
                    {
                        degats += 2;
                    }
                }

            });

            //Debug.Log("Dégâts normal: " + degats);
            degatsEntreeInputs = degats;
            //On ajoute un point de dégâts pour chaque combo droite / gauche effectué par le joueur
            degats += counterLeftRight;
            //Debug.Log("Dégâts avec le combo droite gauche: " + degats);
            GoToPunchScreen();
        }

        public void DamageCalculation()
        {
            //==========On trouve ==========
            degats += statsEvelyn.Attaque;
            GameObject targetUnit = GameObject.Find(levelManager.currentCharacterAttacked);
            degats -= targetUnit.GetComponent<CharacterStats>().Defense;
            targetUnit.GetComponent<CharacterStats>().HealthPoints -= degats;

            degatsDebug = degats;

            Debug.Log(targetUnit);
            EndingCombo();
        }

        public void EndingCombo()
        {        

            //==========NETTOYAGE DES VARIABLES POUR LE PROCHAIN COMBO==========
            //inputs.Clear();
            levelManager.giftEvelynActivated = false;
            iconeGift.GetComponent<Image>().enabled = false;
            degats = 0;
            counterLeftRight = 0;
            counterAdrenaline = 0;
            currentAdrenaline = 0;
            rightSpecial = false;
            leftSpecial = false;
            result = null;
            //GameObject.Find("Debug_inputs").GetComponent<Text>().text = "";
            //==========NETTOYAGE DES VARIABLES POUR LE PROCHAIN COMBO==========

        }

        public void GoToPunchScreen()
        {
            levelManager.isInComboScreen = false;
            manageUIscreens.FromComboToPunch();
            levelManager.isInPunchScreen = true;

            if (levelManager.currentCharacterAttacked == "Asha")
            {
                animatorCanvasPunch.SetTrigger("ashaApparition");
                animatorCanvasPunch.SetTrigger("ashaLoop");
            }
            else if (levelManager.currentCharacterAttacked == "Sbire1" || levelManager.currentCharacterAttacked == "Sbire2")
            {
                animatorCanvasPunch.SetTrigger("henchmanApparition");
                animatorCanvasPunch.SetTrigger("henchamnLoop");
            }

            perfectUI.GetComponent<Image>().enabled = false;
        }

    }
}