﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class MovingManager : MonoBehaviour, IPointerClickHandler
{
    private GameObject GoCanvasMoving, GoCanvasCombat;
    private levelManager managerLevel;
    private Animator canvasMovingAnimatior;

    public bool buttonInattack, buttonInGift, buttonInObject, isRightUIAppeared, isLeftUIAppeared, isSlideAnimationBegin = false;

    private Image underAttack, underGift, underMove, underEndTurn, portraitEvelyn, portraitAsha, portraitHenchman;
    private Text currentHP, maxHP, currentAdrenaline, currentPM;

    private Pathfinding.TurnManagement managerTurn;
    private Pathfinding.CharacterStats statsEvelyn, statsAsha, statsHenchman1, statsHenchman2;

    public Sprite evelynSprite, ashaSprite, henchmanSprite;

    private ManagerUIScreens manageUIscreens;

    private Image statsScreenPortrait;

    [Header("Portraits sprites for stats")]
    public Sprite evelynSpriteStats;
    public Sprite ashaSpriteStats;
    public Sprite henchmanSpriteStats;

    private Text txtHpStats, txtAthStats, txtDefStats, txtChaStats, txtAdStats, txtAgiStats, txtSpdStats, txtMovStats, txtName;

    private Animator animationsBackgroundStats;
    private Animator animCanvasMoving;

    private GameObject ashaUnits, evelynUnits, henchman1Units, henchman2Units;

    private Button buttonAttackMoving , buttonGift, buttonPush;

    private AudioSource sourceTransition;

    private ContainerAudioClip scriptContainer;

    private Pathfinding.Interact scriptEvelynInteract;


    // Start is called before the first frame update
    void Start()
    {
        managerLevel = GameObject.Find("GameManager").GetComponent<levelManager>();
        canvasMovingAnimatior = GameObject.Find("CanvasMoving").GetComponent<Animator>();
        GoCanvasMoving = GameObject.Find("CanvasMoving");
        GoCanvasCombat = GameObject.Find("CanvasCombat");
        managerTurn = GameObject.Find("GameManager").GetComponent<Pathfinding.TurnManagement>();

        portraitEvelyn = GameObject.Find("Portraits_Evelyn_Little_Panel").GetComponent<Image>();
        portraitAsha = GameObject.Find("Portraits_Asha_Little_Panel").GetComponent<Image>();
        portraitHenchman = GameObject.Find("Portraits_Henchman_Little_Panel").GetComponent<Image>();
        underAttack = GameObject.Find("Selection_Bouton_Attaquer").GetComponent<Image>();
        underGift = GameObject.Find("Selection_Bouton_Pousser").GetComponent<Image>();
        underMove = GameObject.Find("Selection_Bouton_Don").GetComponent<Image>();
        underEndTurn = GameObject.Find("Selection_Bouton_Fin_Tour").GetComponent<Image>();

        currentHP = GameObject.Find("currentHPText").GetComponent<Text>();
        maxHP = GameObject.Find("maxHPText").GetComponent<Text>();
        currentAdrenaline = GameObject.Find("Text_Adrenaline").GetComponent<Text>();
        currentPM = GameObject.Find("Text_PM").GetComponent<Text>();

        statsEvelyn = GameObject.Find("Evelyn").GetComponent<Pathfinding.CharacterStats>();
        statsAsha = GameObject.Find("Asha").GetComponent<Pathfinding.CharacterStats>();
        statsHenchman1 = GameObject.Find("Sbire1").GetComponent<Pathfinding.CharacterStats>();
        statsHenchman2 = GameObject.Find("Sbire2").GetComponent<Pathfinding.CharacterStats>();

        txtHpStats = GameObject.Find("Result_HP").GetComponent<Text>();
        txtAthStats = GameObject.Find("Result_ATH").GetComponent<Text>();
        txtDefStats = GameObject.Find("Result_DEF").GetComponent<Text>();
        txtChaStats = GameObject.Find("Result_CHA").GetComponent<Text>();
        txtAdStats = GameObject.Find("Result_AD").GetComponent<Text>();
        txtAgiStats = GameObject.Find("Result_AGI").GetComponent<Text>();
        txtSpdStats = GameObject.Find("Result_SPD").GetComponent<Text>();
        txtMovStats = GameObject.Find("Result_MOV").GetComponent<Text>();
        txtName = GameObject.Find("Name_Character_Text").GetComponent<Text>();
        statsScreenPortrait = GameObject.Find("Portraits_Stats").GetComponent<Image>();
        animationsBackgroundStats = GameObject.Find("Background_Stats_Screen").GetComponent<Animator>();
        animCanvasMoving = GameObject.Find("CanvasMoving").GetComponent<Animator>();

        manageUIscreens = GameObject.Find("GameManager").GetComponent<ManagerUIScreens>();

        buttonInattack = true;
        buttonInGift = false;
        buttonInObject = false;
        isLeftUIAppeared = false;
        isRightUIAppeared = false;
        isSlideAnimationBegin = false;

        ashaUnits = GameObject.Find("Asha");
        evelynUnits = GameObject.Find("Evelyn");
        henchman1Units = GameObject.Find("Sbire1");
        henchman2Units = GameObject.Find("Sbire2");

        buttonAttackMoving = GameObject.Find("Bouton_Attaquer").GetComponent<Button>();
        buttonGift = GameObject.Find("Bouton_Don").GetComponent<Button>();
        buttonPush = GameObject.Find("Bouton_Pousser").GetComponent<Button>();

        sourceTransition = GameObject.Find("SFX_Transition").GetComponent<AudioSource>();
        scriptContainer = GameObject.Find("SoundManager").GetComponent<ContainerAudioClip>();

        scriptEvelynInteract = GameObject.Find("Evelyn").GetComponent<Pathfinding.Interact>();
    }

    // Update is called once per frame
    void Update()
    {
        #region
        /*
        if (managerLevel.isInMovingPhase)
        {
            if(buttonInattack && Input.GetKeyDown(KeyCode.Z))
            {
                canvasMovingAnimatior.SetTrigger("AttackToObject");
                buttonInObject = true;
                buttonInattack = false;
            }
            else if(buttonInattack && Input.GetKeyDown(KeyCode.S))
            {
                canvasMovingAnimatior.SetTrigger("AttackToGift");
                buttonInGift = true;
                buttonInattack = false;
            }
            else if(buttonInGift && Input.GetKeyDown(KeyCode.Z))
            {
                canvasMovingAnimatior.SetTrigger("GiftToAttack");
                buttonInattack = true;
                buttonInGift = false;
            }
            else if (buttonInGift && Input.GetKeyDown(KeyCode.S))
            {
                canvasMovingAnimatior.SetTrigger("GiftToObject");
                buttonInObject = true;
                buttonInGift = false;
            }
            else if (buttonInObject && Input.GetKeyDown(KeyCode.Z))
            {
                canvasMovingAnimatior.SetTrigger("ObjectToGift");
                buttonInGift = true;
                buttonInObject = false;
            }
            else if (buttonInObject && Input.GetKeyDown(KeyCode.S))
            {
                canvasMovingAnimatior.SetTrigger("ObjectToAttack");
                buttonInattack = true;
                buttonInObject = false;
            }

            if (buttonInattack && Input.GetKeyUp(KeyCode.Return))
            {
                ClickAttackButton();
            }
            else if (buttonInGift && Input.GetKeyDown(KeyCode.Return))
            {
                ClickGiftButton();
            }
            else if (buttonInObject && Input.GetKeyDown(KeyCode.Return))
            {
                ClickObjectButton();
            }
        } 
        */
        #endregion

        if(managerLevel.isInMovingPhase && managerTurn.turnUnit == statsEvelyn.gameObject)
        {
            if (scriptEvelynInteract.anObjectHasBeenClickedOnToBePushed && !scriptEvelynInteract.hasPushedThisTurn)
                buttonPush.interactable = true;
            else
                buttonPush.interactable = false;

            if (scriptEvelynInteract.anObjectHasBeenMarkedForGift)
                buttonGift.interactable = true;
            else
                buttonGift.interactable = false;
        }
    }

    public void UIStatsAsha()
    {
        portraitEvelyn.enabled = false;
        portraitAsha.enabled = true;
        portraitHenchman.enabled = false;
        currentHP.text = statsAsha.HealthPoints.ToString();
        maxHP.text = "50";
        currentAdrenaline.text = statsAsha.Adrenaline.ToString();
        currentPM.text = statsAsha.MovementPoints.ToString();
    }

    public void UIStatsEvelyn()
    {
        portraitEvelyn.enabled = true;
        portraitAsha.enabled = false;
        portraitHenchman.enabled = false;
        currentHP.text = statsEvelyn.HealthPoints.ToString();
        maxHP.text = "50";
        currentAdrenaline.text = statsEvelyn.Adrenaline.ToString();
        currentPM.text = statsEvelyn.MovementPoints.ToString();
    }

    public void UIStatsHenchman1()
    {
        portraitEvelyn.enabled = false;
        portraitAsha.enabled = false;
        portraitHenchman.enabled = true;
        currentHP.text = statsHenchman1.HealthPoints.ToString();
        maxHP.text = "40";
        currentAdrenaline.text = statsHenchman1.Adrenaline.ToString();
        currentPM.text = statsHenchman1.MovementPoints.ToString();
    }

    public void UIStatsHenchman2()
    {
        portraitEvelyn.enabled = false;
        portraitAsha.enabled = false;
        portraitHenchman.enabled = true;
        currentHP.text = statsHenchman2.HealthPoints.ToString();
        maxHP.text = "40";
        currentAdrenaline.text = statsHenchman2.Adrenaline.ToString();
        currentPM.text = statsHenchman2.MovementPoints.ToString();
    }

    public void OnEnterButtonAttack()
    {
        if(!managerLevel.victory || managerLevel.defeat)
        {
            if (!isSlideAnimationBegin)
            {
                if (!managerLevel.isInOptionsMenu && !managerLevel.isInPauseMenu)
                {
                    underAttack.enabled = true;
                    underGift.enabled = false;
                    underMove.enabled = false;
                    underEndTurn.enabled = false;
                }
            }
        }
    }

    public void OnEnterButtonGift()
    {
        if (!managerLevel.victory || managerLevel.defeat)
        {
            if (!isSlideAnimationBegin)
            {
                if (!managerLevel.isInOptionsMenu && !managerLevel.isInPauseMenu)
                {
                    underGift.enabled = true;
                    underAttack.enabled = false;
                    underMove.enabled = false;
                    underEndTurn.enabled = false;
                }
            }
        }

    }

    public void OnEnterButtonMoving()
    {
        if (!managerLevel.victory || managerLevel.defeat)
        {
            if (!isSlideAnimationBegin)
            {
                if (!managerLevel.isInOptionsMenu && !managerLevel.isInPauseMenu)
                {
                    underMove.enabled = true;
                    underGift.enabled = false;
                    underAttack.enabled = false;
                    underEndTurn.enabled = false;
                }
            }
        }
        
    }

    public void OnEnterButtonEndTurn()
    {
        if (!managerLevel.victory || managerLevel.defeat)
        {
            if (!isSlideAnimationBegin)
            {
                if (!managerLevel.isInOptionsMenu && !managerLevel.isInPauseMenu)
                {
                    underMove.enabled = false;
                    underGift.enabled = false;
                    underAttack.enabled = false;
                    underEndTurn.enabled = true;
                }
            }
        }

    }

    public void OnExitButtonAttack()
    {
        if (!managerLevel.victory || managerLevel.defeat)
        {
            if (!isSlideAnimationBegin)
            {
                if (!managerLevel.isInOptionsMenu && !managerLevel.isInPauseMenu)
                    underAttack.enabled = false;
            }
        }
    }

    public void OnExitButtonGift()
    {
        if (!managerLevel.victory || managerLevel.defeat)
        {
            if (!isSlideAnimationBegin)
            {
                if (!managerLevel.isInOptionsMenu && !managerLevel.isInPauseMenu)
                    underGift.enabled = false;
            }
        }
    }

    public void OnExitButtonMoving()
    {
        if (!managerLevel.victory || managerLevel.defeat)
        {
            if (!isSlideAnimationBegin)
            {
                if (!managerLevel.isInOptionsMenu && !managerLevel.isInPauseMenu)
                    underMove.enabled = false;
            }
        }
    }

    public void OnExitButtonEndTurn()
    {
        if (!managerLevel.victory || managerLevel.defeat)
        {
            if (!isSlideAnimationBegin)
            {
                if (!managerLevel.isInOptionsMenu && !managerLevel.isInPauseMenu)
                    underEndTurn.enabled = false;
            }
        }

    }

    public void ClickAttackButton() 
    {
        
        if (managerLevel.currentCharacterAttacked != "" && managerLevel.currentCharacterAttacking != "" && managerLevel.isInMovingPhase)
        {
            sourceTransition.clip = scriptContainer.sfxTransiCombatIn;
            sourceTransition.Play();

            isRightUIAppeared = false;
            underAttack.enabled = false;
            animCanvasMoving.SetTrigger("DisappearUIRight");
            Debug.Log("Bisous2");
            animCanvasMoving.SetTrigger("PlayerAttacks");
            buttonAttackMoving.interactable = false;
            isSlideAnimationBegin = true;
            DisableUnitsDuringBattle();
        }
    }

    public void DisappearUILeftAndRight()
    {
        /*
        Debug.Log("Salut2");
        animCanvasMoving.SetTrigger("DisappearUILeft");
        animCanvasMoving.SetTrigger("DisappearUIRight");
        */
    }

    public void ClickGiftButton()
    {
        Debug.Log("Fais pas le fou c'est pas programmé pour le moment");
    }

    public void ClickObjectButton()
    {
        Debug.Log("Fais pas le fou c'est pas programmé pour le moment");
    }

    public void ClickOnStats()
    {
        if(managerLevel.isInMovingPhase)
        {
            managerLevel.isInStatsScreen = true;
            managerLevel.isInMovingPhase = false;
            animationsBackgroundStats.SetTrigger("appearStats");
            Debug.Log(managerLevel.unitsNameStats);
            switch (managerLevel.unitsNameStats)
            {
                case "Asha":
                    txtHpStats.text = statsAsha.HealthPoints.ToString();
                    txtAthStats.text = statsAsha.Attaque.ToString();
                    txtDefStats.text = statsAsha.Defense.ToString();
                    txtChaStats.text = statsAsha.Chance.ToString();
                    txtAdStats.text = statsAsha.Adrenaline.ToString();
                    txtAgiStats.text = statsAsha.Agilite.ToString();
                    txtSpdStats.text = statsAsha.Vitesse.ToString();
                    txtMovStats.text = statsAsha.MovementPoints.ToString();
                    statsScreenPortrait.sprite = ashaSpriteStats;
                    txtName.text = "Asha";
                    break;
                case "Sbire1":
                    txtHpStats.text = statsHenchman1.HealthPoints.ToString();
                    txtAthStats.text = statsHenchman1.Attaque.ToString();
                    txtDefStats.text = statsHenchman1.Defense.ToString();
                    txtChaStats.text = statsHenchman1.Chance.ToString();
                    txtAdStats.text = statsHenchman1.Adrenaline.ToString();
                    txtAgiStats.text = statsHenchman1.Agilite.ToString();
                    txtSpdStats.text = statsHenchman1.Vitesse.ToString();
                    txtMovStats.text = statsHenchman1.MovementPoints.ToString();
                    statsScreenPortrait.sprite = henchmanSpriteStats;
                    txtName.text = "Sbire";
                    break;
                case "Sbire2":
                    txtHpStats.text = statsHenchman2.HealthPoints.ToString();
                    txtAthStats.text = statsHenchman2.Attaque.ToString();
                    txtDefStats.text = statsHenchman2.Defense.ToString();
                    txtChaStats.text = statsHenchman2.Chance.ToString();
                    txtAdStats.text = statsHenchman2.Adrenaline.ToString();
                    txtAgiStats.text = statsHenchman2.Agilite.ToString();
                    txtSpdStats.text = statsHenchman2.Vitesse.ToString();
                    txtMovStats.text = statsHenchman2.MovementPoints.ToString();
                    statsScreenPortrait.sprite = henchmanSpriteStats;
                    txtName.text = "Sbire";
                    break;
                case "Evelyn":
                    txtHpStats.text = statsEvelyn.HealthPoints.ToString();
                    txtAthStats.text = statsEvelyn.Attaque.ToString();
                    txtDefStats.text = statsEvelyn.Defense.ToString();
                    txtChaStats.text = statsEvelyn.Chance.ToString();
                    txtAdStats.text = statsEvelyn.Adrenaline.ToString();
                    txtAgiStats.text = statsEvelyn.Agilite.ToString();
                    txtSpdStats.text = statsEvelyn.Vitesse.ToString();
                    txtMovStats.text = statsEvelyn.MovementPoints.ToString();
                    statsScreenPortrait.sprite = evelynSpriteStats;
                    txtName.text = "Evelyn";
                    break;
            }
        }
        

    }

    public void DisappearStats()
    {
        managerLevel.isInStatsScreen = false;
        managerLevel.isInMovingPhase = true;
        animationsBackgroundStats.SetTrigger("disappearStats");
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if(eventData.button == PointerEventData.InputButton.Right)
        {
            //Disable UIs buttons in animation

            /*
            if(isLeftUIAppeared)
            {
                Debug.Log("Salut3");
                animCanvasMoving.SetTrigger("DisappearUILeft");
                isLeftUIAppeared = false;
            }
            */

            if (isRightUIAppeared)
            {
                animCanvasMoving.SetTrigger("DisappearUIRight");
                managerLevel.currentCharacterAttacked = "";
                managerLevel.currentCharacterAttacking = "";
                managerLevel.unitsNameStats = "";
                isRightUIAppeared = false;
                buttonAttackMoving.interactable = false;
            }


        }
    }

    public void DisableUnitsDuringBattle()
    {
        /*
        evelynUnits.GetComponent<Pathfinding.Seeker>().enabled = false;
        evelynUnits.GetComponent<Pathfinding.AIDestinationSetter>().enabled = false;
        evelynUnits.GetComponent<Pathfinding.AILerp>().enabled = false;
        evelynUnits.GetComponent<Pathfinding.Interact>().enabled = false;

        ashaUnits.GetComponent<Pathfinding.Seeker>().enabled = false;
        ashaUnits.GetComponent<Pathfinding.AIDestinationSetter>().enabled = false;
        ashaUnits.GetComponent<Pathfinding.AILerp>().enabled = false;
        ashaUnits.GetComponent<Pathfinding.IA>().enabled = false;

        henchman1Units.GetComponent<Pathfinding.Seeker>().enabled = false;
        henchman1Units.GetComponent<Pathfinding.AIDestinationSetter>().enabled = false;
        henchman1Units.GetComponent<Pathfinding.AILerp>().enabled = false;
        henchman1Units.GetComponent<Pathfinding.IA>().enabled = false;

        henchman2Units.GetComponent<Pathfinding.Seeker>().enabled = false;
        henchman2Units.GetComponent<Pathfinding.AIDestinationSetter>().enabled = false;
        henchman2Units.GetComponent<Pathfinding.AILerp>().enabled = false;
        henchman2Units.GetComponent<Pathfinding.IA>().enabled = false;
        */
    }

    public void Retry()
    {
        SceneManager.LoadScene(2);
    }

    public void ContinueToEndCinematic()
    {
        SceneManager.LoadScene(4);
    }
}
