﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pathfinding
{
    public class AttackAi : MonoBehaviour
    {
        private levelManager managerLevel;
        public CharacterStats statsEvelyn;
        public int valueCombo;
        public bool inScreenCombat;
        public bool isComboFinished;
        private GameObject canvasCombo,canvasMoving;
        private ManagerUIScreens manageUIScreens;

        public void Start()
        {
            statsEvelyn = GameObject.Find("Evelyn").GetComponent<CharacterStats>();
            managerLevel = GameObject.Find("GameManager").GetComponent<levelManager>();
            canvasCombo = GameObject.Find("CanvasCombo");
            canvasMoving = GameObject.Find("CanvasMoving");
            manageUIScreens = GameObject.Find("GameManager").GetComponent<ManagerUIScreens>();
            inScreenCombat = false;
            isComboFinished = false;

        }

        public void Update()
        {
            /*
            if(managerLevel.currentCharacterAttacking != "")
            {
                if(!inScreenCombat && !isComboFinished)
                {
                    canvasCombo.GetComponent<CanvasGroup>().alpha = 1;
                    managerLevel.isInMovingPhase = false;

                    if(managerLevel.currentCharacterAttacking.Contains("Sbire") && !isComboFinished)
                    {
                            AttackSbire();
                    }
                    else if(managerLevel.currentCharacterAttacking == "Asha" && !isComboFinished)
                    {
                        Attackasha();
                    }

                    inScreenCombat = true;
                }
            }
            */
            /*
            if (canvasCombo.GetComponent<CanvasGroup>().alpha == 1)
            {
                if (managerLevel.currentCharacterAttacked == "Evelyn")
                {
                    animatorCanvasCombo.enabled = true;
                    
                }
                else
                {
                    animatorCanvasCombo.enabled = false;
                }
            }
            */


        }

        public void BeginAttack()
        {
            if(managerLevel.currentCharacterAttacking != "")
            {
                Debug.Log(managerLevel.currentCharacterAttacking);
                manageUIScreens.FromMovingToCombo();
                managerLevel.isInMovingPhase = false;
                Debug.Log("Salut");

                if (managerLevel.currentCharacterAttacking.Contains("Sbire") && !isComboFinished)
                {
                    AttackSbire();
                }
                else if (managerLevel.currentCharacterAttacking == "Asha" && !isComboFinished)
                {
                    Attackasha();
                }
            }
            
        }

        public void Attackasha()
        {
            valueCombo = 0;

            if (statsEvelyn.HealthPoints >= 50)
            {
                canvasCombo.GetComponent<Animator>().SetTrigger("Asha_Powerful");
                canvasCombo.GetComponent<InputGauge>().CounterGauge5();

                canvasCombo.GetComponent<comboManager>().inputs.Add("Gauche");
                canvasCombo.GetComponent<comboManager>().inputs.Add("Droite");
                canvasCombo.GetComponent<comboManager>().inputs.Add("Bas");
                canvasCombo.GetComponent<comboManager>().inputs.Add("Bas");
                canvasCombo.GetComponent<comboManager>().inputs.Add("Droite");

                //42
                statsEvelyn.HealthPoints -= 38;
            }
            else if (statsEvelyn.HealthPoints < 50)
            {
                valueCombo = Random.Range(1, 4);

                if (valueCombo == 1)
                {
                    canvasCombo.GetComponent<Animator>().SetTrigger("Asha_1");
                    canvasCombo.GetComponent<InputGauge>().CounterGauge4();

                    canvasCombo.GetComponent<comboManager>().inputs.Add("Bas");
                    canvasCombo.GetComponent<comboManager>().inputs.Add("Bas");
                    canvasCombo.GetComponent<comboManager>().inputs.Add("Haut");
                    canvasCombo.GetComponent<comboManager>().inputs.Add("Haut");

                    //30
                    statsEvelyn.HealthPoints -= 28;
                    Debug.Log("Bien le bonjour1");
                }
                else if (valueCombo == 2)
                {
                    canvasCombo.GetComponent<Animator>().SetTrigger("Asha_2");
                    canvasCombo.GetComponent<InputGauge>().CounterGauge4();

                    canvasCombo.GetComponent<comboManager>().inputs.Add("Droite");
                    canvasCombo.GetComponent<comboManager>().inputs.Add("Gauche");
                    canvasCombo.GetComponent<comboManager>().inputs.Add("Droite");
                    canvasCombo.GetComponent<comboManager>().inputs.Add("Gauche");

                    //33
                    statsEvelyn.HealthPoints -= 31;
                    Debug.Log("Bien le bonjour2");
                }
                else if (valueCombo == 3)
                {
                    canvasCombo.GetComponent<Animator>().SetTrigger("Asha_3");
                    canvasCombo.GetComponent<InputGauge>().CounterGauge6();

                    canvasCombo.GetComponent<comboManager>().inputs.Add("Haut");
                    canvasCombo.GetComponent<comboManager>().inputs.Add("Bas");
                    canvasCombo.GetComponent<comboManager>().inputs.Add("Haut");
                    canvasCombo.GetComponent<comboManager>().inputs.Add("Bas");
                    canvasCombo.GetComponent<comboManager>().inputs.Add("Droite");
                    canvasCombo.GetComponent<comboManager>().inputs.Add("Bas");

                    //38
                    statsEvelyn.HealthPoints -= 36;
                    Debug.Log("Bien le bonjour3");
                }
                else if (valueCombo == 4)
                {
                    canvasCombo.GetComponent<Animator>().SetTrigger("Asha_4");
                    canvasCombo.GetComponent<InputGauge>().CounterGauge4();

                    canvasCombo.GetComponent<comboManager>().inputs.Add("Gauche");
                    canvasCombo.GetComponent<comboManager>().inputs.Add("Droite");
                    canvasCombo.GetComponent<comboManager>().inputs.Add("Haut");
                    canvasCombo.GetComponent<comboManager>().inputs.Add("Bas");
                    canvasCombo.GetComponent<comboManager>().inputs.Add("Bas");
                    canvasCombo.GetComponent<comboManager>().inputs.Add("Droite");

                    //42
                    statsEvelyn.HealthPoints -= 40;
                    Debug.Log("Bien le bonjour4");
                }

            }
        }

        public void AttackSbire()
        {
            valueCombo = 0;

            if (statsEvelyn.HealthPoints >= 50)
            {
                //Debug.Log("C'est le moment où le sbire fait des miracles");
                canvasCombo.GetComponent<Animator>().SetTrigger("Sbire_Powerful");


                canvasCombo.GetComponent<comboManager>().inputs.Add("Haut");
                canvasCombo.GetComponent<comboManager>().inputs.Add("Bas");
                canvasCombo.GetComponent<comboManager>().inputs.Add("Bas");
                canvasCombo.GetComponent<comboManager>().inputs.Add("Gauche");

                canvasCombo.GetComponent<InputGauge>().CounterGauge4();

                //18
                statsEvelyn.HealthPoints -= 18;
            }
            else if (statsEvelyn.HealthPoints < 50)
            {
                valueCombo = Random.Range(1, 3);

                if (valueCombo == 1)
                {
                    canvasCombo.GetComponent<Animator>().SetTrigger("Sbire_1");

                    canvasCombo.GetComponent<comboManager>().inputs.Add("Gauche");
                    canvasCombo.GetComponent<comboManager>().inputs.Add("Haut");

                    canvasCombo.GetComponent<InputGauge>().CounterGauge2();

                    //17
                    statsEvelyn.HealthPoints -= 17;
                }
                else if (valueCombo == 2)
                {
                    canvasCombo.GetComponent<Animator>().SetTrigger("Sbire_2");

                    canvasCombo.GetComponent<comboManager>().inputs.Add("Bas");
                    canvasCombo.GetComponent<comboManager>().inputs.Add("Bas");
                    canvasCombo.GetComponent<comboManager>().inputs.Add("Bas");
                    canvasCombo.GetComponent<comboManager>().inputs.Add("Bas");
                    canvasCombo.GetComponent<comboManager>().inputs.Add("Bas");

                    canvasCombo.GetComponent<InputGauge>().CounterGauge5();

                    //10
                    statsEvelyn.HealthPoints -= 10;
                }
                else if (valueCombo == 3)
                {
                    canvasCombo.GetComponent<Animator>().SetTrigger("Sbire_3");
                    canvasCombo.GetComponent<InputGauge>().CounterGauge3();

                    canvasCombo.GetComponent<comboManager>().inputs.Add("Haut");
                    canvasCombo.GetComponent<comboManager>().inputs.Add("Bas");
                    canvasCombo.GetComponent<comboManager>().inputs.Add("Bas");

                    //18
                    statsEvelyn.HealthPoints -= 10;
                }

            }
        }
    }
}