﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class pauseMenu : MonoBehaviour
{

    private levelManager managerLevel;
    private Animator pauseAnimator, movingAnimator;
    private GameObject buttonResume, buttonOptions, buttonMenu, txtResume, txtOptions, txtMenu, buttonOptionsResume;

    public Sprite buttonSelected, buttonNOTSelected;
    public Color colorBlack, colorWhite;

    private BoxCollider colliderAsha, colliderEvelyn, colliderHenchman1, colliderHenchman2;
    private Pathfinding.AILerp lerpEvelyn, lerpAsha, lerpHenchman1, lerpHenchman2;

    private MovingManager managerMoving;

    private Button buttonAttackMoving, buttonPushMoving, buttonGiftMoving, buttonEndTurnMoving;

    private AudioSource sfxPause;

    // Start is called before the first frame update
    void Start()
    {
        managerLevel = gameObject.GetComponent<levelManager>();
        pauseAnimator = GameObject.Find("CanvasPause").GetComponent<Animator>();
        movingAnimator = GameObject.Find("CanvasMoving").GetComponent<Animator>();

        buttonResume = GameObject.Find("Button_Resume");
        buttonOptions = GameObject.Find("Button_Options");
        buttonMenu = GameObject.Find("Button_Menu");
        buttonOptionsResume = GameObject.Find("Button_Return");

        txtResume = GameObject.Find("Text_Resume");
        txtOptions = GameObject.Find("Text_Options");
        txtMenu = GameObject.Find("Text_Menu");

        colliderAsha = GameObject.Find("Asha").GetComponent<BoxCollider>();
        //colliderEvelyn = GameObject.Find("Evelyn").GetComponent<BoxCollider>();
        colliderHenchman1 = GameObject.Find("Sbire1").GetComponent<BoxCollider>();
        colliderHenchman2 = GameObject.Find("Sbire2").GetComponent<BoxCollider>();

        lerpEvelyn = GameObject.Find("Evelyn").GetComponent<Pathfinding.AILerp>();
        lerpAsha = GameObject.Find("Asha").GetComponent<Pathfinding.AILerp>();
        lerpHenchman1 = GameObject.Find("Sbire1").GetComponent<Pathfinding.AILerp>();
        lerpHenchman2 = GameObject.Find("Sbire2").GetComponent<Pathfinding.AILerp>();

        managerMoving = GameObject.Find("CanvasMoving").GetComponent<MovingManager>();

        buttonAttackMoving = GameObject.Find("Bouton_Attaquer").GetComponent<Button>();
        buttonPushMoving = GameObject.Find("Bouton_Pousser").GetComponent<Button>();
        buttonGiftMoving = GameObject.Find("Bouton_Don").GetComponent<Button>();
        buttonEndTurnMoving = GameObject.Find("Bouton_Fin_Tour").GetComponent<Button>();

        sfxPause = GameObject.Find("SFX_Pause").GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        //Apparition et disparition du menu pause
        if(managerLevel.isInMovingPhase == true)
        {
            if(Input.GetKeyDown(KeyCode.Escape))
            {
                if(!lerpEvelyn.canMove && !lerpAsha.canMove && !lerpHenchman1.canMove && !lerpHenchman2.canMove)
                {
                    colliderAsha.enabled = false;
                    colliderHenchman1.enabled = false;
                    colliderHenchman2.enabled = false;

                    buttonAttackMoving.interactable = false;
                    buttonPushMoving.interactable = false;
                    buttonGiftMoving.interactable = false;
                    buttonEndTurnMoving.interactable = false;

                    sfxPause.Play();
                    /*
                    if(managerMoving.isLeftUIAppeared)
                    {
                        Debug.Log("SalutAgain");
                        movingAnimator.SetTrigger("DisappearUILeft");
                        managerMoving.isLeftUIAppeared = false;
                    }
                    */

                    if (managerMoving.isRightUIAppeared)
                    {
                        movingAnimator.SetTrigger("DisappearUIRight");
                        managerMoving.isRightUIAppeared = false;
                    }

                    pauseAnimator.SetTrigger("apparitionPauseMenu");
                    managerLevel.isInPauseMenu = true;
                    managerLevel.isInMovingPhase = false;
                    lerpEvelyn.enabled = false;
                }
            }
        }
        else if(managerLevel.isInPauseMenu)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                buttonAttackMoving.interactable = false;
                buttonPushMoving.interactable = true;
                buttonGiftMoving.interactable = true;
                buttonEndTurnMoving.interactable = true;
                DisappearPauseMenu();
            }

        }
        //Gestion UI pauseBoutons
        if(managerLevel.isInPauseMenu)
        {
            if(pauseAnimator.GetBool("resumeTrue") == true && Input.GetKeyDown(KeyCode.UpArrow))
            {
                pauseAnimator.SetBool("resumeTrue", false);
                pauseAnimator.SetBool("menuTrue", true);
            }
            else if(pauseAnimator.GetBool("resumeTrue") == true && Input.GetKeyDown(KeyCode.DownArrow))
            {
                pauseAnimator.SetBool("resumeTrue", false);
                pauseAnimator.SetBool("optionsTrue", true);
            }
            else if (pauseAnimator.GetBool("optionsTrue") == true && Input.GetKeyDown(KeyCode.UpArrow))
            {
                pauseAnimator.SetBool("optionsTrue", false);
                pauseAnimator.SetBool("resumeTrue", true);
            }
            else if (pauseAnimator.GetBool("optionsTrue") == true && Input.GetKeyDown(KeyCode.DownArrow))
            {
                pauseAnimator.SetBool("optionsTrue", false);
                pauseAnimator.SetBool("menuTrue", true);
            }
            else if (pauseAnimator.GetBool("menuTrue") == true && Input.GetKeyDown(KeyCode.UpArrow))
            {
                pauseAnimator.SetBool("menuTrue", false);
                pauseAnimator.SetBool("optionsTrue", true);
            }
            else if (pauseAnimator.GetBool("menuTrue") == true && Input.GetKeyDown(KeyCode.DownArrow))
            {
                pauseAnimator.SetBool("menuTrue", false);
                pauseAnimator.SetBool("resumeTrue", true);

            }

            
            if(pauseAnimator.GetBool("resumeTrue") == true && Input.GetKeyDown(KeyCode.Return))
            {
                DisappearPauseMenu();
            }
            else if (pauseAnimator.GetBool("optionsTrue") == true && Input.GetKeyDown(KeyCode.Return))
            {
                OpenOptionsMenu();
            }
            else if (pauseAnimator.GetBool("menuTrue") == true && Input.GetKeyDown(KeyCode.Return))
            {
                LoadMainMenu();
            }
            
        }

        if(managerLevel.isInOptionsMenu)
        {
            if(Input.GetKeyDown(KeyCode.Escape))
            {
                ClickReturnOptions();
            }
        }
    }

    public void DisappearPauseMenu()
    {
        buttonAttackMoving.interactable = false;
        buttonPushMoving.interactable = true;
        buttonGiftMoving.interactable = true;
        buttonEndTurnMoving.interactable = true;
        colliderAsha.enabled = true;
        lerpEvelyn.enabled = true;
        //colliderEvelyn.enabled = true;
        colliderHenchman1.enabled = true;
        colliderHenchman2.enabled = true;
        pauseAnimator.SetTrigger("disparitionPauseMenu");
        managerLevel.isInPauseMenu = false;
        managerLevel.isInMovingPhase = true;
    }

    public void LoadMainMenu()
    {
        if(managerLevel.isInPauseMenu)
            SceneManager.LoadScene("MainMenu");
    }

    public void OpenOptionsMenu()
    {
        managerLevel.isInPauseMenu = false;
        managerLevel.isInOptionsMenu = true;
        pauseAnimator.SetTrigger("disparitionPauseMenu");
        pauseAnimator.SetTrigger("apparitionOptions");
    }

    public void OnPointerEnterResume()
    {
        buttonOptions.GetComponent<Image>().sprite = buttonNOTSelected;
        buttonResume.GetComponent<Image>().sprite = buttonSelected;
        buttonMenu.GetComponent<Image>().sprite = buttonNOTSelected;

        txtOptions.GetComponent<Text>().color = colorWhite;
        txtResume.GetComponent<Text>().color = colorBlack;
        txtMenu.GetComponent<Text>().color = colorWhite;

        pauseAnimator.SetBool("optionsTrue", false);
        pauseAnimator.SetBool("resumeTrue", true);
        pauseAnimator.SetBool("menuTrue", false);
    }

    public void OnPointerEnterOptions()
    {
        buttonOptions.GetComponent<Image>().sprite = buttonSelected;
        buttonResume.GetComponent<Image>().sprite = buttonNOTSelected;
        buttonMenu.GetComponent<Image>().sprite = buttonNOTSelected;

        txtOptions.GetComponent<Text>().color = colorBlack;
        txtResume.GetComponent<Text>().color = colorWhite;
        txtMenu.GetComponent<Text>().color = colorWhite;

        pauseAnimator.SetBool("optionsTrue", true);
        pauseAnimator.SetBool("resumeTrue", false);
        pauseAnimator.SetBool("menuTrue", false);
    }

    public void OnPointerEnterMainMenu()
    {
        buttonOptions.GetComponent<Image>().sprite = buttonNOTSelected;
        buttonResume.GetComponent<Image>().sprite = buttonNOTSelected;
        buttonMenu.GetComponent<Image>().sprite = buttonSelected;

        txtOptions.GetComponent<Text>().color = colorWhite;
        txtResume.GetComponent<Text>().color = colorWhite;
        txtMenu.GetComponent<Text>().color = colorBlack;

        pauseAnimator.SetBool("optionsTrue", false);
        pauseAnimator.SetBool("resumeTrue", false);
        pauseAnimator.SetBool("menuTrue", true);
    }

    public void ClickReturnOptions()
    {
        if(managerLevel.isInOptionsMenu)
        {
            managerLevel.isInOptionsMenu = false;
            managerLevel.isInPauseMenu = true;
            pauseAnimator.SetTrigger("disparitionOptions");
        }
    }

    public void OnPointerEnterReturnOptions()
    {
        pauseAnimator.Play("Button_Options_Return_Selected");
    }

    public void OnPointerExitReturnOptions()
    {
        pauseAnimator.Play("Button_Options_Return_NotSelected");
    }
}
