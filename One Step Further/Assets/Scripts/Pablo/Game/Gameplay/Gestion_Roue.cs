﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Pathfinding
{
    public class Gestion_Roue : MonoBehaviour
    {
        private Animator animatorActions;
        private Image imageDescription, imageIconeCounter;

        public Sprite comboSprite, riposteSprite, donSprite;
        public Sprite riposteGiftENG, donSpriteENG;

        public bool comboBouton, riposteBouton, donBouton;

        private GameObject canvasCombat, canvasCombo, canvasPunch;

        private levelManager managerLevel;

        private CharacterStats statsAsha, statsEvelyn, statsSbire1, statsSbire2;

        private Camera mainCamera, cameraUI;

        private ManagerCounterAttack scriptCounterAttack;

        private ManagerUIScreens manageUIscreens;

        private AudioSource qteStart;

        private ContainerAudioClip scriptContainer;

        // Start is called before the first frame update
        void Start()
        {
            animatorActions = gameObject.GetComponent<Animator>();
            imageDescription = GameObject.Find("Description").GetComponent<Image>();

            canvasCombat = GameObject.Find("CanvasCombat");
            canvasCombo = GameObject.Find("CanvasCombo");
            canvasPunch = GameObject.Find("CanvasPunch");

            statsAsha = GameObject.Find("Asha").GetComponent<Pathfinding.CharacterStats>();
            statsEvelyn = GameObject.Find("Evelyn").GetComponent<Pathfinding.CharacterStats>();
            statsSbire1 = GameObject.Find("Sbire1").GetComponent<Pathfinding.CharacterStats>();
            statsSbire2 = GameObject.Find("Sbire2").GetComponent<Pathfinding.CharacterStats>();

            imageIconeCounter = GameObject.Find("Icone_Riposte").GetComponent<Image>();

            mainCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
            cameraUI = GameObject.Find("CameraUIOnly").GetComponent<Camera>();

            scriptCounterAttack = GameObject.Find("CanvasPunch").GetComponent<ManagerCounterAttack>();

            managerLevel = GameObject.Find("GameManager").GetComponent<levelManager>();

            manageUIscreens = GameObject.Find("GameManager").GetComponent<ManagerUIScreens>();

            comboBouton = true;
            riposteBouton = false;
            donBouton = false;

            qteStart = GameObject.Find("SFX_Riposte").GetComponent<AudioSource>() ;
            scriptContainer = GameObject.Find("SoundManager").GetComponent<ContainerAudioClip>();
        }

        // Update is called once per frame
        void Update()
        {
            //Debug.Log(statsEvelyn.nombreActions);

            //---------Gestion de l'animation de la roue----------


            if (managerLevel.isInCombatScreen && canvasCombat.GetComponent<CanvasGroup>().alpha == 1)
            {
                if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
                {
                    if (animatorActions.GetBool("fightCenter") == true)
                    {
                        animatorActions.SetTrigger("animationGiftCenter");
                        animatorActions.SetBool("giftCenter", true);
                        animatorActions.SetBool("fightCenter", false);

                        if (PlayerPrefs.GetInt("Language") == 0)
                        {
                            imageDescription.sprite = donSpriteENG;
                        }
                        else
                        {
                            imageDescription.sprite = donSprite;
                        }

                        comboBouton = false;
                        riposteBouton = false;
                        donBouton = true;
                    }
                    else if(animatorActions.GetBool("giftCenter") == true)
                    {
                        animatorActions.SetTrigger("animationShieldCenter");
                        animatorActions.SetBool("shieldCenter", true);
                        animatorActions.SetBool("giftCenter", false);

                        if (PlayerPrefs.GetInt("Language") == 0)
                        {
                            imageDescription.sprite = riposteGiftENG;
                        }
                        else
                        {
                            imageDescription.sprite = riposteSprite;
                        }

                        comboBouton = false;
                        riposteBouton = true;
                        donBouton = false;
                    }
                    else if(animatorActions.GetBool("shieldCenter") == true)
                    {
                        animatorActions.SetTrigger("animationfightCenter");
                        animatorActions.SetBool("fightCenter", true);
                        animatorActions.SetBool("shieldCenter", false);
                        imageDescription.sprite = comboSprite;

                        comboBouton = true;
                        riposteBouton = false;
                        donBouton = false;
                    }
                }

                if (Input.GetKeyDown(KeyCode.Q) || Input.GetKeyDown(KeyCode.LeftArrow))
                {
                    if (animatorActions.GetBool("fightCenter") == true)
                    {
                        animatorActions.SetTrigger("animationShieldCenter");
                        animatorActions.SetBool("shieldCenter", true);
                        animatorActions.SetBool("fightCenter", false);

                        if (PlayerPrefs.GetInt("Language") == 0)
                        {
                            imageDescription.sprite = riposteGiftENG;
                        }
                        else
                        {
                            imageDescription.sprite = riposteSprite;
                        }

                        comboBouton = false;
                        riposteBouton = true;
                        donBouton = false;
                    }
                    else if (animatorActions.GetBool("giftCenter") == true)
                    {
                        animatorActions.SetTrigger("animationfightCenter");
                        animatorActions.SetBool("fightCenter", true);
                        animatorActions.SetBool("giftCenter", false);
                        imageDescription.sprite = comboSprite;

                        comboBouton = true;
                        riposteBouton = false;
                        donBouton = false;
                    }
                    else if (animatorActions.GetBool("shieldCenter") == true)
                    {
                        animatorActions.SetTrigger("animationGiftCenter");
                        animatorActions.SetBool("giftCenter", true);
                        animatorActions.SetBool("shieldCenter", false);

                        if (PlayerPrefs.GetInt("Language") == 0)
                        {
                            imageDescription.sprite = donSpriteENG;
                        }
                        else
                        {
                            imageDescription.sprite = donSprite;
                        }

                        comboBouton = false;
                        riposteBouton = false;
                        donBouton = true;
                    }
                }                   
                //---------Gestion de l'animation de la roue----------


                if (Input.GetKeyDown(KeyCode.Return))
                {
                    ClickActionButton();
                }
            }
        }

        public void ClickActionButton()
        {
            //Si le joueur appuie sur le bouton de combo, l'écran choisi s'active
            if (comboBouton)
            {
                if (statsEvelyn.nombreActions > 0 && statsEvelyn.Adrenaline > 0)
                {
                    GameObject.Find("CanvasCombo").GetComponent<comboManager>().currentAdrenaline = GameObject.Find("Evelyn").GetComponent<CharacterStats>().Adrenaline;
                    manageUIscreens.FromCombatToCombo();
                }
            }
            else if (riposteBouton)
            {
                Pathfinding.CharacterStats characterAttacked;
                characterAttacked = GameObject.Find(managerLevel.currentCharacterAttacked).GetComponent<Pathfinding.CharacterStats>();

                if (statsEvelyn.nombreActions == 0 || characterAttacked.nombreActions <= 0)
                {
                    Debug.Log("coucou, tu ne peux pas faire de riposte");
                    return;
                }

                qteStart.clip = scriptContainer.sfxQTEStart;
                qteStart.Play();

                if (managerLevel.currentCharacterAttacked == "Asha" && statsAsha.nombreActions > 0)
                {
                    managerLevel.currentCharacterAttacking = "Asha";
                }
                else if (managerLevel.currentCharacterAttacked == "Sbire1" && statsSbire1.nombreActions > 0)
                {
                    managerLevel.currentCharacterAttacking = "Sbire1";
                }
                else if (managerLevel.currentCharacterAttacked == "Sbire2" && statsSbire2.nombreActions > 0)
                {
                    managerLevel.currentCharacterAttacking = "Sbire2";
                }
                else
                {
                    //Debug.Log("coucou2");
                    return;
                }

                managerLevel.currentCharacterAttacked = "Evelyn";
                managerLevel.actionsEvelyn -= 1;
                statsEvelyn.nombreActions -= 1;
                statsEvelyn.HealthPoints += 8;
                manageUIscreens.FromCombatToPunch();
                managerLevel.isInCombatScreen = false;
                managerLevel.isInPunchScreen = true;
                scriptCounterAttack.ChooseAttack();
            }
            else if (donBouton && !managerLevel.giftEvelynActivated && statsEvelyn.nombreActions > 0)
            {
                qteStart.clip = scriptContainer.sfxDonActivate;
                qteStart.Play();
                Debug.Log("Le don d'Evelyn est activé");
                managerLevel.giftEvelynActivated = true;
            }
        }
    }
}