﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour
{
    public List<AudioSource> componentAudioSourceMusics = new List<AudioSource>();
    public List<AudioSource> componentAudioSourceSFX = new List<AudioSource>();

    public float debugPrefsMusic, debugPrefsSFX;

    public Slider componentSliderSFX, componentSliderMusic;

    // Start is called before the first frame update
    void Start()
    {
        if (!PlayerPrefs.HasKey("Music"))
        {
            PlayerPrefs.SetFloat("Music", 1f);
        }

        if (!PlayerPrefs.HasKey("SFX"))
        {
            PlayerPrefs.SetFloat("SFX", 1f);
        }

        if(componentSliderSFX != null)
        {
            componentSliderSFX.value = PlayerPrefs.GetFloat("SFX");
        }

        if(componentSliderMusic != null)
        {
            componentSliderMusic.value = PlayerPrefs.GetFloat("Music");
        }

        //VolumeMusic();
        //VolumeSFX();
    }

    // Update is called once per frame
    void Update()
    {
        if (componentSliderMusic != null)
            PlayerPrefs.SetFloat("Music", componentSliderMusic.value);

        if (componentSliderSFX != null)
            PlayerPrefs.SetFloat("SFX", componentSliderSFX.value);

        if(componentAudioSourceMusics.Count > 0)
        {
            for (int i = 0; i < componentAudioSourceMusics.Count; i++)
            {
                componentAudioSourceMusics[i].volume = PlayerPrefs.GetFloat("Music");
            }
        }
        
        if(componentAudioSourceSFX.Count > 0)
        {
            for (int i = 0; i < componentAudioSourceSFX.Count; i++)
            {
                componentAudioSourceSFX[i].volume = PlayerPrefs.GetFloat("SFX");
            }
        }


        debugPrefsMusic = PlayerPrefs.GetFloat("Music");
        debugPrefsSFX = PlayerPrefs.GetFloat("SFX");
        //Debug.Log("Valeurs PlayerPrefs Music : " + PlayerPrefs.GetFloat("Music"));
        //Debug.Log("Valeurs PlayerPrefs SFX : " + PlayerPrefs.GetFloat("SFX"));
    }

    /*
    public void VolumeMusic()
    {
        for (int i = 0; i < componentAudioSourceMusics.Count; i++)
        {
            componentAudioSourceMusics[i].volume = PlayerPrefs.GetFloat("Music");
        }
    }

    public void VolumeSFX()
    {
        for (int i = 0; i < componentAudioSourceSFX.Count; i++)
        {
            componentAudioSourceSFX[i].volume = PlayerPrefs.GetFloat("SFX");
        }
    }

    
    public void SliderSFX(float valueSFX)
    {
        Debug.Log("Valeur slider valueSFX" + valueSFX);
        PlayerPrefs.SetFloat("SFX", valueSFX);
    }

    public void SliderMusic(float valueMusic)
    {
        Debug.Log("Valeur slider valueMusic" + valueMusic);
        PlayerPrefs.SetFloat("Music", valueMusic);
    }
    */
    
}
