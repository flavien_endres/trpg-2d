﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LanguageManager : MonoBehaviour
{
    public List<string> englishTextList = new List<string>();
    public List<string> frenchTextList = new List<string>();
    public List<Text> componentTextList = new List<Text>();

    // Start is called before the first frame update
    void Start()
    {
        if (!PlayerPrefs.HasKey("Language"))
        {
            PlayerPrefs.SetInt("Language", 0);
        }

        if (PlayerPrefs.GetInt("Language") == 0)
        {
            EnglishText();
        }
        else
        {
            FrenchText();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void EnglishText()
    {
        PlayerPrefs.SetInt("Language", 0);
        for (int i = 0; i < Mathf.Min(englishTextList.Count, componentTextList.Count); i++)
        {
            componentTextList[i].text = englishTextList[i];
        }
    }

    public void FrenchText()
    {
        PlayerPrefs.SetInt("Language", 1);
        for (int i = 0; i < Mathf.Min(frenchTextList.Count, componentTextList.Count); i++)
        {
            componentTextList[i].text = frenchTextList[i];
        }
    }
}
