﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Pathfinding
{
    public class Background_PV_Animations : MonoBehaviour
    {
        public Sprite HalfPvAllyImageDark, HalfPvEnemyImageDark,
                     FullPvAllyImageDark, FullPvEnemyImageDark,
                     LowPvAllyImageDark, LowPvEnemyImageDark;

        private GameObject backgroundPVAllyDark, backgroundPVEnemyDark;

        private CharacterStats statsEvelyn;

        private Animator animatorAllyBackground, animatorEnemyBackground, animatorEnemyCombat;

        private GameObject pvAlly, pvEnemy;

        private GameObject iconeGift;

        private levelManager manager;

        private levelManager managerLevel;

        private Image imageAtMiddle;

        [Header("Evelyn's sprites")]
        public Sprite evelynFull;
        public Sprite evelynMiddle;
        public Sprite evelynBad;

        [Header("Asha's sprites")]
        public Sprite ashaFull;
        public Sprite ashaMiddle;
        public Sprite ashaBad;

        [Header("Henchman's sprites")]
        public Sprite henchmanFull;
        public Sprite henchmanMiddle;
        public Sprite henchmanBad;

        private Image portraitEvelyn, portraitsEnemies;

        [Header("Enemies's stats")]
        public CharacterStats statsAsha;
        public CharacterStats statsHenchman1;
        public CharacterStats statsHenchman2;

        // Start is called before the first frame update
        void Start()
        {
            statsEvelyn = GameObject.Find("Evelyn").GetComponent<CharacterStats>();
            statsAsha = GameObject.Find("Asha").GetComponent<CharacterStats>();
            statsHenchman1 = GameObject.Find("Sbire1").GetComponent<CharacterStats>();
            statsHenchman2 = GameObject.Find("Sbire2").GetComponent<CharacterStats>();

            backgroundPVAllyDark = GameObject.Find("Background_PV_Ally_dark");
            backgroundPVEnemyDark = GameObject.Find("Background_PV_Enemy_dark");
            animatorAllyBackground = GameObject.Find("Background_PV_Ally_light").GetComponent<Animator>();
            animatorEnemyBackground = GameObject.Find("Background_PV_Enemy_light").GetComponent<Animator>();
            portraitEvelyn = GameObject.Find("Portraits_Evelyn").GetComponent<Image>();
            portraitsEnemies = GameObject.Find("Portraits_Enemy").GetComponent<Image>();

            iconeGift = GameObject.Find("Icone_Don");
            manager = GameObject.Find("GameManager").GetComponent<levelManager>();

            pvAlly = GameObject.Find("PV_Ally");
            pvEnemy = GameObject.Find("PV_Enemy");

            managerLevel = GameObject.Find("GameManager").GetComponent<levelManager>();

            animatorEnemyCombat = GameObject.Find("Enemy_Sprite_Middle").GetComponent<Animator>();
        }

        // Update is called once per frame
        void Update()
        {
            //Manage portraits and texts
            if(manager.currentCharacterAttacked == "Asha")
            {
                animatorEnemyCombat.Play("AshaMiddle");

                if(statsAsha.HealthPoints <= 50 && statsAsha.HealthPoints > 25)
                {
                    portraitsEnemies.sprite = ashaFull;
                    backgroundPVEnemyDark.GetComponent<Image>().sprite = FullPvEnemyImageDark;
                    animatorEnemyBackground.Play("Animations_PV_Enemy");
                }
                else if(statsAsha.HealthPoints <= 25 && statsAsha.HealthPoints > 7)
                {
                    portraitsEnemies.sprite = ashaMiddle;
                    backgroundPVEnemyDark.GetComponent<Image>().sprite = HalfPvEnemyImageDark;
                    animatorEnemyBackground.Play("Animations_PV_Enemy_Half");
                }
                else if(statsAsha.HealthPoints <= 7)
                {
                    portraitsEnemies.sprite = ashaBad;
                    backgroundPVEnemyDark.GetComponent<Image>().sprite = LowPvEnemyImageDark;
                    animatorEnemyBackground.Play("Animations_PV_Enemy_Low");
                }

                if (statsAsha.HealthPoints > 0)
                    pvEnemy.GetComponent<Text>().text = statsAsha.HealthPoints.ToString();
                else
                    pvEnemy.GetComponent<Text>().text = 0.ToString();
            }
            else if(manager.currentCharacterAttacked == "Sbire1")
            {
                animatorEnemyCombat.Play("henchmanMiddle");

                if (statsHenchman1.HealthPoints <= 40 && statsHenchman1.HealthPoints > 20)
                {
                    portraitsEnemies.sprite = henchmanFull;
                    backgroundPVEnemyDark.GetComponent<Image>().sprite = FullPvEnemyImageDark;
                    animatorEnemyBackground.Play("Animations_PV_Enemy");
                }
                else if (statsHenchman1.HealthPoints <= 20 && statsHenchman1.HealthPoints > 05)
                {
                    portraitsEnemies.sprite = henchmanMiddle;
                    backgroundPVEnemyDark.GetComponent<Image>().sprite = HalfPvEnemyImageDark;
                    animatorEnemyBackground.Play("Animations_PV_Enemy_Half");
                }
                else if (statsHenchman1.HealthPoints <= 05)
                {
                    portraitsEnemies.sprite = henchmanBad;
                    backgroundPVEnemyDark.GetComponent<Image>().sprite = LowPvEnemyImageDark;
                    animatorEnemyBackground.Play("Animations_PV_Enemy_Low");
                }

                if (statsHenchman1.HealthPoints > 0)
                    pvEnemy.GetComponent<Text>().text = statsHenchman1.HealthPoints.ToString();
                else
                    pvEnemy.GetComponent<Text>().text = 0.ToString();
            }
            else if(manager.currentCharacterAttacked == "Sbire2")
            {
                animatorEnemyCombat.Play("henchmanMiddle");

                if (statsHenchman2.HealthPoints <= 40 && statsHenchman2.HealthPoints > 20)
                {
                    portraitsEnemies.sprite = henchmanFull;
                    backgroundPVEnemyDark.GetComponent<Image>().sprite = FullPvEnemyImageDark;
                    animatorEnemyBackground.Play("Animations_PV_Enemy");
                }
                else if (statsHenchman2.HealthPoints <= 20 && statsHenchman2.HealthPoints > 05)
                {
                    portraitsEnemies.sprite = henchmanMiddle;
                    backgroundPVEnemyDark.GetComponent<Image>().sprite = HalfPvEnemyImageDark;
                    animatorEnemyBackground.Play("Animations_PV_Enemy_Half");
                }
                else if (statsHenchman2.HealthPoints <= 05)
                {
                    portraitsEnemies.sprite = henchmanBad;
                    backgroundPVEnemyDark.GetComponent<Image>().sprite = LowPvEnemyImageDark;
                    animatorEnemyBackground.Play("Animations_PV_Enemy_Low");
                }

                if (statsHenchman2.HealthPoints > 0)
                    pvEnemy.GetComponent<Text>().text = statsHenchman2.HealthPoints.ToString();
                else
                    pvEnemy.GetComponent<Text>().text = 0.ToString();
            }

            //Background PV pour Evelyn
            if (statsEvelyn.HealthPoints <= 50 && statsEvelyn.HealthPoints > 25)
            {
                //Lancement de l'animation bleue
                //On met l'image sombre à la bonne couleur

                backgroundPVAllyDark.GetComponent<Image>().sprite = FullPvAllyImageDark;
                animatorAllyBackground.Play("Animation_PV_Ally_Full");
                portraitEvelyn.sprite = evelynFull;
            }

            if (statsEvelyn.HealthPoints <= 25 && statsEvelyn.HealthPoints > 07)
            {
                backgroundPVAllyDark.GetComponent<Image>().sprite = HalfPvAllyImageDark;
                animatorAllyBackground.Play("Animation_PV_Half_Ally");
                portraitEvelyn.sprite = evelynMiddle;
            }

            if (statsEvelyn.HealthPoints <= 07)
            {
                backgroundPVAllyDark.GetComponent<Image>().sprite = LowPvAllyImageDark;
                animatorAllyBackground.Play("Animation_PV_Ally_Low");
                portraitEvelyn.sprite = evelynBad;
            }

            if(statsEvelyn.HealthPoints > 0)
                pvAlly.GetComponent<Text>().text = statsEvelyn.HealthPoints.ToString();
            else
                pvAlly.GetComponent<Text>().text = 0.ToString();

            if (manager.giftEvelynActivated)
            {
                GameObject.Find("Icone_Don").GetComponent<Image>().enabled = true;
            }
        }
    }
}
