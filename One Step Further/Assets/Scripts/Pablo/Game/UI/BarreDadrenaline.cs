﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Pathfinding { 
    public class BarreDadrenaline : MonoBehaviour
    {
        //Variables pour le calcul d'adrénaline
        private int additionPAUnites, totalActions;
        public int nombreActionsEvelyn, nombreActionsEnnemi;
        public float pourcentageActionsevelyn, pourcentageActionsEnnemi, divisionVariable;

        //Variables pour les compteurs du premier écran de la phase de combat
        private Text txtEvelyn, txtEnemy;
        private GameObject counterLeftYellow, counterLeftOrange, counterRightYellow, counterRightOrange;

        //Variables pour la jauge en haut de l'écran
        private Image adrenaLineBarYellow, adrenaLinebarOrange;

        private CharacterStats statsEvelyn, statsHenchman1, statsHenchman2, statsAsha, statsCurrentAttacking, statsCurrentAttacked;

        private levelManager managerLevel;

        // Start is called before the first frame update
        void Start()
        {
            statsEvelyn = GameObject.Find("Evelyn").GetComponent<CharacterStats>();
            statsAsha = GameObject.Find("Asha").GetComponent<CharacterStats>();
            statsHenchman1 = GameObject.Find("Sbire1").GetComponent<CharacterStats>();
            statsHenchman2 = GameObject.Find("Sbire2").GetComponent<CharacterStats>();

            txtEvelyn = GameObject.Find("Txt_Compteur_Gauche").GetComponent<Text>();
            txtEnemy = GameObject.Find("Txt_Compteur_Droite").GetComponent<Text>();

            counterLeftYellow = GameObject.Find("Compteur_Filled_Yellow");
            counterLeftOrange = GameObject.Find("Compteur_Filled_Orange_Animation");
            counterRightYellow = GameObject.Find("Counter_Filled_Yellow_Right");
            counterRightOrange = GameObject.Find("Counter_Filled_Orange_Right_Animation");

            adrenaLineBarYellow = GameObject.Find("Adrenaline_Bar_Filled_Yellow").GetComponent<Image>();
            adrenaLinebarOrange = GameObject.Find("Adrenaline_Bar_Filled_Orange_Animation").GetComponent<Image>();

            managerLevel = GameObject.Find("GameManager").GetComponent<levelManager>();
        }

        public void WhatStatsNeeded()
        {

            if (managerLevel.currentCharacterAttacked == "Evelyn")
            {
                statsCurrentAttacked = statsEvelyn;
            }
            else if (managerLevel.currentCharacterAttacked == "Asha")
            {
                statsCurrentAttacked = statsAsha;
            }
            else if (managerLevel.currentCharacterAttacked == "Sbire1")
            {
                statsCurrentAttacked = statsHenchman1;
            }
            else if (managerLevel.currentCharacterAttacked == "Sbire2")
            {
                statsCurrentAttacked = statsHenchman2;
            }

            if (managerLevel.currentCharacterAttacking == "Evelyn")
            {
                statsCurrentAttacking = statsEvelyn;
            }
            else if (managerLevel.currentCharacterAttacking == "Asha")
            {
                statsCurrentAttacking = statsAsha;
            }
            else if (managerLevel.currentCharacterAttacking == "Sbire1")
            {
                statsCurrentAttacking = statsHenchman1;
            }
            else if (managerLevel.currentCharacterAttacking == "Sbire2")
            {
                statsCurrentAttacking = statsHenchman2;
            }

            if(managerLevel.currentCharacterAttacked == "Evelyn")
                calculAdrenalineEvelynAttacked();
            else if(managerLevel.currentCharacterAttacking == "Evelyn")
                calculAdrenaline();
        }

        public void calculAdrenaline()
        {

            additionPAUnites = statsCurrentAttacking.Adrenaline + statsCurrentAttacked.Adrenaline;

            pourcentageActionsevelyn = ((float)statsCurrentAttacking.Adrenaline / (float)additionPAUnites) * 100;
            pourcentageActionsevelyn = Mathf.Round(pourcentageActionsevelyn);
            nombreActionsEvelyn = (int)pourcentageActionsevelyn / 20;

            pourcentageActionsEnnemi = ((float)statsCurrentAttacked.Adrenaline / (float)additionPAUnites) * 100;
            pourcentageActionsEnnemi = Mathf.Round(pourcentageActionsEnnemi);
            nombreActionsEnnemi = (int)pourcentageActionsEnnemi / 20;

            AttributeActionPoints();
        }

        public void calculAdrenalineEvelynAttacked()
        {
            additionPAUnites = statsCurrentAttacking.Adrenaline + statsCurrentAttacked.Adrenaline;

            pourcentageActionsevelyn = ((float)statsCurrentAttacked.Adrenaline / (float)additionPAUnites) * 100;
            pourcentageActionsevelyn = Mathf.Round(pourcentageActionsevelyn);
            nombreActionsEvelyn = (int)pourcentageActionsevelyn / 20;

            pourcentageActionsEnnemi = ((float)statsCurrentAttacking.Adrenaline / (float)additionPAUnites) * 100;
            pourcentageActionsEnnemi = Mathf.Round(pourcentageActionsEnnemi);
            nombreActionsEnnemi = (int)pourcentageActionsEnnemi / 20;

            AttributeActionPoints();
        }

        public void AttributeActionPoints()
        {

            if(managerLevel.currentCharacterAttacked == "Asha")
            {
                statsAsha.nombreActions = nombreActionsEnnemi;              
            }
            else if(managerLevel.currentCharacterAttacked == "Sbire1")
            {
                statsHenchman1.nombreActions = nombreActionsEnnemi;
            }
            else if(managerLevel.currentCharacterAttacked == "Sbire2")
            {
                statsHenchman2.nombreActions = nombreActionsEnnemi;
            }
            else if(managerLevel.currentCharacterAttacked == "Evelyn")
            {
                statsEvelyn.nombreActions = nombreActionsEvelyn;
            }

            if (managerLevel.currentCharacterAttacking == "Asha")
            {
                statsAsha.nombreActions = nombreActionsEnnemi;
            }
            else if (managerLevel.currentCharacterAttacking == "Sbire1")
            {
                statsHenchman1.nombreActions = nombreActionsEnnemi;
            }
            else if (managerLevel.currentCharacterAttacking == "Sbire2")
            {
                statsHenchman2.nombreActions = nombreActionsEnnemi;
            }
            else if (managerLevel.currentCharacterAttacking == "Evelyn")
            {
                statsEvelyn.nombreActions = nombreActionsEvelyn;
            }

            managerLevel.actionsEnemy = nombreActionsEnnemi;
            managerLevel.actionsEvelyn = nombreActionsEvelyn;


            if (managerLevel.currentCharacterAttacking == "Evelyn")
            {
                statsEvelyn.nombreActions = nombreActionsEvelyn;
                managerLevel.actionsEvelyn = statsEvelyn.nombreActions;
            }

            if (managerLevel.actionsEnemy == 0)
            {
                managerLevel.actionsEnemy = 1;
                statsCurrentAttacked.nombreActions = 1;
            }

            if (managerLevel.actionsEvelyn == 0)
            {
                managerLevel.actionsEvelyn = 1;
                statsCurrentAttacking.nombreActions = 1;
            }

            AdrenalineGaugeVisually();
        }

        public void UpdateAdrenalineBar()
        {

            additionPAUnites = statsCurrentAttacking.Adrenaline + statsCurrentAttacked.Adrenaline;


            pourcentageActionsevelyn = ((float)statsCurrentAttacking.Adrenaline / (float)additionPAUnites) * 100;
            pourcentageActionsevelyn = Mathf.Round(pourcentageActionsevelyn);

            pourcentageActionsEnnemi = ((float)statsCurrentAttacked.Adrenaline / (float)additionPAUnites) * 100;
            pourcentageActionsEnnemi = Mathf.Round(pourcentageActionsEnnemi);
        }

        public void AdrenalineGaugeVisually()
        {
            totalActions = 0;
            divisionVariable = 0.0f;

            totalActions = managerLevel.actionsEnemy + managerLevel.actionsEvelyn;
            divisionVariable = (float)1 / totalActions;
            //Debug.Log(totalActions);
            //Debug.Log((float)1 / totalActions);
            //----------Que se passe-t-il visuellement----------
            if (managerLevel.actionsEvelyn > managerLevel.actionsEnemy)
            {
                counterRightYellow.GetComponent<Image>().enabled = false;
                counterRightOrange.GetComponent<Image>().enabled = false;

                counterLeftYellow.GetComponent<Image>().enabled = true;
                counterLeftOrange.GetComponent<Image>().enabled = true;

                adrenaLineBarYellow.fillOrigin = (int)Image.OriginHorizontal.Right;
                adrenaLinebarOrange.fillOrigin = (int)Image.OriginHorizontal.Right;

                adrenaLineBarYellow.fillAmount = (float)(divisionVariable * managerLevel.actionsEvelyn);
                adrenaLinebarOrange.fillAmount = (float)(divisionVariable * managerLevel.actionsEvelyn);

            }
            else if (managerLevel.actionsEvelyn < managerLevel.actionsEnemy)
            {
                counterRightYellow.GetComponent<Image>().enabled = true;
                counterRightOrange.GetComponent<Image>().enabled = true;

                counterLeftYellow.GetComponent<Image>().enabled = false;
                counterLeftOrange.GetComponent<Image>().enabled = false;

                adrenaLineBarYellow.fillOrigin = (int)Image.OriginHorizontal.Left;
                adrenaLinebarOrange.fillOrigin = (int)Image.OriginHorizontal.Left;

                adrenaLineBarYellow.fillAmount = (float)(divisionVariable * managerLevel.actionsEnemy);
                adrenaLinebarOrange.fillAmount = (float)(divisionVariable * managerLevel.actionsEnemy);
            }
            else if (managerLevel.actionsEvelyn == managerLevel.actionsEnemy)
            {
                counterRightYellow.GetComponent<Image>().enabled = true;
                counterRightOrange.GetComponent<Image>().enabled = true;

                counterLeftYellow.GetComponent<Image>().enabled = true;
                counterLeftOrange.GetComponent<Image>().enabled = true;

                adrenaLineBarYellow.fillOrigin = (int)Image.OriginHorizontal.Right;
                adrenaLinebarOrange.fillOrigin = (int)Image.OriginHorizontal.Right;

                adrenaLineBarYellow.fillAmount = 0.5f;
                adrenaLinebarOrange.fillAmount = 0.5f;

            }

            txtEvelyn.text = managerLevel.actionsEvelyn.ToString();
            txtEnemy.text = managerLevel.actionsEnemy.ToString();

            //----------Que se passe-t-il visuellement----------
        }
    }
}