﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationsGameScene : MonoBehaviour
{
    private Animator pauseAnimator;
    private ManagerPunchscreen punchScreenManager;
    private Pathfinding.AttackAi iaAttack;
    private ManagerCounterAttack counterAttackManager;
    private Pathfinding.comboManager managerCombo;
    private CombatEndTurn managerEndTurn;
    private ManagerUIScreens manageUIscreens;
    private GameObject canvasMoving, canvasCombo;
    private GameObject ashaUnits, evelynUnits, henchman1Units, henchman2Units;
    private levelManager managerLevel;
    private CanvasGroup canvasGroupCombo;
    private ContainerAudioClip managerSound;
    private AudioSource comboInputsAudioSource;
    private GameObject managerLevelGO;
    private Pathfinding.TurnManagement scriptTurnManagement;

    // Start is called before the first frame update
    void Start()
    {
        pauseAnimator = GameObject.Find("CanvasPause").GetComponent<Animator>();
        punchScreenManager = GameObject.Find("CanvasPunch").GetComponent<ManagerPunchscreen>();
        iaAttack = GameObject.Find("Units").GetComponent<Pathfinding.AttackAi>();
        counterAttackManager = GameObject.Find("CanvasPunch").GetComponent<ManagerCounterAttack>();
        managerCombo = GameObject.Find("CanvasCombo").GetComponent<Pathfinding.comboManager>();
        managerEndTurn = GameObject.Find("GameManager").GetComponent<CombatEndTurn>();
        manageUIscreens = GameObject.Find("GameManager").GetComponent<ManagerUIScreens>();
        managerLevel = GameObject.Find("GameManager").GetComponent<levelManager>();
        canvasGroupCombo = GameObject.Find("CanvasCombo").GetComponent<CanvasGroup>();
        managerSound = GameObject.Find("SoundManager").GetComponent<ContainerAudioClip>();
        comboInputsAudioSource = GameObject.Find("SFX_Combo_Inputs").GetComponent<AudioSource>();

        ashaUnits = GameObject.Find("Asha");
        evelynUnits = GameObject.Find("Evelyn");
        henchman1Units = GameObject.Find("Sbire1");
        henchman2Units = GameObject.Find("Sbire2");

        canvasMoving = GameObject.Find("CanvasMoving");
        canvasCombo = GameObject.Find("CanvasCombo");

        managerLevelGO = GameObject.Find("GameManager");

        scriptTurnManagement = GameObject.Find("GameManager").GetComponent<Pathfinding.TurnManagement>();
    }

    public void AnimationApparitionPauseAfterOptions()
    {
        pauseAnimator.SetTrigger("apparitionPauseMenu");
    }

    public void BeginVFX()
    {
        if(managerCombo.inputs.Count != 0)
        {
            punchScreenManager.isVFXLaunched = true;
            StartCoroutine(punchScreenManager.LaunchVFXCoroutine());
        }
        else if(counterAttackManager.inputs.Count != 0)
        {
            StartCoroutine(counterAttackManager.LaunchVFX());
        }
        
    }

    public void EndEnemiesSprite()
    {
        if (managerCombo.inputs.Count != 0)
        {
            managerEndTurn.IsCharacterAttackedDead();
        }
        else if (counterAttackManager.inputs.Count != 0)
        {
            counterAttackManager.EndCounterAttack();
        }
    }

    public void FinishAnimationEnemyAttackCombo()
    {
        iaAttack.isComboFinished = true;
        punchScreenManager.LaunchPunchScreenForEnemies();
    }

    public void SlideChangement()
    {
        if (managerLevel.currentCharacterAttacking == "Evelyn")
        {
            manageUIscreens.ClearCanvasComboForEvelyn();
            manageUIscreens.FromMovingToCombat();
        }
        else
        {
            canvasGroupCombo.alpha = 1;
        }

    }

    public void EndSlide()
    {
        Debug.Log("YO2");
        canvasMoving.GetComponent<MovingManager>().isSlideAnimationBegin = false;
        if (managerLevel.currentCharacterAttacking == "Evelyn" && managerLevel.currentCharacterAttacking != "")
        {
            canvasMoving.GetComponent<CanvasGroup>().alpha = 0;
            //gameObject.GetComponent<MovingManager>().DisappearUILeftAndRight();
        }
        else
        {
            //canvasCombo.GetComponent<Animator>().enabled = true;
            //canvasCombo.GetComponent<Animator>().Rebind();
            iaAttack.BeginAttack();
        }
    }

    public void EndSlideReturnCanvasMoving()
    {
        /*
        evelynUnits.GetComponent<Pathfinding.Seeker>().enabled = true;
        evelynUnits.GetComponent<Pathfinding.AIDestinationSetter>().enabled = true;
        evelynUnits.GetComponent<Pathfinding.AILerp>().enabled = true;
        evelynUnits.GetComponent<Pathfinding.Interact>().enabled = true;

        ashaUnits.GetComponent<Pathfinding.Seeker>().enabled = true;
        ashaUnits.GetComponent<Pathfinding.AIDestinationSetter>().enabled = true;
        ashaUnits.GetComponent<Pathfinding.AILerp>().enabled = true;
        ashaUnits.GetComponent<Pathfinding.IA>().enabled = true;

        henchman1Units.GetComponent<Pathfinding.Seeker>().enabled = true;
        henchman1Units.GetComponent<Pathfinding.AIDestinationSetter>().enabled = true;
        henchman1Units.GetComponent<Pathfinding.AILerp>().enabled = true;
        henchman1Units.GetComponent<Pathfinding.IA>().enabled = true;

        henchman2Units.GetComponent<Pathfinding.Seeker>().enabled = true;
        henchman2Units.GetComponent<Pathfinding.AIDestinationSetter>().enabled = true;
        henchman2Units.GetComponent<Pathfinding.AILerp>().enabled = true;
        henchman2Units.GetComponent<Pathfinding.IA>().enabled = true;
        
        if(managerLevelGO.GetComponent<CombatEndTurn>().needNewTurn)
        {
            managerLevelGO.GetComponent<Pathfinding.TurnManagement>().EndOfTurn();
            managerLevelGO.GetComponent<CombatEndTurn>().needNewTurn = false;
        }
        */

        manageUIscreens.AppearButtonsMovingPhase();
        scriptTurnManagement.EndOfTurn();
        gameObject.GetComponent<Animator>().Rebind();
    }

    public void LaunchSoundInputTop()
    {
        comboInputsAudioSource.clip = managerSound.sfxInputTop;
        comboInputsAudioSource.Play();
    }

    public void LaunchSoundInputBottom()
    {
        comboInputsAudioSource.clip = managerSound.sfxInputBottom;
        comboInputsAudioSource.Play();
    }

    public void LaunchSoundLeftOrRight()
    {
        comboInputsAudioSource.clip = managerSound.sfxInputLeftRight;
        comboInputsAudioSource.Play();
    }

    public void DisappearButtonsMovingSlide()
    {
        manageUIscreens.DisappearButtonsMovingPhase();
    }

    public void VictoryOrDefeatAnimation()
    {
        if(managerLevel.victory)
        {
            canvasMoving.GetComponent<Animator>().SetTrigger("Victory");
        }
        else if(managerLevel.defeat)
        {
            canvasMoving.GetComponent<Animator>().SetTrigger("Defeat");
        }
    }

    public void TakingDamage()
    {
        if (gameObject.GetComponentInParent<Pathfinding.AnimationsCharacters>().takeDamage == true)
        {
            gameObject.GetComponentInParent<Pathfinding.AnimationsCharacters>().takeDamage = false;
        }
    }
}
