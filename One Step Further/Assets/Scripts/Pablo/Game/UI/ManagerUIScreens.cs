﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManagerUIScreens : MonoBehaviour
{
    private GameObject canvasCombat, canvasCombo, canvasMoving, canvasCombatFront, canvasCombatBehind, canvasPause, canvasPunch, movingButtonUI;

    private GameObject redDiamondGO, whiteDiamondGO, arrowUpGO, arrowRightGO, arrowDownGO, arrowLeftGO;

    private Pathfinding.BarreDadrenaline adrenalineGauge;

    private levelManager managerLevel;

    private GameObject perfectUI, pushButton, giftButton, endTurnButton, attackButton;

    private Camera mainCamera, cameraPunchOnly;

    private Image edgeInput, imageDefeatRetry, imageDefeatQuit, imageVictoryContinue, imagePhasesPlayer, imagePhasesEnemy;

    private Transform transformParentAfrenaline;

    public bool endCombat, isSlideAnimationBegin;

    private SpriteRenderer punchShield, punchBackground, punchBackgroundEdge, punchBackgroundAbove;

    private Button buttonAttackMoving, buttonGiftMoving, buttonPushMoving, buttonEndOfTurn;

    public Color colorTransparent, colorNormal, colorBlack;

    public Sprite buttonSelected, buttonNotSelected, phasePlayerENG, phasePlayerFR, phaseEnemyFR, phaseEnemyENG;

    // Start is called before the first frame update
    void Start()
    {
        canvasCombat = GameObject.Find("CanvasCombat");
        canvasCombo = GameObject.Find("CanvasCombo");
        canvasMoving = GameObject.Find("CanvasMoving");
        canvasCombatFront = GameObject.Find("CanvasCombatFront");
        canvasCombatBehind = GameObject.Find("CanvasCombatBehind");
        canvasPause = GameObject.Find("CanvasPause");
        canvasPunch = GameObject.Find("CanvasPunch");
        movingButtonUI = GameObject.Find("Button_Hide_Uis");

        adrenalineGauge = GameObject.Find("CanvasCombat").GetComponent<Pathfinding.BarreDadrenaline>();

        managerLevel = GameObject.Find("GameManager").GetComponent<levelManager>();

        mainCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
        cameraPunchOnly = GameObject.Find("CameraUIOnly").GetComponent<Camera>();

        redDiamondGO = GameObject.Find("Losange_Rouge");
        whiteDiamondGO = GameObject.Find("Losange_Blanc");
        arrowUpGO = GameObject.Find("Arrow_Up");
        arrowRightGO = GameObject.Find("Arrow_Right");
        arrowDownGO = GameObject.Find("Arrow_Down");
        arrowLeftGO = GameObject.Find("Arrow_Left");
        perfectUI = GameObject.Find("Announcement_Perfect");

        edgeInput = GameObject.Find("Edge_Input").GetComponent<Image>();
        imagePhasesEnemy = GameObject.Find("Phase Ennemis").GetComponent<Image>();
        imagePhasesPlayer = GameObject.Find("Phase Joueur").GetComponent<Image>();

        transformParentAfrenaline = GameObject.Find("Adrenaline_Bar").GetComponent<Transform>();

        endCombat = false;
        punchBackground = GameObject.Find("Background_Canvas_Punch").GetComponent<SpriteRenderer>();
        punchShield = GameObject.Find("Shield_Center").GetComponent<SpriteRenderer>();
        punchBackgroundEdge = GameObject.Find("Background_Contours_Canvas_Punch").GetComponent<SpriteRenderer>();
        punchBackgroundAbove = GameObject.Find("Above_Background_Canvas_Punch").GetComponent<SpriteRenderer>();

        buttonAttackMoving = GameObject.Find("Bouton_Attaquer").GetComponent<Button>();
        buttonGiftMoving = GameObject.Find("Bouton_Don").GetComponent<Button>();
        buttonPushMoving = GameObject.Find("Bouton_Pousser").GetComponent<Button>();
        buttonEndOfTurn = GameObject.Find("Bouton_Fin_Tour").GetComponent<Button>();

        attackButton = GameObject.Find("Bouton_Attaquer");
        pushButton = GameObject.Find("Bouton_Pousser");
        giftButton = GameObject.Find("Bouton_Don");
        endTurnButton = GameObject.Find("Bouton_Fin_Tour");

        imageDefeatQuit = GameObject.Find("Quit_Button").GetComponent<Image>();
        imageDefeatRetry = GameObject.Find("Retry_Button").GetComponent<Image>();
        imageVictoryContinue = GameObject.Find("Continue_Button").GetComponent<Image>();
    }

    public void Update()
    {
        /*
        if(canvasCombatBehind.GetComponent<Canvas>().renderMode == RenderMode.WorldSpace)
        {
            canvasCombatBehind.GetComponent<RectTransform>().transform.position = new Vector3(959, 538, 689);
            canvasCombatFront.GetComponent<RectTransform>().transform.position = new Vector3(959, 538, 689);
        }
        */

        if(canvasCombat.GetComponent<CanvasGroup>().alpha == 1 || canvasPunch.GetComponent<CanvasGroup>().alpha == 1 || canvasCombo.GetComponent<CanvasGroup>().alpha == 1)
        {
            movingButtonUI.GetComponent<Button>().enabled = false;
            movingButtonUI.GetComponent<Image>().enabled = false;
        }
        else if(canvasMoving.GetComponent<CanvasGroup>().alpha == 1)
        {
            movingButtonUI.GetComponent<Button>().enabled = true;
            movingButtonUI.GetComponent<Image>().enabled = true;
        }

        if (PlayerPrefs.GetInt("Language") == 0)
        {
            imagePhasesEnemy.sprite = phaseEnemyENG;
            imagePhasesPlayer.sprite = phasePlayerENG;
        }
        else
        {
            imagePhasesEnemy.sprite = phaseEnemyFR;
            imagePhasesPlayer.sprite = phasePlayerFR;
        }

        if(managerLevel.currentCharacterAttacked == "Evelyn")
        {
            canvasCombo.GetComponent<Animator>().enabled = true;
            //Debug.Log("1");
        }
        else if(managerLevel.currentCharacterAttacked == "Asha" || managerLevel.currentCharacterAttacked == "Sbire1" || managerLevel.currentCharacterAttacked == "Sbire2")
        {
            //canvasCombo.GetComponent<Animator>().Rebind();
            canvasCombo.GetComponent<Animator>().enabled = false;
            //Debug.Log("2");
        }
    }

    public void DisappearButtonsMovingPhase()
    {
        attackButton.GetComponent<Image>().color = colorTransparent;
        attackButton.GetComponentInChildren<Text>().color = colorTransparent;
        attackButton.GetComponent<Button>().interactable = false;

        giftButton.GetComponent<Image>().color = colorTransparent;
        giftButton.GetComponentInChildren<Text>().color = colorTransparent;
        giftButton.GetComponent<Button>().interactable = false;

        pushButton.GetComponent<Image>().color = colorTransparent;
        pushButton.GetComponentInChildren<Text>().color = colorTransparent;
        pushButton.GetComponent<Button>().interactable = false;

        endTurnButton.GetComponent<Image>().color = colorTransparent;
        endTurnButton.GetComponentInChildren<Text>().color = colorTransparent;
        endTurnButton.GetComponent<Button>().interactable = false;
    }

    public void AppearButtonsMovingPhase()
    {
        attackButton.GetComponent<Image>().color = colorNormal;
        attackButton.GetComponentInChildren<Text>().color = colorNormal;
        //attackButton.GetComponent<Button>().interactable = true;

        giftButton.GetComponent<Image>().color = colorNormal;
        giftButton.GetComponentInChildren<Text>().color = colorNormal;
        //giftButton.GetComponent<Button>().interactable = true;

        pushButton.GetComponent<Image>().color = colorNormal;
        pushButton.GetComponentInChildren<Text>().color = colorNormal;
        //pushButton.GetComponent<Button>().interactable = true;

        endTurnButton.GetComponent<Image>().color = colorNormal;
        endTurnButton.GetComponentInChildren<Text>().color = colorNormal;
        //endTurnButton.GetComponent<Button>().interactable = true;
    }

    public void ManagementAdrenalineBar()
    {
        /*
        if (managerLevel.isInCombatScreen)
        {
            foreach (Transform child in transformParentAfrenaline)
            {

                if (child.GetComponent<Image>() != null)
                {
                    child.GetComponent<Image>().enabled = true;
                }

                if (child.GetComponent<Text>() != null)
                {
                    child.GetComponent<Text>().enabled = true;
                }

                if (child.GetComponent<Animator>() != null)
                {
                    child.GetComponent<Animator>().enabled = true;
                }
            }
        }
        else
        {
            foreach (Transform child in transformParentAfrenaline)
            {
                if (child.GetComponent<Animator>() != null)
                {
                    child.GetComponent<Animator>().enabled = false;
                }

                if (child.GetComponent<Image>() != null)
                {
                    child.GetComponent<Image>().enabled = false;
                }

                if (child.GetComponent<Text>() != null)
                {
                    child.GetComponent<Text>().enabled = false;
                }
            }
        }
        */
    }

    //Function called when Evelyn chose to attack an enemy in Moving Phase
    public void FromMovingToCombat()
    {
        managerLevel.isInMovingPhase = false;
        managerLevel.isInCombatScreen = true;
        //canvasMoving.GetComponent<CanvasGroup>().alpha = 0;
        canvasCombat.GetComponent<CanvasGroup>().alpha = 1;
        //canvasCombatFront.GetComponent<CanvasGroup>().alpha = 1;
        //canvasCombatBehind.GetComponent<CanvasGroup>().alpha = 1;
        adrenalineGauge.WhatStatsNeeded();
        ManagementAdrenalineBar();
    }

    //Function called when players are going to enter their combos
    public void FromCombatToCombo()
    {
        /*
        canvasCombatBackgrounds.SetActive(false);
        canvasCombatLife.SetActive(false);
        canvasCombatActions.SetActive(false);
        canvasCombatAdrenalineBar.SetActive(false);
        */
        managerLevel.isInCombatScreen = false;
        managerLevel.isInComboScreen = true;
        canvasCombat.GetComponent<CanvasGroup>().alpha = 0;
        canvasCombo.GetComponent<CanvasGroup>().alpha = 1;
    }

    //Function called when Players want to return in canvas Combo. They don't want to attack anymore
    public void FromComboToCombat()
    {
        managerLevel.isInCombatScreen = true;
        managerLevel.isInComboScreen = false;
        canvasCombat.GetComponent<Pathfinding.BarreDadrenaline>().UpdateAdrenalineBar();
        perfectUI.GetComponent<Image>().enabled = false;
        canvasCombat.GetComponent<CanvasGroup>().alpha = 1;
        canvasCombo.GetComponent<CanvasGroup>().alpha = 0;
        ManagementAdrenalineBar();
    }

    public void FromMovingToCombo()
    {
        canvasMoving.GetComponent<CanvasGroup>().alpha = 0f;
        canvasCombo.GetComponent<CanvasGroup>().alpha = 1;
        //canvasCombatBehind.GetComponent<CanvasGroup>().alpha = 1;
        //canvasCombatFront.GetComponent<CanvasGroup>().alpha = 1;
        //canvasCombatBehind.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
        //canvasCombatFront.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
    }

    //Function called when a combos is finished
    public void FromComboToPunch()
    {
        punchBackground.enabled = true;
        punchBackgroundAbove.enabled = true;
        punchBackgroundEdge.enabled = true;
        mainCamera.enabled = false;
        cameraPunchOnly.enabled = true;
        canvasCombo.GetComponent<CanvasGroup>().alpha = 0;
        canvasCombat.GetComponent<CanvasGroup>().alpha = 0;
        canvasPunch.GetComponent<CanvasGroup>().alpha = 1;
        punchShield.enabled = false;
        edgeInput.enabled = false;
        //canvasCombatBehind.GetComponent<Canvas>().renderMode = RenderMode.WorldSpace;
        //canvasCombatBehind.GetComponent<Canvas>().worldCamera = cameraPunchOnly;
        //canvasCombatFront.GetComponent<Canvas>().renderMode = RenderMode.WorldSpace;
        //canvasCombatFront.GetComponent<Canvas>().worldCamera = cameraPunchOnly;
    }

    //Function called when game returns in Moving Phase after a combat
    public void FromCombatToMovingPhase()
    {
        //canvasCombatBehind.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
        //canvasCombatFront.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
        canvasPunch.GetComponent<CanvasGroup>().alpha = 0;
        //canvasCombatBehind.GetComponent<CanvasGroup>().alpha = 0;
        //canvasCombatFront.GetComponent<CanvasGroup>().alpha = 0;
        canvasMoving.GetComponent<CanvasGroup>().alpha = 1;
        cameraPunchOnly.enabled = false;
        mainCamera.enabled = true;
        endCombat = true;
        canvasMoving.GetComponent<CanvasGroup>().alpha = 1;
        canvasMoving.GetComponent<Animator>().SetTrigger("EndAttacks");
    }

    //Function called when Evelyn can attack after another attack (enemy or her)
    public void FromPunchToCombat()
    {
        //canvasCombatBehind.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
        //canvasCombatFront.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
        canvasPunch.GetComponent<CanvasGroup>().alpha = 0;
        canvasCombat.GetComponent<CanvasGroup>().alpha = 1;
        managerLevel.isInCombatScreen = true;
        ManagementAdrenalineBar();
    }

    //Function when an enemy has other action points and can attack one more time
    public void FromPunchToCombo()
    {
        //canvasCombatBehind.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
        //canvasCombatFront.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
        punchBackground.enabled = false;
        punchBackgroundAbove.enabled = false;
        punchBackgroundEdge.enabled = false;
        punchShield.enabled = false;
        canvasPunch.GetComponent<CanvasGroup>().alpha = 0;
        canvasCombo.GetComponent<CanvasGroup>().alpha = 1;
    }

    public void FromCombatToPunch()
    {
        punchShield.enabled = true;
        punchBackground.enabled = true;
        punchBackgroundAbove.enabled = true;
        punchBackgroundEdge.enabled = true;
        edgeInput.enabled = true;
        //canvasCombatBehind.GetComponent<Canvas>().renderMode = RenderMode.WorldSpace;
        //canvasCombatBehind.GetComponent<Canvas>().worldCamera = cameraPunchOnly;
        //canvasCombatFront.GetComponent<Canvas>().renderMode = RenderMode.WorldSpace;
        //canvasCombatFront.GetComponent<Canvas>().worldCamera = cameraPunchOnly;
        canvasCombat.GetComponent<CanvasGroup>().alpha = 0;
        canvasPunch.GetComponent<CanvasGroup>().alpha = 1;
        mainCamera.enabled = false;
        cameraPunchOnly.enabled = true;
    }

    public void ClearCanvasComboForEvelyn()
    {
        redDiamondGO.SetActive(true);
        whiteDiamondGO.SetActive(true);
        arrowUpGO.SetActive(true);
        arrowRightGO.SetActive(true);
        arrowDownGO.SetActive(true);
        arrowLeftGO.SetActive(true);
    }

    public void ClearCanvasComboForEnemies()
    {
        redDiamondGO.SetActive(false);
        whiteDiamondGO.SetActive(false);
        arrowUpGO.SetActive(false);
        arrowRightGO.SetActive(false);
        arrowDownGO.SetActive(false);
        arrowLeftGO.SetActive(false);
    }

    public void DisableButtonsInCanvasMoving()
    {
        buttonAttackMoving.interactable = false;
        buttonPushMoving.interactable = false;
        buttonGiftMoving.interactable = false;
        buttonEndOfTurn.interactable = false;
    }

    public void EnableButtonsInCanvasMoving()
    {
        buttonPushMoving.interactable = true;
        buttonGiftMoving.interactable = true;
        buttonEndOfTurn.interactable = true;
    }

    public void ButtonsForEvelyn()
    {
        attackButton.GetComponent<Button>().interactable = false;
        giftButton.GetComponent<Button>().interactable = false;
        pushButton.GetComponent<Button>().interactable = false;
        endTurnButton.GetComponent<Button>().interactable = true;    
    }

    public void ButtonsForEnemies()
    {
        attackButton.GetComponent<Button>().interactable = false;
        giftButton.GetComponent<Button>().interactable = false;
        pushButton.GetComponent<Button>().interactable = false;
        endTurnButton.GetComponent<Button>().interactable = false;
    }

    public void OnEnterDefeatQuit()
    {
        if (managerLevel.defeat)
        {
            imageDefeatQuit.sprite = buttonSelected;
            imageDefeatQuit.GetComponentInChildren<Text>().color = colorBlack;
        }
    }

    public void OnExitDefeatQuit()
    {
        if (managerLevel.defeat)
        {
            imageDefeatQuit.sprite = buttonNotSelected;
            imageDefeatQuit.GetComponentInChildren<Text>().color = colorNormal;
        }
    }

    public void OnEnterDefeatRetry()
    {
        if (managerLevel.defeat)
        {
            imageDefeatRetry.sprite = buttonSelected;
            imageDefeatRetry.GetComponentInChildren<Text>().color = colorBlack;
        }
    }

    public void OnExitDefeatRetry()
    {
        if (managerLevel.defeat)
        {
            imageDefeatRetry.sprite = buttonNotSelected;
            imageDefeatRetry.GetComponentInChildren<Text>().color = colorNormal;
        }
    }

    public void OnEnterVictoryContinue()
    {
        if (managerLevel.victory)
        {
            imageVictoryContinue.sprite = buttonSelected;
            imageVictoryContinue.GetComponentInChildren<Text>().color = colorBlack;
        }
    }

    public void OnExitVictoryContinue()
    {
        if (managerLevel.victory)
        {
            imageVictoryContinue.sprite = buttonNotSelected;
            imageVictoryContinue.GetComponentInChildren<Text>().color = colorNormal;
        }
    }
}
