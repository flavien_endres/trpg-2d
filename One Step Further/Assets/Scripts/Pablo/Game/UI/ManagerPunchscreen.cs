﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManagerPunchscreen : MonoBehaviour
{
    private Pathfinding.comboManager managerCombo;
    private levelManager managerLevel;

    public List<GameObject> vfxLeftGO = new List<GameObject>();
    public List<GameObject> vfxRightGO = new List<GameObject>();
    public List<GameObject> vfxTopGO = new List<GameObject>();
    public List<GameObject> vfxBottomGO = new List<GameObject>();

    private int counterPunch;

    public bool isVFXLaunched, isVfxFinished;

    public float shakeAmount;
    public float decreasedFactor;
    public float shake;

    private GameObject backgrounds, enemyAttacks;

    private IEnumerator coroutine;

    Vector3 cameraInitialPosition;
    public float shakeMagnitude = 0.05f, shakeTime = 0.5f;
    public Camera cameraUI;

    private Pathfinding.AttackAi iaAttack;

    private GameObject canvasCombo, canvasPunch, cameraMain, cameraPunch, canvasMoving, canvasCombat;

    private Pathfinding.TurnManagement managerTurn;

    private CombatEndTurn managerEndTurn;

    private Text pvAllyPunch, pvEnemyPunch;

    private Pathfinding.CharacterStats statsAsha;
    private Pathfinding.CharacterStats statsHenchman1;
    private Pathfinding.CharacterStats statsHenchman2;
    private Pathfinding.CharacterStats statsEvelyn;

    private AudioSource sfxPunch0, sfxPunch1, sfxPunch2, sfxPunch3, sfxPunch4, sfxPunch5, sfxPunch6, sfxPunch7;


    // Start is called before the first frame update
    void Start()
    {
        managerCombo = GameObject.Find("CanvasCombo").GetComponent<Pathfinding.comboManager>();
        managerLevel = GameObject.Find("GameManager").GetComponent<levelManager>();
        backgrounds = GameObject.Find("Backgrounds");
        enemyAttacks = GameObject.Find("Enemy_Attack");

        canvasCombo = GameObject.Find("CanvasCombo");
        canvasPunch = GameObject.Find("CanvasPunch");
        canvasMoving = GameObject.Find("CanvasMoving");
        canvasCombat = GameObject.Find("CanvasCombat");
        cameraMain = GameObject.Find("Main Camera");
        cameraPunch = GameObject.Find("CameraUIOnly");

        counterPunch = 0;
        isVFXLaunched = false;
        isVfxFinished = false;

        iaAttack = GameObject.Find("Units").GetComponent<Pathfinding.AttackAi>();
        managerTurn = GameObject.Find("GameManager").GetComponent<Pathfinding.TurnManagement>();
        managerEndTurn = GameObject.Find("GameManager").GetComponent<CombatEndTurn>();

        cameraInitialPosition = cameraUI.transform.position;

        pvAllyPunch = GameObject.Find("PV_Ally_Punch").GetComponent<Text>();
        pvEnemyPunch = GameObject.Find("PV_Enemy_Punch").GetComponent<Text>();
        statsEvelyn = GameObject.Find("Evelyn").GetComponent<Pathfinding.CharacterStats>();
        statsAsha = GameObject.Find("Asha").GetComponent<Pathfinding.CharacterStats>();
        statsHenchman1 = GameObject.Find("Sbire1").GetComponent<Pathfinding.CharacterStats>();
        statsHenchman2 = GameObject.Find("Sbire2").GetComponent<Pathfinding.CharacterStats>();

        sfxPunch0 = GameObject.Find("SFX_VFX_00").GetComponent<AudioSource>();
        sfxPunch1 = GameObject.Find("SFX_VFX_01").GetComponent<AudioSource>();
        sfxPunch2 = GameObject.Find("SFX_VFX_02").GetComponent<AudioSource>();
        sfxPunch3 = GameObject.Find("SFX_VFX_03").GetComponent<AudioSource>();
        sfxPunch4 = GameObject.Find("SFX_VFX_04").GetComponent<AudioSource>();
        sfxPunch5 = GameObject.Find("SFX_VFX_05").GetComponent<AudioSource>();
        sfxPunch6 = GameObject.Find("SFX_VFX_06").GetComponent<AudioSource>();
        sfxPunch7 = GameObject.Find("SFX_VFX_07").GetComponent<AudioSource>();

    }

    public void StartCameraShaking()
    {
        float cameraShakingOffsetX = Random.value * shakeMagnitude * 2 - shakeMagnitude;
        float cameraShakingOffsetY = Random.value * shakeMagnitude * 2 - shakeMagnitude;
        Vector3 cameraIntermediatePosition = cameraUI.transform.position;
        cameraIntermediatePosition.x += cameraShakingOffsetX;
        cameraIntermediatePosition.y += cameraShakingOffsetY;
        cameraUI.transform.position = cameraIntermediatePosition;
    }

    public void StopCameraShaking()
    {
        CancelInvoke("StartCameraShaking");
        cameraUI.transform.position = cameraInitialPosition;
    }

    // Update is called once per frame
    void Update()
    {
        if(managerLevel.currentCharacterAttacked == "Asha" || managerLevel.currentCharacterAttacking == "Asha")
        {
            if (statsAsha.HealthPoints > 0)
                pvEnemyPunch.GetComponent<Text>().text = statsAsha.HealthPoints.ToString();
            else
                pvEnemyPunch.GetComponent<Text>().text = 0.ToString();
        }
        else if(managerLevel.currentCharacterAttacked == "Sbire1" || managerLevel.currentCharacterAttacking == "Sbire1")
        {
            if (statsHenchman1.HealthPoints > 0)
                pvEnemyPunch.GetComponent<Text>().text = statsHenchman1.HealthPoints.ToString();
            else
                pvEnemyPunch.GetComponent<Text>().text = 0.ToString();
        }
        else if(managerLevel.currentCharacterAttacked == "Sbire2" || managerLevel.currentCharacterAttacking == "Sbire2")
        {
            if (statsHenchman2.HealthPoints > 0)
                pvEnemyPunch.GetComponent<Text>().text = statsHenchman2.HealthPoints.ToString();
            else
                pvEnemyPunch.GetComponent<Text>().text = 0.ToString();
        }

        if (statsEvelyn.HealthPoints > 0)
            pvAllyPunch.GetComponent<Text>().text = statsEvelyn.HealthPoints.ToString();
        else
            pvAllyPunch.GetComponent<Text>().text = 0.ToString();
    }

    public void LaunchPunchScreenForEnemies()
    {
        GameObject.Find("GameManager").GetComponent<ManagerUIScreens>().FromComboToPunch();
        //Debug.Log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        
        canvasCombo.GetComponent<CanvasGroup>().alpha = 0;
        canvasPunch.GetComponent<CanvasGroup>().alpha = 1;
        cameraMain.GetComponent<Camera>().enabled = false;
        cameraPunch.GetComponent<Camera>().enabled = true;
        
        isVFXLaunched = true;
        if(isVFXLaunched)
            StartCoroutine(LaunchVFXCoroutine());
    }

    public IEnumerator LaunchVFXCoroutine()
    {
        //Determines scale of VFX
        //If it is an enemy that is attacked, scale is 60
        // It is Evelyn is attacked, scale is 120
        if (iaAttack.isComboFinished)
        {
            foreach (GameObject gameobjectGO in vfxLeftGO)
            {
                gameobjectGO.transform.localScale = new Vector3(120, 120, 120);
            }
            foreach (GameObject gameobjectGO in vfxRightGO)
            {
                gameobjectGO.transform.localScale = new Vector3(120, 120, 120);
            }
            foreach (GameObject gameobjectGO in vfxTopGO)
            {
                gameobjectGO.transform.localScale = new Vector3(120, 120, 120);
            }
            foreach (GameObject gameobjectGO in vfxBottomGO)
            {
                gameobjectGO.transform.localScale = new Vector3(120, 120, 120);
            }
        }
        else
        {
            foreach (GameObject gameobjectGO in vfxLeftGO)
            {
                gameobjectGO.transform.localScale = new Vector3(60, 60, 60);
            }
            foreach (GameObject gameobjectGO in vfxRightGO)
            {
                gameobjectGO.transform.localScale = new Vector3(60, 60, 60);
            }
            foreach (GameObject gameobjectGO in vfxTopGO)
            {
                gameobjectGO.transform.localScale = new Vector3(60, 60, 60);
            }
            foreach (GameObject gameobjectGO in vfxBottomGO)
            {
                gameobjectGO.transform.localScale = new Vector3(60, 60, 60);
            }
        }

        if(managerLevel.currentCharacterAttacking == "Evelyn")
        {
            canvasCombo.GetComponent<Pathfinding.comboManager>().DamageCalculation();
        }

        /*
        if (managerLevel.currentCharacterAttacked == "Asha")
        {
            gameObject.GetComponent<Animator>().Play("Combat_Asha_Take_Degats");
        }
        else if (managerLevel.currentCharacterAttacked == "Sbire1" || managerLevel.currentCharacterAttacked == "Sbire2")
        {
            gameObject.GetComponent<Animator>().Play("Combat_Henchman_Take_Degats");
        }
        */
        for (int i = 0; i < managerCombo.inputs.Count; i++)
        {
            switch (i)
            {
                case 0:
                    sfxPunch0.Play();
                    break;
                case 1:
                    sfxPunch1.Play();
                    break;
                case 2:
                    sfxPunch2.Play(); 
                    break;
                case 3:
                    sfxPunch3.Play();
                    break;
                case 4:
                    sfxPunch4.Play();
                    break;
                case 5:
                    sfxPunch5.Play();
                    break;
                case 6:
                    sfxPunch6.Play();
                    break;
                case 7:
                    sfxPunch7.Play();
                    break;
            }

            if (managerCombo.inputs[i] == "Gauche")
            {
                switch (i)
                {
                    case 0:
                        vfxLeftGO[0].GetComponent<ParticleSystem>().Play();
                        break;

                    case 1:
                        vfxLeftGO[1].GetComponent<ParticleSystem>().Play();
                        break;

                    case 2:
                        vfxLeftGO[2].GetComponent<ParticleSystem>().Play();
                        break;

                    case 3:
                        vfxLeftGO[3].GetComponent<ParticleSystem>().Play();
                        break;

                    case 4:
                        vfxLeftGO[4].GetComponent<ParticleSystem>().Play();
                        break;

                    case 5:
                        vfxLeftGO[5].GetComponent<ParticleSystem>().Play();
                        break;

                    case 6:
                        vfxLeftGO[6].GetComponent<ParticleSystem>().Play();
                        break;

                    case 7:
                        vfxLeftGO[7].GetComponent<ParticleSystem>().Play();
                        break;
                }
            }
            else if (managerCombo.inputs[i] == "Bas")
            {
                switch (i)
                {
                    case 0:
                        vfxBottomGO[0].GetComponent<ParticleSystem>().Play();
                        break;

                    case 1:
                        vfxBottomGO[1].GetComponent<ParticleSystem>().Play();
                        break;

                    case 2:
                        vfxBottomGO[2].GetComponent<ParticleSystem>().Play();
                        break;

                    case 3:
                        vfxBottomGO[3].GetComponent<ParticleSystem>().Play();
                        break;

                    case 4:
                        vfxBottomGO[4].GetComponent<ParticleSystem>().Play();
                        break;

                    case 5:
                        vfxBottomGO[5].GetComponent<ParticleSystem>().Play();
                        break;

                    case 6:
                        vfxBottomGO[6].GetComponent<ParticleSystem>().Play();
                        break;

                    case 7:
                        vfxBottomGO[7].GetComponent<ParticleSystem>().Play();
                        break;
                }
            }
            else if (managerCombo.inputs[i] == "Droite")
            {
                switch (i)
                {
                    case 0:
                        vfxRightGO[0].GetComponent<ParticleSystem>().Play();
                        break;

                    case 1:
                        vfxRightGO[1].GetComponent<ParticleSystem>().Play();
                        break;

                    case 2:
                        vfxRightGO[2].GetComponent<ParticleSystem>().Play();
                        break;

                    case 3:
                        vfxRightGO[3].GetComponent<ParticleSystem>().Play();
                        break;

                    case 4:
                        vfxRightGO[4].GetComponent<ParticleSystem>().Play();
                        break;

                    case 5:
                        vfxRightGO[5].GetComponent<ParticleSystem>().Play();
                        break;

                    case 6:
                        vfxRightGO[6].GetComponent<ParticleSystem>().Play();
                        break;

                    case 7:
                        vfxRightGO[7].GetComponent<ParticleSystem>().Play();
                        break;
                }
            }
            else if (managerCombo.inputs[i] == "Haut")
            {
                switch (i)
                {
                    case 0:
                        vfxTopGO[0].GetComponent<ParticleSystem>().Play();
                        break;

                    case 1:
                        vfxTopGO[1].GetComponent<ParticleSystem>().Play();
                        break;

                    case 2:
                        vfxTopGO[2].GetComponent<ParticleSystem>().Play();
                        break;

                    case 3:
                        vfxTopGO[3].GetComponent<ParticleSystem>().Play();
                        break;

                    case 4:
                        vfxTopGO[4].GetComponent<ParticleSystem>().Play();
                        break;

                    case 5:
                        vfxTopGO[5].GetComponent<ParticleSystem>().Play();
                        break;

                    case 6:
                        vfxTopGO[6].GetComponent<ParticleSystem>().Play();
                        break;

                    case 7:
                        vfxTopGO[7].GetComponent<ParticleSystem>().Play();
                        break;
                }
                
            }

            InvokeRepeating("StartCameraShaking", 0f, 0.005f);
            Invoke("StopCameraShaking", shakeTime);

            WaitForSeconds wait = new WaitForSeconds(0.20f);
            yield return wait;
        }

        iaAttack.isComboFinished = false;
        isVFXLaunched = false;

        if (managerLevel.currentCharacterAttacked == "Asha")
        {
            gameObject.GetComponent<Animator>().SetTrigger("ashaDisparition");
        }
        else if (managerLevel.currentCharacterAttacked == "Sbire1" || managerLevel.currentCharacterAttacked == "Sbire2")
        {
            gameObject.GetComponent<Animator>().SetTrigger("henchmanDisparition");
        }
        else
        {
            managerEndTurn.IsCharacterAttackedDead();
        }
            yield return null;
        //}
    }
}
