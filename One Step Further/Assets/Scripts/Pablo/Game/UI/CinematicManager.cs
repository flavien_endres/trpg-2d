﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CinematicManager : MonoBehaviour
{
    [TextArea]
    [Tooltip("List of all dialogs in the cinematic")]
    public List<string> dialogTextList = new List<string>();

    [TextArea]
    [Tooltip("List of all dialogs in the cinematic IN ENGLISH")]
    public List<string> EnglishDialogTextList = new List<string>();


    [Tooltip("List of all dialogs boxes in the cinematic. Use 'right' for right boxes and 'left' for left boxes")]
    public List<string> uiDialogList = new List<string>();

    [Tooltip("List of all sprite characters. Evelyn 'eveSurprised', 'eveNormal', 'eveAngry'    Asha 'ashaNormal'    " +
        "Henchmans 'noEnemy'")]
    public List<string> spriteCharacterList = new List<string>();

    public List<string> nameList = new List<string>();

    [Header("Dialog UI")]
    public Sprite uiDialogRight;
    public Sprite uiDialogLeft;

    private Image spriteEvelyn, spriteAsha;

    private Text dialogText, enemiesNameText, evelynNameText, skipText;
    private float letterPause;
    public string textComp;
    private Animator animCanvas, animatorEvelyn, animatorAsha, animatorOnomatopoeia, animatorOnomatopeiaAsha;

    public int currentDialogsIndex, firstModificationIndex, secondModificationIndex;

    private GameObject uiDialogGO, backgroundsGO, allyGO, enemyGO;

    public bool endDisplayText;

    private bool ashaAppearing, IsDisplayTextSoundLaunched;

    private Scene currentScene;

    private ContainerAudioClip scriptConatiner;

    private AudioSource sourceDisplayingText, sourceAmbiances, sourceReactions, sourceSFX, sourceMusic;

    private int whoIsTalking;

    private LoadingScreen scriptLoadingScreen;

    public Color colorNormal, colorThinking;

    // Start is called before the first frame update
    void Start()
    {
        currentScene = SceneManager.GetActiveScene();
        dialogText = GameObject.Find("Dialog_Text").GetComponent<Text>();
        enemiesNameText = GameObject.Find("Name_Dialog_Left").GetComponent<Text>();
        evelynNameText = GameObject.Find("Name_Dialog_Right").GetComponent<Text>();
        skipText = GameObject.Find("SkipText").GetComponent<Text>();
        uiDialogGO = GameObject.Find("UI_Dialog");
        backgroundsGO = GameObject.Find("Backgrounds");
        allyGO = GameObject.Find("Face_Evelyn");
        enemyGO = GameObject.Find("Face_Enemies");
        animCanvas = GameObject.Find("Canvas").GetComponent<Animator>();
        animatorEvelyn = GameObject.Find("Face_Evelyn").GetComponent<Animator>();
        animatorAsha = GameObject.Find("Face_Enemies").GetComponent<Animator>();
        animatorOnomatopoeia = GameObject.Find("Onomatopoeia_Evelyn").GetComponent<Animator>();
        spriteEvelyn = GameObject.Find("Face_Evelyn").GetComponent<Image>();
        spriteAsha = GameObject.Find("Face_Enemies").GetComponent<Image>();
        scriptConatiner = GetComponent<ContainerAudioClip>();
        sourceDisplayingText = GameObject.Find("Display_Text").GetComponent<AudioSource>();
        sourceAmbiances = GameObject.Find("Ambiances").GetComponent<AudioSource>();
        sourceReactions = GameObject.Find("Reactions").GetComponent<AudioSource>();
        sourceSFX = GameObject.Find("sounds_SFX").GetComponent<AudioSource>();
        sourceMusic = GameObject.Find("Music").GetComponent<AudioSource>();
        scriptLoadingScreen = GameObject.Find("CanvasLoadingScreen").GetComponent<LoadingScreen>();
        letterPause = 0.01f;

        if (currentScene.name == "CinematicEnding")
        {
            animatorOnomatopeiaAsha = GameObject.Find("Onomatopoeia_Asha").GetComponent<Animator>();
            sourceAmbiances.clip = scriptConatiner.ambianceBar;
            sourceAmbiances.Play();
        }
        else if(currentScene.name == "CinematicIntro")
        {
            sourceAmbiances.clip = scriptConatiner.ambianceRooftop;
            sourceAmbiances.Play();
        }

        if (PlayerPrefs.GetInt("Language") == 0)
        {
            skipText.text = "Press ESCAPE to skip.";
        }
        else
        {
            skipText.text = "Appuyez sur ESPACE pour passer.";
        }

        StartCoroutine(BeginningCinematic());
    }

    private void Update()
    {
       if (Input.GetKeyDown(KeyCode.Space))
        {
            if (currentScene.name == "CinematicIntro")
            {
                scriptLoadingScreen.LoadLevel(2);
            }
            else if (currentScene.name == "CinematicEnding")
            {
                scriptLoadingScreen.LoadLevel(3);
            }
        }
    }

    private IEnumerator BeginningCinematic()
    {
        if (currentScene.name == "CinematicIntro")
        {
            WaitForSeconds wait1 = new WaitForSeconds(2f);
            yield return wait1;
        }
        else if (currentScene.name == "CinematicEnding")
        {
            WaitForSeconds wait2 = new WaitForSeconds(1.5f);
            yield return wait2;
        }

        animCanvas.SetTrigger("beginning");

        WaitForSeconds wait = new WaitForSeconds(0.40f);
        yield return wait;

        //if (currentScene.name == "CinematicEnding")
            //animatorEvelyn.Play("EvelynNoClothesDark");

        StartCoroutine(CinematicCoroutine());
    }

    public IEnumerator TypeText()
    {
        foreach (char letter in textComp.ToCharArray())
        {
            if(!IsDisplayTextSoundLaunched)
            {
                switch(whoIsTalking)
                {
                    case 0:
                        sourceDisplayingText.clip = scriptConatiner.sfxDisplayTextEvelyn;
                        sourceDisplayingText.Play();
                        break;
                    case 1:
                        sourceDisplayingText.clip = scriptConatiner.sfxDisplayTextHenchman;
                        sourceDisplayingText.Play();
                        break;
                    case 2:
                        sourceDisplayingText.clip = scriptConatiner.sfxDisplayTextAsha;
                        sourceDisplayingText.Play();
                        break;
                }
                IsDisplayTextSoundLaunched = true;
            }

            dialogText.text += letter;
            yield return new WaitForSeconds(letterPause);

        }
        endDisplayText = true;
        sourceDisplayingText.Stop();
        IsDisplayTextSoundLaunched = false;
    }

    public IEnumerator CinematicCoroutine()
    {
        yield return null;

        for (int i = 0; i < Mathf.Min(dialogTextList.Count, EnglishDialogTextList.Count, nameList.Count, spriteCharacterList.Count); i++)
        {
            dialogText.color = colorNormal;

            if(currentScene.name == "CinematicIntro")
            {
                if(i == 3)
                {
                    sourceSFX.clip = scriptConatiner.sfxFtNormal;
                    sourceSFX.Play();
                }
                else if(i == 7)
                {
                    sourceSFX.clip = scriptConatiner.sfxPipeGrab;
                    sourceSFX.Play();
                }
                else if(i == 8)
                {
                    sourceMusic.enabled = true; 
                }
                else if(i == 11)
                {
                    dialogText.color = colorThinking;
                    sourceSFX.clip = scriptConatiner.sfxFtLow;
                    sourceSFX.Play();
                }
                else if (i == 12)
                {
                    sourceSFX.clip = scriptConatiner.sfxGunfire;
                    sourceSFX.Play();
                }
                else if (i == 13)
                {
                    sourceSFX.clip = scriptConatiner.sfxGunLoading;
                    sourceSFX.Play();
                }
                else if (i == 14)
                {
                    sourceSFX.clip = scriptConatiner.sfxPipeFall;
                    sourceSFX.Play();
                }
            }

            //===CODE FOR CHARACTERS SPRITES===

            switch (spriteCharacterList[i])
            {

                case "eveNormal":
                    animatorEvelyn.Play("EvelynNoClothesNormal");
                    break;

                case "eveNormalNoTalking":
                    animatorEvelyn.Play("NoTalkingEvelynNoClothesNormal");
                    break;

                case "eveSurprised":
                    animatorEvelyn.Play("EvelynNoClothesSurprised");
                    animatorOnomatopoeia.SetTrigger("introggation");
                    if(ashaAppearing && currentScene.name == "CinematicIntro")
                    {
                        animatorAsha.Play("AshaDark");
                    }
                    sourceReactions.clip = scriptConatiner.soundSurprisedReaction;
                    sourceReactions.Play();
                    break;

                case "eveSurprisedNoTalking":
                    animatorEvelyn.Play("NoTalkingEvelynNoClothesSurprised");
                    break;

                case "eveAngry":
                    animatorEvelyn.Play("EvelynNoClothesAngry");
                    animatorOnomatopoeia.SetTrigger("exclamation");
                    sourceReactions.clip = scriptConatiner.soundAngryReaction;
                    sourceReactions.Play();
                        break;

                case "eveClothesAngry":
                    animatorEvelyn.Play("EvelynClothesAngry");
                    animatorOnomatopoeia.SetTrigger("exclamation");
                    sourceReactions.clip = scriptConatiner.soundAngryReaction;
                    sourceReactions.Play();
                    animatorAsha.Play("AshaDark");
                    break;

                case "eveAngryNoTalking":
                    animatorEvelyn.Play("NoTalkingEvelynNoClothesAngry");
                    break;

                case "ashaNormal":
                    animatorAsha.Rebind();
                    animatorAsha.Play("AshaNormal");
                    if (currentScene.name == "CinematicIntro")
                    {
                        animatorEvelyn.Play("EvelynNoClothesDark");
                        ashaAppearing = true;
                    }
                    else
                        animatorEvelyn.Play("NoTalkingClothesEvelynNormal");
                    break;

                case "ashaAngry":
                    animatorAsha.Rebind();
                    animatorAsha.Play("AshaAngry");
                    animatorOnomatopeiaAsha.SetTrigger("exclamation");
                    sourceReactions.clip = scriptConatiner.soundAngryReaction;
                    sourceReactions.Play();
                    if (currentScene.name == "CinematicEnding")
                        animatorEvelyn.Play("NoTalkingClothesEvelynNormal");
                    else
                        animatorEvelyn.Play("EvelynNoClothesDark");

                    break;

                case "noSprites":
                    animatorEvelyn.Play("EvelynNoClothesDark");
                    break;

                case "noEnemySprite":
                    spriteAsha.enabled = false;
                    break;
            }

            //===CODE FOR CHARACTERS SPRITES===

            switch(nameList[i])
            {
                case "evelyn":
                    enemiesNameText.enabled = false;
                    evelynNameText.enabled = true;
                    evelynNameText.text = "Evelyn";
                    uiDialogGO.GetComponent<Image>().sprite = uiDialogRight;
                    whoIsTalking = 0;
                    break;
                case "man1":
                    enemiesNameText.enabled = true;
                    evelynNameText.enabled = false;

                    if (PlayerPrefs.GetInt("Language") == 0)
                    {
                        //EnglishText
                        enemiesNameText.text = "Men's voice #1";
                    }
                    else
                    {
                        //FrenchText
                        enemiesNameText.text = "Voix masculine #1";
                    }

                    uiDialogGO.GetComponent<Image>().sprite = uiDialogLeft;
                    whoIsTalking = 1;
                    break;
                case "man2":
                    enemiesNameText.enabled = true;
                    evelynNameText.enabled = false;

                    if (PlayerPrefs.GetInt("Language") == 0)
                    {
                        //EnglishText
                        enemiesNameText.text = "Men's voice #2";
                    }
                    else
                    {
                        //FrenchText
                        enemiesNameText.text = "Voix masculine #2";
                    }

                    uiDialogGO.GetComponent<Image>().sprite = uiDialogLeft;
                    whoIsTalking = 1;
                    break;
                case "asha":
                    enemiesNameText.enabled = true;
                    evelynNameText.enabled = false;

                    if (PlayerPrefs.GetInt("Language") == 0)
                    {
                        //EnglishText
                        enemiesNameText.text = "Woman's voice";
                    }
                    else
                    {
                        //FrenchText
                        enemiesNameText.text = "Voix féminine";
                    }

                    uiDialogGO.GetComponent<Image>().sprite = uiDialogLeft;
                    whoIsTalking = 2;
                    break;
                case "ashaSpecific":
                    enemiesNameText.enabled = true;
                    evelynNameText.enabled = false;

                    if (PlayerPrefs.GetInt("Language") == 0)
                    {
                        //EnglishText
                        enemiesNameText.text = "Young woman";
                    }
                    else
                    {
                        //FrenchText
                        enemiesNameText.text = "Jeune femme";
                    }

                    uiDialogGO.GetComponent<Image>().sprite = uiDialogLeft;
                    whoIsTalking = 2;
                    break;
                case "Unknown":
                    enemiesNameText.enabled = true;
                    evelynNameText.enabled = false;
                    enemiesNameText.text = "???";
                    uiDialogGO.GetComponent<Image>().sprite = uiDialogLeft;
                    whoIsTalking = 1;
                    break;
            }

            //===CODE FOR DIALOGS===
            endDisplayText = false;
            textComp = "";
            dialogText.text = "";

            if (PlayerPrefs.GetInt("Language") == 0)
            {
                textComp = EnglishDialogTextList[i];
            }
            else
            {
                textComp = dialogTextList[i];
            }
            currentDialogsIndex = i;
            StartCoroutine(TypeText());
            //===CODE FOR DIALOGS===

            yield return waitForKeyPress(KeyCode.Return);
               
            
        }

        if (currentScene.name == "CinematicIntro")
        {
            scriptLoadingScreen.LoadLevel(2);
        }
        else if (currentScene.name == "CinematicEnding")
        {
            scriptLoadingScreen.LoadLevel(3);
        }
    }

    private IEnumerator waitForKeyPress(KeyCode key)
    {
        WaitForSeconds wait;
        bool done = false;
        while (!done) // essentially a "while true", but with a bool to break out naturally
        {
            if(Input.GetKeyDown(key) || Input.GetMouseButtonDown(0))
            {
                if (endDisplayText)
                {
                    endDisplayText = false;
                    textComp = "";
                    dialogText.text = "";
                    done = true; // breaks the loop
                }
            }


            yield return null; // wait until next frame, then continue execution from here (loop continues)
        }

        if (currentScene.name == "CinematicIntro")
        {
            if (currentDialogsIndex == firstModificationIndex)
            {
                evelynNameText.text = "";
                animCanvas.SetTrigger("disappear");
                wait = new WaitForSeconds(1f);
                yield return wait;
                evelynNameText.text = "";
                animatorEvelyn.Play("EvelynNoClothesDark");
                animCanvas.SetTrigger("backgroundBehindBar");
                wait = new WaitForSeconds(1.25f);
                yield return wait;
                animCanvas.SetTrigger("beginning");
                sourceAmbiances.clip = scriptConatiner.ambianceBar;
                sourceAmbiances.Play();
                wait = new WaitForSeconds(0.5f);
                yield return wait;

            }
            else if (currentDialogsIndex == secondModificationIndex)
            {
                animCanvas.SetTrigger("disappear");
                wait = new WaitForSeconds(1f);
                yield return wait;
                animCanvas.SetTrigger("backgroundInBar");
                wait = new WaitForSeconds(1.25f);
                yield return wait;
                animCanvas.SetTrigger("beginning");
                wait = new WaitForSeconds(0.5f);
                yield return wait;
            }
        }      

    }
}
