﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingScreen : MonoBehaviour
{
    private CanvasGroup canvasGroupLoading;
    private Slider sliderLoading;
    private levelManager managerLevel;
    private Scene currentScene;

    public void Start()
    {
        canvasGroupLoading = GameObject.Find("CanvasLoadingScreen").GetComponent<CanvasGroup>();
        sliderLoading = GameObject.Find("SliderLoadingScreen").GetComponent<Slider>();
        currentScene = SceneManager.GetActiveScene();

        if (currentScene.name == "GameScene")
            managerLevel = GameObject.Find("GameManager").GetComponent<levelManager>();
    }

    public void LoadLevel (int sceneIndex)
    {
        if(currentScene.name == "GameScene")
        {
            if(managerLevel.isInPauseMenu || managerLevel.victory || managerLevel.defeat)
            {
                StartCoroutine(LoadAsynchronously(sceneIndex));
            }
        }
        else
        {
            StartCoroutine(LoadAsynchronously(sceneIndex));
        }
    }

    IEnumerator LoadAsynchronously (int sceneIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);
        canvasGroupLoading.alpha = 1;

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);
            sliderLoading.value = progress;

            yield return null;
        }
    }
}
