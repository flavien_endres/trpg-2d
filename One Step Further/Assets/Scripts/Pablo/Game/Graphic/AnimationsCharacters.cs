﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pathfinding
{

    public class AnimationsCharacters : MonoBehaviour
    {
        //public 
        public AILerp aiLerp;
        public Animator animatorAnimationsCharacters;

        public bool lastNorthWest, lastNorthEast, lastSouthWest, lastSouthEast, hasMoved;

        //private AudioSource sourceFootsteps;

        public bool isFootstepSoundLaunched, takeDamage;

    // Start is called before the first frame update
    void Start()
        {
            animatorAnimationsCharacters = GetComponentInChildren<Animator>();
            aiLerp = gameObject.GetComponent<AILerp>();
            hasMoved = false;
            isFootstepSoundLaunched = false;

            //sourceFootsteps = GameObject.Find("SFX_Footsteps").GetComponent<AudioSource>();
            
        }

        // Update is called once per frame
        void Update()
        {
            if(!hasMoved)
            {
                if (lastNorthWest)
                {
                    animatorAnimationsCharacters.Play("Idle_North_West");
                }
                else if (lastNorthEast)
                {
                    animatorAnimationsCharacters.Play("Idle_North_East");
                }
                else if (lastSouthWest)
                {
                    animatorAnimationsCharacters.Play("Idle_South_West");
                }
                else if (lastSouthEast)
                {
                    animatorAnimationsCharacters.Play("Idle_South_East");
                }
            }

            IAstarAI ai = GetComponent<AILerp>();
            var velocity = ai.velocity;
            //Debug.Log("X: " + velocity.x);
            //Debug.Log("Y: " + velocity.y);

            //Evelyn va à gauche
            if(velocity.x > 0 && velocity.y > 0 && ai.reachedEndOfPath != true)
            {
                /*
                if(!isFootstepSoundLaunched)
                {
                    //Debug.Log("coucou");
                    sourceFootsteps.Play();
                    isFootstepSoundLaunched = true;
                }
                */
                hasMoved = true;
                animatorAnimationsCharacters.Play("Run_North_West");
                //Debug.Log("animation GAUCHE");

                lastNorthWest = true;
                lastNorthEast = false;
                lastSouthEast = false;
                lastSouthWest = false;
            }

            //Evelyn va à droite
            if(velocity.x < 0 && velocity.y < 0 && ai.reachedEndOfPath != true)
            {
                /*
                if (!isFootstepSoundLaunched)
                {
                    //Debug.Log("coucou2");
                    sourceFootsteps.Play();
                    isFootstepSoundLaunched = true;
                }
                */
                hasMoved = true;
                animatorAnimationsCharacters.Play("Run_South_East");
                //Debug.Log("animation DROITE");

                lastNorthEast = false;
                lastNorthWest = false;
                lastSouthEast = true;
                lastSouthWest = false;
            }

            //Evelyn va en haut
            if(velocity.x < 0 && velocity.y > 0 && ai.reachedEndOfPath != true)
            {
                /*
                if (!isFootstepSoundLaunched)
                {
                    //Debug.Log("coucou3");
                    sourceFootsteps.Play();
                    isFootstepSoundLaunched = true;
                }
                */
                hasMoved = true;
                animatorAnimationsCharacters.Play("Run_North_East");
                //Debug.Log("animation HAUT");

                lastSouthEast = false;
                lastNorthEast = true;
                lastNorthWest = false;
                lastSouthWest = false;
            }

            //Evelyn va en bas
            if( velocity.x > 0 && velocity.y < 0 && ai.reachedEndOfPath != true)
            {
                /*
                if (!isFootstepSoundLaunched)
                {
                    //Debug.Log("coucou4");
                    sourceFootsteps.Play();
                    isFootstepSoundLaunched = true;
                }
                */
                hasMoved = true;
                animatorAnimationsCharacters.Play("Run_South_West");
                //Debug.Log("animation BAS");

                lastSouthWest = true;
                lastSouthEast = false;
                lastNorthEast = false;
                lastNorthWest = false;

            }
            //From the moment that Evelyn has moved
            if(ai.reachedEndOfPath == true)
            {

                if (lastNorthWest && !takeDamage)
                {
                    animatorAnimationsCharacters.Play("Idle_North_West");
                }
                else if(lastNorthEast && !takeDamage)
                {
                    animatorAnimationsCharacters.Play("Idle_North_East");
                }
                else if(lastSouthWest && !takeDamage)
                {
                    animatorAnimationsCharacters.Play("Idle_South_West");
                }
                else if(lastSouthEast && !takeDamage)
                {
                    animatorAnimationsCharacters.Play("Idle_South_East");
                }
                //sourceFootsteps.Stop();
                //Debug.Log("Salut");
                //isFootstepSoundLaunched = false;
            }
            /*
            //At the begininning of the scene
            if (velocity.x == 0 && velocity.y == 0 && ai.reachedEndOfPath != true)
            {
                animatorAnimationsCharacters.Play("Idle");
                //Debug.Log("Fausse animation d'idle");
            }
            */
        }
    }
}

