﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CreditsManager : MonoBehaviour
{

    public ScrollRect scrollRect;
    private bool isScrolling = false;
    public bool isNormalizedPosition = false;
    private float timer = 0;
    public float timeBeforeAutoScroll = 2;
    public float speedAutoScroll = 30;

    public Sprite buttonEnter, buttonExit;
    public Color colorEnter, colorExit;
    public GameObject buttonReturn, txtButtonReturn;

    private void Update()
    {

        if (scrollRect.gameObject.activeSelf == true && !isNormalizedPosition)
        {
            scrollRect.verticalNormalizedPosition = 1;
            isNormalizedPosition = true;
        }

        if (!isScrolling)
        {
            timer += Time.deltaTime;
        }

        if (timer >= timeBeforeAutoScroll && scrollRect.verticalNormalizedPosition > 0)
        {
            AutoScroll();
            isScrolling = true;
        }
    }

    public void StopScroll()
    {
        isScrolling = false;
        Debug.Log("coucou");
        timer = 0;
    }

    private void AutoScroll()
    {
        scrollRect.verticalNormalizedPosition += -speedAutoScroll * Time.deltaTime;
        timer = 2;
    }

    public void ReturnInMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void OnPointerEnter()
    {
        buttonReturn.GetComponent<Image>().sprite = buttonEnter;
        txtButtonReturn.GetComponent<Text>().color = colorEnter;
    }

    public void OnPointerExit()
    {
        buttonReturn.GetComponent<Image>().sprite = buttonExit;
        txtButtonReturn.GetComponent<Text>().color = colorExit;
    }

}
