﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationsMenu : MonoBehaviour
{
    private MainMenuManager menuManager;

    private Animator animatorBackground2, animatorMainMenu, animatorButtonsMenu, animatorIntroLogo, animatorIntroText, animatorSquare, 
                        animatorBackgroundTwoMain, animatorBackgroundOptions, animatorButtons;

    private AudioSource voice;

    // Start is called before the first frame update
    void Start()
    {
        menuManager = GameObject.Find("MainMenuManager").GetComponent<MainMenuManager>();

        animatorBackground2 = GameObject.Find("Background_2_Red").GetComponent<Animator>();
        animatorMainMenu = GameObject.Find("Main_Menu").GetComponent<Animator>();
        animatorButtonsMenu = GameObject.Find("Buttons_Main_Menu").GetComponent<Animator>();

        animatorIntroLogo = GameObject.Find("Logo_Game").GetComponent<Animator>();
        animatorIntroText = GameObject.Find("Txt_Appuyer_Touche").GetComponent<Animator>();

        animatorSquare = GameObject.Find("Background_Options_Square").GetComponent<Animator>();

        animatorBackgroundTwoMain = GameObject.Find("Background_2_Red").GetComponent<Animator>();
        animatorButtons = GameObject.Find("Buttons_Main_Menu").GetComponent<Animator>();
        animatorBackgroundOptions = GameObject.Find("Background_2_Options").GetComponent<Animator>();
        voice = GameObject.Find("Voice").GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //Fonctions lançant l'animation du Texte dans l'intro
    //Appeler grâce à une clé dans l'animation "intro_logo_center"
    public void LaunchAnimationText()
    {
        voice.Play();
        animatorIntroText.SetTrigger("apparition");
        menuManager.canPressnyButtons = true;

    }

    //Fonction lançant l'animation de disparition du logo lors de l'intro
    //Appeler grâce à une clé dans l'animation "Text_Intro_Click"
    public void LaunchAnimationDisparitionLogo()
    {
        animatorIntroLogo.SetTrigger("left");
    }

    //Fonction lançant l'animation d'apparition du fond 2
    //Appeler grâce à une clé dans l'animation "Go_To_Left_Logo"
    public void LaunchAnimationApparitionBackground2()
    {
        animatorBackground2.SetTrigger("apparition");
    }

    //Fonction lançant l'animation d'apparition d'Eve normal
    //Appeler grâce à une clé dans l'animation "Background_2_Apparition_Menu"
    public void LaunchAnimationEveNormal()
    {
        //Debug.Log("Wesh");
        animatorMainMenu.SetTrigger("apparitionEve");
    }

    //Fonction lançant l'animation d'apparition des boutons dans le menu principal
    //Appeler grâce à une clé dans l'animation "Eve_Normal_Apparition"
    public void LaunchAnimationApparationButtons()
    {
        menuManager.isInMainMenu = true;
        menuManager.isInIntro = false;
        animatorButtonsMenu.SetTrigger("apparitionButtons");
    }

    //Fonction lançant l'animation d'apparition du bras pour la première fois
    //Appeler grâce à une clé dans l'animation "Buttons_Menu_Appear"
    public void AnimationArmFirstTime()
    {
        animatorMainMenu.SetTrigger("firstShowing");
    }

    //Fonction lançant l'animation d'apparition du background carré options
    //Appeler grâce à une clé dans l'animation "Buttons_Menu_Appear" --- A DETERMINER
    public void ApparitionSquareOptions()
    {
        animatorSquare.SetTrigger("apparitionSquare");
    }


    //Fonction lançant les animations lorsque le joueur quitte l'écran des options
    //Appeler grâce à une clé dans l'animation "Disparition_Options_Square"
    public void QuitOptions()
    {
        animatorBackgroundTwoMain.SetTrigger("disparitionOptions");
        animatorBackgroundOptions.SetTrigger("disparitionBackgroundOptions");
        animatorButtons.SetTrigger("apparitionButtonsOptions");
        animatorMainMenu.SetTrigger("apparitionEve");
    }

    public void EndApparitionOptions()
    {
        menuManager.isApparitionOptionsEnded = true;
    }
}
