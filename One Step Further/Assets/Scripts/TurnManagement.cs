﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Pathfinding
{
    public class TurnManagement : MonoBehaviour
    {
        public List<GameObject> units;
        public GameObject turnUnit;
        CharacterStats turnUnitStats;
        int turnUnitListKey;

        int roundNumber = 1;

        //Objet suit la souris pour définir quel est le point de destination du personnage. Crucial
        public TargetMover target;

        bool setTargetAsEnabled = true;

        /*** VARIABLES UI ***/
        public Button EndOfTurnButton;
        private ManagerUIScreens managerUI;

        private CharacterStats statsEvelyn;
        private GameObject canvasMovingGO;
        private Animator animatorPhases;

        private Pathfinding.AILerp lerpAsha, lerpEvelyn, lerpHenchman1, lerpHenchman2;

        // Start is called before the first frame update
        public void Start()
        {
            turnUnit = units[0];
            turnUnitListKey = 0;
            turnUnitStats = turnUnit.GetComponent<CharacterStats>();

            turnUnit.GetComponent<Seeker>().MovementPoints = turnUnitStats.MovementPoints;
            turnUnit.transform.GetChild(1).gameObject.SetActive(true);

            managerUI = gameObject.GetComponent<ManagerUIScreens>();

            statsEvelyn = GameObject.Find("Evelyn").GetComponent<CharacterStats>();
            canvasMovingGO = GameObject.Find("CanvasMoving");
            animatorPhases = GameObject.Find("CanvasPhases").GetComponent<Animator>();

            lerpEvelyn = GameObject.Find("Evelyn").GetComponent<Pathfinding.AILerp>();
            lerpAsha = GameObject.Find("Asha").GetComponent<Pathfinding.AILerp>();
            lerpHenchman1 = GameObject.Find("Sbire1").GetComponent<Pathfinding.AILerp>();
            lerpHenchman2 = GameObject.Find("Sbire2").GetComponent<Pathfinding.AILerp>();
        }

        public void OnGUI()
        {
            //Le double-clic indique que le joueur s'est déplacé. Donc hop on kill le TargetMover
            if (Event.current.type == EventType.MouseDown && Event.current.clickCount == 2
                && turnUnitStats.MovementPoints >= Mathf.Ceil(turnUnit.GetComponent<AILerp>().remainingDistance))
            {
                UnitMovementManager((int)Mathf.Ceil(turnUnit.GetComponent<AILerp>().remainingDistance));
            }
        }

        // Update is called once per frame
        public void Update()
        {
            //Contrôle l'activation du script TargetMover au besoin
            if (!setTargetAsEnabled)
                target.enabled = false;
            else target.enabled = true;

            if (turnUnit.GetComponent<AILerp>().reachedEndOfPath)
            {
                //EndOfTurnButton.enabled = true;
                turnUnit.GetComponent<AILerp>().canMove = false;

                if (turnUnitStats.MovementPoints >= Mathf.Round(turnUnit.GetComponent<AILerp>().remainingDistance))
                    setTargetAsEnabled = true;
            }
        }

        //Permet à l'unité dont c'est le tour de se déplacer lorsqu'elle le demande
        public void UnitMovementManager(int distanceUnitMoved)
        {
            turnUnit.GetComponent<AILerp>().canMove = true;
            turnUnit.GetComponent<AILerp>().reachedEndOfPath = false;

            turnUnitStats.MovementPoints -= distanceUnitMoved;
            turnUnit.GetComponent<Seeker>().MovementPoints = turnUnitStats.MovementPoints;

            setTargetAsEnabled = false;
        }

        public void UnitDeath(GameObject deadUnit)
        {
            units.Remove(deadUnit);
            deadUnit.SetActive(false);

            if (turnUnit == deadUnit)
            {
                //Vu qu'on vient de rétrécir le tableau de 1, il est nécessaire de réduire la clé actuelle de 1.
                //Elle trahit son nom alors, ce n'est plus la clé de l'unité dont c'était le tour ; mais sans ça, le tour d'un personnage sera sauté ou y'aura bug
                // Dans un tableau de 3 unités, imaginez qu'on tue la dernière unité ( [2] ) : sa clé 2 ne correspond plus à rien... bug
                turnUnitListKey -= 1;
                EndOfTurn();
            }
        }

        //Initialisation du tour : on empêche le joueur de bouger mais on active tout le reste
        void StartOfTurn()
        {
            if (turnUnit.tag == "Player")
            {
                managerUI.EnableButtonsInCanvasMoving();
                turnUnit.GetComponent<AILerp>().canMove = false;
                setTargetAsEnabled = true;
                //Activer les éléments UI qui permettent de prévoir le parcours suivi par l'unité
                turnUnit.transform.GetChild(0).gameObject.SetActive(true);
                turnUnit.GetComponent<Interact>().hasPushedThisTurn = false;

                roundNumber += 1;
                if (roundNumber == 3) units.Insert(1, GameObject.Find("Asha"));
            }
            //Debug.Log(turnUnit.GetComponent<AILerp>().canMove);
            //Debug.Log("C'est le tour du joueur " + turnUnit.name);
        }

        //Fin de tour : on passe à l'entité suivante
        public void EndOfTurn()
        {
            //On fait disparaître la mini fenêtre des statistiques à droite
            canvasMovingGO.GetComponent<Animator>().SetTrigger("DisappearUIRight");
            canvasMovingGO.GetComponent<MovingManager>().isRightUIAppeared = false;

            //On remet les stats à leur valeur max lorsque c'est pertinent;
            turnUnitStats.ResetStats();

            //Désactiver les éléments UI qui permettent de prévoir le parcours suivi par l'unité
            if (turnUnit.tag == "Player")
            {
                turnUnit.transform.GetChild(0).gameObject.SetActive(false);
                managerUI.DisableButtonsInCanvasMoving();
            }

            GameObject previousUnit = turnUnit;

            //On regarde quelle est la prochaine unité dans le tableau et on lui donne le tour.
            //Si on est au bout du tableau, on repart avec la première unité
            Debug.Log("Le tour du joueur " + turnUnit.name + " est terminé");
            if (units.Count > turnUnitListKey + 1)
            {
                turnUnit = units[turnUnitListKey + 1];
                turnUnitListKey += 1;
            }
            else
            {
                turnUnit = units[0];
                turnUnitListKey = 0;
            }
            turnUnitStats = turnUnit.GetComponent<CharacterStats>();
            turnUnit.GetComponent<Seeker>().MovementPoints = turnUnitStats.MovementPoints;

            //On délègue certains points touchy à TurnTransition car impossible de mettre un IEnumerator en fonction bouton
            StartCoroutine(TurnTransition(turnUnit, previousUnit, 2f));

            lerpEvelyn.canMove = false;
            lerpAsha.canMove = false;
            lerpHenchman1.canMove = false;
            lerpHenchman2.canMove = false;
        }

        //Besoin de faire cette fonction IEnumerator à part car ce type n'est pas pris en charge par les boutons UI...
        //Sert à retirer le collider du joueur suivant et rescanner la zone pour bien enlever l'obstacle associé
        IEnumerator TurnTransition(GameObject unit, GameObject previousUnit, float t)
        {
            if (unit.name == "Evelyn")
                statsEvelyn.Adrenaline += 2;

            //On désactive le collider du prochain joueur, on rescan et on donne du temps pour terminer le scan
            //Meanwhile on réactive le collider de l'unité précédente, ça marche pas dans l'EndOfTurn je sais pas pourquoi
            turnUnit.GetComponent<SphereCollider>().enabled = false;
            previousUnit.GetComponent<SphereCollider>().enabled = true;
            AstarPath.active.Scan();
            yield return new WaitForSeconds(1);

            if (unit.tag == "IA")
            {
                unit.GetComponent<IA>().SearchPath();
                unit.GetComponent<IA>().hasAttackedThisTurn = false;
            }

            if (unit.name == "Evelyn")
            {
                canvasMovingGO.GetComponent<Animator>().ResetTrigger("DisappearUIRight");
                managerUI.ButtonsForEvelyn();
                animatorPhases.Play("PlayerPhase");
                Debug.Log("Ici, on regarde si c'est le tour d'Evelyn et on peut jouer l'animation");
            }
            else if (unit.name == "Asha" || unit.name == "Sbire1" || unit.name == "Sbire2")
            {
                managerUI.ButtonsForEnemies();
            }


            if(roundNumber < 3)
            {
                if (unit.name == "Sbire1")
                    animatorPhases.Play("EnemiesPhase");
            }
            else
            {
                if (unit.name == "Asha")
                    animatorPhases.Play("EnemiesPhase");
            }
            
            StartOfTurn();
        }
    }
}
