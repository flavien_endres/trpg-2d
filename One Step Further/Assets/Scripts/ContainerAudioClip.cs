﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContainerAudioClip : MonoBehaviour
{
    [Header("Audio Clips for Inputs and Combos")]
    public AudioClip sfxComboValidate;
    public AudioClip sfxInputTop;
    public AudioClip sfxInputLeftRight;
    public AudioClip sfxComboLeftRight;
    public AudioClip sfxInputBottom;
    public AudioClip sfxComboNeutral;
    public AudioClip sfxComboPerfect;

    [Header("Audio Clips for QTE")]
    public AudioClip sfxQTEStart;
    public AudioClip sfxQTEFail;
    public AudioClip sfxQTEWin;
    public AudioClip sfxQTEFullWin;

    [Header("Audio Clips for DONS")]
    public AudioClip sfxDonActivate;

    [Header("Audio Clips for cinematics, displaying text")]
    public AudioClip sfxDisplayTextAsha;
    public AudioClip sfxDisplayTextEvelyn;
    public AudioClip sfxDisplayTextHenchman;

    [Header("Audio Clips for cinematics, ambiances")]
    public AudioClip ambianceRooftop;
    public AudioClip ambianceBar;

    [Header("Audio Clips for cinematics, reactions")]
    public AudioClip soundAngryReaction;
    public AudioClip soundSurprisedReaction;

    [Header("Audio Clips for cinematics, sfx")]
    public AudioClip sfxPipeFall;
    public AudioClip sfxPipeGrab;
    public AudioClip sfxGunfire;
    public AudioClip sfxGunLoading;
    public AudioClip sfxGunLoad;
    public AudioClip sfxFtLow;
    public AudioClip sfxFtNormal;

    [Header("Transitions")]
    public AudioClip sfxTransiCombatIn;
    public AudioClip sfxTransiCombatOut;

    [Header("stickers win & lose") ]
    public AudioClip sfxWin;
    public AudioClip sfxLose;

    [Header("stickers win & lose")]
    public AudioClip sfxHoverButtons;
    public AudioClip sfxClickButtons;
}
