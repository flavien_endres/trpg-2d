﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pathfinding
{
    public class CharacterStats : MonoBehaviour
    {
        public int HealthPoints;
        public int MovementPoints;

        int MovementPoints_max;

        public int Attaque;
        public int Defense;
        public int Adrenaline;
        public int Agilite;
        public int Vitesse;
        public int Chance;
        public int nombreActions;

        // Start is called before the first frame update

        private void Start()
        {
            MovementPoints_max = MovementPoints;
        }

        //Pour réinitialiser les stats en début de tour
        public void ResetStats()
        {
            MovementPoints = MovementPoints_max;
        }

    }
}