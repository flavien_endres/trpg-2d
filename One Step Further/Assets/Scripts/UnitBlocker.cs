﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class UnitBlocker : MonoBehaviour
{
    public void Start()
    {
        var blocker = GetComponent<SingleNodeBlocker>();

        blocker.BlockAtCurrentPosition();
    }
}
