﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionMenu : MonoBehaviour
{

	public GameObject attackButton;
	public GameObject defenseButton;

	public bool activeMenu = false;

    private void enableMenu(){

    	attackButton.SetActive(true);
    	defenseButton.SetActive(true);
    	activeMenu = true;
    }

    private void disableMenu(){

    	attackButton.SetActive(false);
    	defenseButton.SetActive(false);
    	activeMenu = false;

    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    	if ( Input.GetMouseButtonUp(1) ){


    		if( !activeMenu ){

    			GameObject.FindWithTag("Player").GetComponent<PlayerMovements>().enabled = false;
    			enableMenu();
    		}	
    		else{

    			GameObject.FindWithTag("Player").GetComponent<PlayerMovements>().enabled = true;
    			disableMenu();
    		}

    	}
        
    }
}
