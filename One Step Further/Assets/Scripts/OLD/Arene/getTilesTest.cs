﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

using System;
using System.IO;

public class getTilesTest : MonoBehaviour
{

    private char[,] arenaGrid;

    static private int absoluteValue( int value ){

        return value < 0 ? -(value) : (value);
    
    }

    void Start () {


        Tilemap tilemap = gameObject.transform.Find("Level").GetComponent<Tilemap>();
        Tilemap tilemap2 = gameObject.transform.Find("Basic").GetComponent<Tilemap>();
        GameObject player = gameObject.transform.Find("Character").gameObject;



        Debug.Log("LAAA : " + player.transform.position );
        Debug.Log("LAAA : " + player.transform.localPosition );

        Debug.Log( tilemap.layoutGrid.LocalToCell( player.transform.position ) );

        BoundsInt bounds = tilemap2.cellBounds;

        arenaGrid = new char[ bounds.size.x, bounds.size.y ];

        TileBase[] allTiles = tilemap.GetTilesBlock(bounds);

        /*
            Récupérer toutes les tiles d'une tilemap : 

                Stocker dans tableau TileBase -> tilemap.GetTilesBlock(nb tiles)
                Récupérer chaques Tiles d'une tilemap -> x + y * taille colonne
                Stocker dans un tableau 2D -> (x, tailleY - y - 1)

        */
                
        for (int x = 0; x < bounds.size.x; x++) {

            for (int y = 0; y < bounds.size.y; y++) {

                Debug.Log( "TESST : ( " + x + " - " + (bounds.size.y - y - 1) + " )" );
                TileBase tile = allTiles[x + y * bounds.size.x];    

                if (tile != null) {

                    if( String.Compare( tile.name, "plains-sliced_72") == 0  || String.Compare( tile.name, "plains-sliced_71") == 0) {

                        arenaGrid[x, bounds.size.y - y - 1] = 'e';
                        Debug.Log("x:" + x + " y:" + y + " tile:" + tile.name);
                    
                    }

                    else{

                        if( String.Compare( tile.name, "plains-sliced_44" ) == 0 //C'est dégueulasse !
                            || String.Compare( tile.name, "plains-sliced_48" ) == 0 
                            || String.Compare( tile.name, "plains-sliced_53" ) == 0 
                            || String.Compare( tile.name, "plains-sliced_56" ) == 0
                            || String.Compare( tile.name, "plains-sliced_53" ) == 0 
                            || String.Compare( tile.name, "plains-sliced_52" ) == 0 
                            || String.Compare( tile.name, "plains-sliced_51" ) == 0 
                            || String.Compare( tile.name, "plains-sliced_41" ) == 0
                            || String.Compare( tile.name, "plains-sliced_45" ) == 0 ){

                            arenaGrid[x,bounds.size.y - y - 1] = 'm';

                        }

                        else{

                            arenaGrid[x, bounds.size.y - y - 1] = '0';
                            Debug.Log("x:" + x + " y:" + y + " tile: (null)");

                        }


                    }


                } else {

                    arenaGrid[x, bounds.size.y - y - 1] = '0';
                    Debug.Log("x:" + x + " y:" + y + " tile: (null)");

                }

            }

        } 

        Vector3 playerPosition = tilemap.layoutGrid.LocalToCell( player.transform.position ); //A changer après les test pour le faire pour tous les joueurs
        arenaGrid[ (int) playerPosition.x, absoluteValue( (int) playerPosition.y ) ] = 'p';

  




    string path = "Assets/test.txt";

        //Write some text to the test.txt file
        StreamWriter writer = new StreamWriter(path, true);

        string str = "";

        for (int y = 0; y < bounds.size.y; y++) {

            for (int x = 0; x < bounds.size.x; x++) {

                        str += arenaGrid[x,y];
                
                }

            

            writer.WriteLine(str);
            str = "";
        }


        writer.Close();


    Debug.Log("FINIII");

/*        Debug.Log("AVANT -> " + allTiles[ 1 + 1 * bounds.size.x] );    	
        SwapTile( allTiles[1 + 1 * bounds.size.x], allTiles[0].sprite );
        Debug.Log("APRES -> " + allTiles[ 1 + 1 * bounds.size.x] );     */
    }   



}
