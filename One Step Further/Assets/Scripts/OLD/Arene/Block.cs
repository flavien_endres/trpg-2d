﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Block
{

	Ground,
	Wall,
	Stairs

}