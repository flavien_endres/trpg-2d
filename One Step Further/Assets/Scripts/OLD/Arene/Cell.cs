﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour {

	private readonly string name;
	private readonly Block blockType;
	private readonly bool clickable;

	public Cell( string name ){

		this.name = name;

	}

	public string getName(){
		return name;
	}

}
