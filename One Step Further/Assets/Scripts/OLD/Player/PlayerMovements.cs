using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovements : MonoBehaviour
{
    private Rigidbody2D rb;
    private float moveH, moveV;
    [SerializeField] private float moveSpeed = 1.0f;

    private Vector3 futurPosition;
    private bool inMovement = false;
    private static float MAX_EPSILON = 0.1f;



    private void Awake()
    {	
        rb = GetComponent<Rigidbody2D>();
    }

    private float positionToMove(float currentPosition, float futurPosition)
    {

        Debug.Log("ici");

        if( currentPosition > futurPosition)
        	return -1 * moveSpeed; 
        
        return 1 * moveSpeed;

    }

    private float movingPlayerByAxis(float currentPosition, float futurPosition ,float epsilon){
		
		if ( Mathf.Abs( currentPosition - futurPosition ) >= epsilon )
			return positionToMove(currentPosition, futurPosition);

		return 0;
			            
    }

    private float[] movingPlayer(Vector3 currentPosition, Vector3 futurPosition ){

 		if (  (currentPosition - futurPosition).sqrMagnitude <= 0.01f )
	        return new float[2] {0,0};

        return new float[2] {movingPlayerByAxis( currentPosition.x, futurPosition.x, MAX_EPSILON ), movingPlayerByAxis( currentPosition.y, futurPosition.y, MAX_EPSILON )};
	         
    }

    private Vector3 getMousePosition(){

    	Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    	mousePosition.z = transform.position.z;

    	return mousePosition;

    }

    private void setInMovement(){
    	inMovement = true;
    }

    private void unsetInMovement(){
    	inMovement = false;
    }


    private void FixedUpdate()
    {

        Debug.Log("Position local : " + transform.localPosition);
        Debug.Log("Futur Position : " + futurPosition);

        if (Input.GetMouseButton(0))
        {

            futurPosition = getMousePosition();
			/* futurPosition = new Vector3(2, 2, 0);*/
           	setInMovement();

        }

        else
        {

            print("Not pressed");

        }

       
        if( inMovement )
        {

        	float[] moving = movingPlayer( transform.localPosition, futurPosition );

        	if( moving[0] == 0 && moving[1] == 0 ){
        		
        		unsetInMovement();
        		
        	}
        	else{

        		moveH = moving[0];
        		moveV = moving[1]; 

        	}

        }
        else
        {
            moveH = 0;
            moveV = 0;
        }

/*        var hmo = Input.GetAxis("Horizontal");
        var vmo = Input.GetAxis("Vertical");

        moveH = hmo * moveSpeed;
        moveV = vmo * moveSpeed;
*/
        Debug.Log("Moving vectors : " + moveH + " - " + moveV);

        rb.velocity = new Vector2(moveH, moveV);//OPTIONAL rb.MovePosition();

        Vector2 direction = new Vector2(moveH, moveV);

        FindObjectOfType<PlayerAnimation>().SetDirection(direction);
    }



}
