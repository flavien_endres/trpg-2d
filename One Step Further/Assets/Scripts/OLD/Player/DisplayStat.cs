﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayStat : MonoBehaviour
{

   	private Text healthDisplay;
   	private Text manaDisplay;
   	private Text defenseDisplay;

    public PlayerStats statsPlayer;

    void Start()
    {

    	healthDisplay = transform.Find("Health").GetComponent<Text>(); 
    	manaDisplay = transform.Find("Mana").GetComponent<Text>();
    	
     
    }

    // Update is called once per frame
    void Update()
    {

        healthDisplay.text = "" + statsPlayer.getHealth();
        manaDisplay.text = "" + statsPlayer.getMana();

    }
}
