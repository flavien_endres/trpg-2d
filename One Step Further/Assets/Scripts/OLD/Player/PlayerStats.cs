﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{

	public int health;
	public int mana;
	public int defense;

	public int movePoints;

	public PlayerStats( int health, int mana, int defense ){

		this.health = health;
		this.defense = defense;
		this.mana = mana;

	}

	public void addHealth( int addingHealth ){
		health += addingHealth;
	}

	public void removeHealth( int removingHealth ){
		health -= removingHealth;
	}

	public int getHealth(){
		return health;
	}

	public int getMana(){
		return mana;
	}



}
