﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pathfinding
{
    public class CheckForTargetsInRange : MonoBehaviour
    {

        private void OnTriggerEnter(Collider other)
        {
            if (transform.parent.tag == "IA")
            {
                if (other.tag == "Player")
                {
                    transform.parent.GetComponent<IA>().UnitsInAttackRange.Add(other.gameObject);
                }
            }

            if (transform.parent.tag == "Player")
            {
                if (other.tag == "IA")
                {
                    //Debug.Log(other.name);
                    transform.parent.GetComponent<Interact>().UnitsInAttackRange.Add(other.gameObject);
                }
                if (other.tag == "Interactable")
                {
                    transform.parent.GetComponent<Interact>().UnitsInInteractRange.Add(other.gameObject);
                }
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (transform.parent.tag == "IA")
            {
                if (other.tag == "Player")
                {
                    transform.parent.GetComponent<IA>().UnitsInAttackRange.Remove(other.gameObject);
                }
            }

            if (transform.parent.tag == "Player")
            {
                if (other.tag == "IA")
                {
                    transform.parent.GetComponent<Interact>().UnitsInAttackRange.Remove(other.gameObject);
                }
                if (other.tag == "Interactable")
                {
                    transform.parent.GetComponent<Interact>().UnitsInInteractRange.Remove(other.gameObject);
                    transform.parent.GetComponent<Interact>().anObjectHasBeenClickedOnToBePushed = false;
                }
            }
        }
    }
}
