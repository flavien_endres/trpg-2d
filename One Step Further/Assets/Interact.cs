﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Pathfinding
{
    public class Interact : MonoBehaviour
    {
        /// <summary>Mask for the raycast placement</summary>
        LayerMask mask;

        [HideInInspector]
        public List<GameObject> UnitsInAttackRange = new List<GameObject>();
        [HideInInspector]
        public List<GameObject> UnitsInInteractRange = new List<GameObject>();
        [HideInInspector]
        public bool hasAttackedThisTurn = false;
        private GameObject previousElementPointed;
        [HideInInspector]
        public GameObject previousObjectPushed;

        private levelManager managerLevel;

        private MovingManager manageMoving;
        private Animator animCanvasMoving;

        [HideInInspector]
        private GameObject objectToPush;

        Camera cam;

        private Button buttonAttackMoving;

        [HideInInspector]
        public bool hasPushedThisTurn = false;
        [HideInInspector]
        public bool anObjectHasBeenClickedOnToBePushed = false;
        [HideInInspector]
        public bool anObjectHasBeenMarkedForGift = false;

        private CanvasGroup canvasGroupCombat, canvasGroupPunch, canvasGroupCombo;
        private TurnManagement scriptTurnManagement;
        private GameObject evelynUnits;

        // Start is called before the first frame update
        void Start()
        {
            cam = Camera.main;
            mask = (1 << 8) | (1 << 9);

            managerLevel = GameObject.Find("GameManager").GetComponent<levelManager>();
            manageMoving = GameObject.Find("CanvasMoving").GetComponent<MovingManager>();
            canvasGroupCombat = GameObject.Find("CanvasCombat").GetComponent<CanvasGroup>();
            canvasGroupPunch = GameObject.Find("CanvasPunch").GetComponent<CanvasGroup>();
            canvasGroupCombo = GameObject.Find("CanvasCombo").GetComponent<CanvasGroup>();
            animCanvasMoving = GameObject.Find("CanvasMoving").GetComponent<Animator>();
            buttonAttackMoving = GameObject.Find("Bouton_Attaquer").GetComponent<Button>();
            scriptTurnManagement = GameObject.Find("GameManager").GetComponent<TurnManagement>();
            evelynUnits = GameObject.Find("Evelyn");
        }

        // Update is called once per frame
        void Update()
        {
            if(canvasGroupCombat.alpha == 0 && canvasGroupCombo.alpha == 0 && canvasGroupPunch.alpha == 0)
            {
                RaycastHit hit;
                if (Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out hit, Mathf.Infinity, mask))
                {
                    if (hit.collider.gameObject.name == "Evelyn" && !managerLevel.isInPauseMenu)
                    {
                        previousElementPointed = hit.transform.gameObject;
                        hit.transform.gameObject.GetComponentInChildren<SetAlpha>().ChangeAlphaValue(255);
                        if (Input.GetMouseButtonDown(0))
                        {
                            manageMoving.UIStatsEvelyn();
                            managerLevel.unitsNameStats = "Evelyn";
                            objectToPush = null;

                            /*if (!manageMoving.isLeftUIAppeared)
                                animCanvasMoving.SetTrigger("AppearUILeft");*/

                            if (!manageMoving.isRightUIAppeared && scriptTurnManagement.turnUnit == evelynUnits)
                            {
                                animCanvasMoving.SetTrigger("AppearUIRight");
                                //Debug.Log("coucouAgainV2");
                                manageMoving.isRightUIAppeared = true;
                            }
                        }
                    }

                    previousElementPointed = hit.transform.gameObject;
                    hit.transform.gameObject.GetComponentInChildren<SetAlpha>().ChangeAlphaValue(255);
                    if (Input.GetMouseButtonDown(0) && !managerLevel.isInPauseMenu && managerLevel.isInMovingPhase)
                    {

                        if (hit.collider.gameObject.name == "Asha")
                        {
                            managerLevel.unitsNameStats = hit.collider.gameObject.name;
                            manageMoving.UIStatsAsha();
                        }
                        else if (hit.collider.gameObject.name == "Sbire1")
                        {
                            managerLevel.unitsNameStats = hit.collider.gameObject.name;
                            manageMoving.UIStatsHenchman1();
                        }
                        else if (hit.collider.gameObject.name == "Sbire2")
                        {
                            managerLevel.unitsNameStats = hit.collider.gameObject.name;
                            manageMoving.UIStatsHenchman2();
                        }


                        /*
                        if (!manageMoving.isLeftUIAppeared)
                        {
                            Debug.Log("coucou");
                            animCanvasMoving.SetTrigger("AppearUILeft");
                            manageMoving.isLeftUIAppeared = true;
                        }
                        */

                        if (!manageMoving.isRightUIAppeared && scriptTurnManagement.turnUnit == evelynUnits && managerLevel.unitsNameStats != "")
                        {
                            animCanvasMoving.SetTrigger("AppearUIRight");
                            manageMoving.isRightUIAppeared = true;
                        }

                    }

                    //On vérifie si l'unité pointée par la souris est bien dans la liste des unités ennemies à portée d'attaque
                    if (UnitsInAttackRange.Contains(hit.transform.gameObject))
                    {
                        if (Input.GetMouseButtonDown(0) && !managerLevel.isInPauseMenu && managerLevel.isInMovingPhase)
                        {

                            managerLevel.currentCharacterAttacking = "Evelyn";
                            managerLevel.currentCharacterAttacked = hit.collider.gameObject.name;
                            managerLevel.unitsNameStats = hit.collider.gameObject.name;
                            buttonAttackMoving.interactable = true;

                        }
                    }
                    //Sinon, on vérifie si c'est un objet avec lequel on peut interagir
                    if (UnitsInInteractRange.Contains(hit.transform.gameObject))
                    {
                        anObjectHasBeenClickedOnToBePushed = true;
                        previousElementPointed = hit.transform.gameObject;
                        hit.transform.gameObject.GetComponentInChildren<SetAlpha>().ChangeAlphaValue(255);
                        if (Input.GetMouseButtonDown(0))
                        {
                            objectToPush = previousElementPointed;
                            //Debug.Log(objectToPush);

                            /*
                            if (!manageMoving.isLeftUIAppeared)
                            {
                                animCanvasMoving.SetTrigger("AppearUILeft");
                                manageMoving.isLeftUIAppeared = true;
                            }
                            */
                        }
                    }

                    if (hit.collider.gameObject.name.Contains("Flèche"))
                    {
                        if (Input.GetMouseButtonDown(0))
                        {
                            hit.collider.gameObject.transform.parent.parent.GetComponent<IA_objet>().MoveAgain(hit.collider.gameObject.name);
                            anObjectHasBeenMarkedForGift = false;
                        }
                    }

                }
                else if (previousElementPointed != null)
                {
                    if (previousElementPointed.tag == "Interactable")
                    {
                        if (!previousElementPointed.GetComponent<IA_objet>().isMarked)
                        {
                            previousElementPointed.GetComponentInChildren<SetAlpha>().ChangeAlphaValue(0);
                        }
                        previousElementPointed = null;
                    }
                    else
                    {
                        previousElementPointed.GetComponentInChildren<SetAlpha>().ChangeAlphaValue(0);
                        previousElementPointed = null;
                    }
                }
            }

            
        }

        public void Push()
        {
            if (UnitsInInteractRange.Contains(objectToPush)  && !hasPushedThisTurn)
            {
                objectToPush.GetComponent<IA_objet>().isMarked = true;
                anObjectHasBeenMarkedForGift = true;
                objectToPush.GetComponentInChildren<SetAlpha>().ChangeAlphaValue(255);
                objectToPush.GetComponent<IA_objet>().Move(transform.position);

                if (previousObjectPushed != null && previousObjectPushed != objectToPush)
                {
                    previousObjectPushed.GetComponent<IA_objet>().isMarked = false;
                    previousObjectPushed.GetComponentInChildren<SetAlpha>().ChangeAlphaValue(0);
                }

                previousObjectPushed = objectToPush;
                objectToPush = null;
                hasPushedThisTurn = true;
                anObjectHasBeenClickedOnToBePushed = false;
            }

            /*
            if (manageMoving.isLeftUIAppeared)
            {
                animCanvasMoving.SetTrigger("DisappearUILeft");
                manageMoving.isLeftUIAppeared = false;
            }
            */

        }

        public void ShowGiftArrows()
        {
            if (previousObjectPushed != null)
            {
                if (!previousObjectPushed.transform.GetChild(2).gameObject.activeSelf && previousObjectPushed.GetComponent<IA_objet>().isMarked)
                    previousObjectPushed.transform.GetChild(2).gameObject.SetActive(true);
                else if (previousObjectPushed.transform.GetChild(2).gameObject.activeSelf)
                    previousObjectPushed.transform.GetChild(2).gameObject.SetActive(false);
            }
        }
    }

}
